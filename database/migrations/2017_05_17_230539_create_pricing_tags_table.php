<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label')->unique();
            $table->timestamps();
        });

        Schema::create('pricing_tags_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pricing_tag_id')->unsigned();
            $table->string('locale')->index();
            $table->string('description');
            $table->unique(['pricing_tag_id','locale']);
            $table->foreign('pricing_tag_id')->references('id')->on('pricing_tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_tags_translations');
        Schema::dropIfExists('pricing_tags');
    }
}
