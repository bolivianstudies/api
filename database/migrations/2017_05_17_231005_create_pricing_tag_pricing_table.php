<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTagPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_tag_pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pricing_id')->unsigned();
            $table->foreign('pricing_id')->references('id')->on('pricings')->onDelete('cascade');
            $table->integer('pricing_tag_id')->unsigned();
            $table->foreign('pricing_tag_id')->references('id')->on('pricing_tags')->onDelete('cascade');
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_tag_pricing');
    }
}
