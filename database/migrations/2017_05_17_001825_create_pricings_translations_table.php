<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricings_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pricing_id')->unsigned();
            $table->string('locale')->index();
            $table->string('description');
            $table->string('membership');
            $table->string('nationality');
            $table->string('member');
            $table->unique(['pricing_id','locale']);
            $table->foreign('pricing_id')->references('id')->on('pricings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricings_translations');
    }
}
