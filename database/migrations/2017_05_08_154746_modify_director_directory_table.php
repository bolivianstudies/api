<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDirectorDirectoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('director_directory', function (Blueprint $table) {
            $table->string('role', 255);
            $table->integer('position')->unsigned();
        });

        Schema::table('directors', function (Blueprint $table) {
            $table->dropColumn('role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('director_directory', function(Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('position');
        });

        Schema::table('directors', function (Blueprint $table) {
            $table->string('role', 255);
        });
    }
}
