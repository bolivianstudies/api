<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('pricing_id')->unsigned();
            $table->foreign('pricing_id')->references('id')->on('pricings')->onDelete('cascade');
            $table->string('country_id', 2);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->string('city');
            $table->string('name');
            $table->string('email');
            $table->text('address');
            $table->string('phone');
            $table->string('zip');
            $table->date('verified_at')->nullable();
            $table->integer('payment_method_id')->unsigned();
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
            $table->string('payment_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
