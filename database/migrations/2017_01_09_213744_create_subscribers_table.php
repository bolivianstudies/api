<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->boolean('active')->default(0);
            $table->boolean('posts')->default(1);
            $table->boolean('events')->default(1);
            $table->boolean('docs')->default(1);
            $table->boolean('magazines')->default(1);
            $table->boolean('newsletters')->default(1);
            $table->boolean('publications')->default(1);
            $table->string('activation_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
