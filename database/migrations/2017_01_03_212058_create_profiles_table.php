<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang', 2)->default('es');
            $table->string('name');
            $table->string('institution');
            $table->text('address_personal');
            $table->text('address_institution');
            $table->string('discipline');
            $table->text('areas');
            $table->text('content');
            $table->date('birthday')->nullable();
            $table->string('phone');
            $table->string('website');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('google');
            $table->string('skype');
            $table->string('city');
            $table->string('country_id', 2);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
