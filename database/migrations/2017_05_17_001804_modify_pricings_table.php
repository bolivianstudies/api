<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pricings', function (Blueprint $table) {
            $table->dropColumn('price');
            $table->dropColumn('lang');
            $table->dropColumn('membership');
            $table->dropColumn('nationality');
            $table->dropColumn('member');
            $table->dropColumn('currency');
            $table->decimal('price_usd', 5, 2)->nullable();
            $table->decimal('price_bob', 5, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pricings', function (Blueprint $table) {
            $table->integer('price');
            $table->string('lang', 2)->default('es');
            $table->string('membership');
            $table->string('nationality');
            $table->string('member');
            $table->string('currency');
            $table->dropColumn('price_usd');
            $table->dropColumn('price_bob');
        });
    }
}
