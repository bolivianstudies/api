<?php

use App\Education;
use Illuminate\Database\Seeder;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('educations')->delete();

        $educations = [
            [
                "date" => "1998-03-01",
                "title" => "Lic. Literatura",
                "institution" => "Universidad de Buenos Aires",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 1,
            ],
            [
                "date" => "2001-11-01",
                "title" => "Master filosofía y letra",
                "institution" => "Universidad Mayor de San Andrés",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 1,
            ],
            [
                "date" => "2012-06-01",
                "title" => "Doc. Antropología",
                "institution" => "Universidad de Boston",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 1,
            ],
            [
                "date" => "2015-11-01",
                "title" => "Estudio Superior en Literatura",
                "institution" => "Centro de Historia y Arte",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 1,
            ],
            [
                "date" => "1998-03-01",
                "title" => "Lic. Literatura",
                "institution" => "Universidad de Buenos Aires",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 2,
            ],
            [
                "date" => "2001-11-01",
                "title" => "Master filosofía y letra",
                "institution" => "Universidad Mayor de San Andrés",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 2,
            ],
            [
                "date" => "2012-06-01",
                "title" => "Doc. Antropología",
                "institution" => "Universidad de Boston",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 2,
            ],
            [
                "date" => "2015-11-01",
                "title" => "Estudio Superior en Literatura",
                "institution" => "Centro de Historia y Arte",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 2,
            ],
            [
                "date" => "1998-03-01",
                "title" => "Lic. Literatura",
                "institution" => "Universidad de Buenos Aires",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 3,
            ],
            [
                "date" => "2001-11-01",
                "title" => "Master filosofía y letra",
                "institution" => "Universidad Mayor de San Andrés",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 3,
            ],
            [
                "date" => "2012-06-01",
                "title" => "Doc. Antropología",
                "institution" => "Universidad de Boston",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 3,
            ],
            [
                "date" => "2015-11-01",
                "title" => "Estudio Superior en Literatura",
                "institution" => "Centro de Historia y Arte",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 3,
            ],
            [
                "date" => "1998-03-01",
                "title" => "Lic. Literatura",
                "institution" => "Universidad de Buenos Aires",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 4,
            ],
            [
                "date" => "2001-11-01",
                "title" => "Master filosofía y letra",
                "institution" => "Universidad Mayor de San Andrés",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 4,
            ],
            [
                "date" => "2012-06-01",
                "title" => "Doc. Antropología",
                "institution" => "Universidad de Boston",
                "description" => "Etiam semper sollicitudin eros, sed vulputate eros efficitur quis. Ut placerat lacus sed pretium venenatis.",
                "profile_id" => 4,
            ],
            [
                "date" => "2015-11-01",
                "title" => "Estudio Superior en Literatura",
                "institution" => "Centro de Historia y Arte",
                "description" => "Vestibulum fringilla sodales sem, in sodales est semper sed. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
                "profile_id" => 4,
            ],
        ];

        foreach ($educations as $education) {
            Education::create($education);
        }
    }
}
    
