<?php

use App\Director;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DirectoryDirectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("director_directory")->delete();
        $directorDirectories = [
            // 2003
            [
                "director_id" => 25,
                "directory_id" => 2003,
                "role" => "Presidente fundador",
                "position" => 1
            ],
            [
                "director_id" => 5,
                "directory_id" => 2003,
                "role" => "Vicepresidenta fundadora",
                "position" => 2
            ],
            [
                "director_id" => 23,
                "directory_id" => 2003,
                "role" => "Tesorera",
                "position" => 3
            ],
            [
                "director_id" => 21,
                "directory_id" => 2003,
                "role" => "",
                "position" => 4
            ],
            // 2005
            [
                "director_id" => 14,
                "directory_id" => 2005,
                "role" => "Presidente",
                "position" => 1
            ],
            [
                "director_id" => 19,
                "directory_id" => 2005,
                "role" => "Vicepresidente",
                "position" => 2
            ],
            [
                "director_id" => 5,
                "directory_id" => 2005,
                "role" => "Tesorera",
                "position" => 3
            ],
            [
                "director_id" => 11,
                "directory_id" => 2005,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 15,
                "directory_id" => 2005,
                "role" => "",
                "position" => 5
            ],
            [
                "director_id" => 10,
                "directory_id" => 2005,
                "role" => "",
                "position" => 6
            ],
            [
                "director_id" => 18,
                "directory_id" => 2005,
                "role" => "",
                "position" => 7
            ],
            [
                "director_id" => 29,
                "directory_id" => 2005,
                "role" => "",
                "position" => 8
            ],
            // 2007
            [
                "director_id" => 27,
                "directory_id" => 2007,
                "role" => "Presidenta",
                "position" => 1
            ],
            [
                "director_id" => 17,
                "directory_id" => 2007,
                "role" => "Vicepresidente",
                "position" => 2
            ],
            [
                "director_id" => 13,
                "directory_id" => 2007,
                "role" => "Tesorera",
                "position" => 3
            ],
            [
                "director_id" => 22,
                "directory_id" => 2007,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 11,
                "directory_id" => 2007,
                "role" => "",
                "position" => 5
            ],
            [
                "director_id" => 26,
                "directory_id" => 2007,
                "role" => "",
                "position" => 6
            ],
            [
                "director_id" => 7,
                "directory_id" => 2007,
                "role" => "",
                "position" => 7
            ],
            [
                "director_id" => 24,
                "directory_id" => 2007,
                "role" => "",
                "position" => 8
            ],
            // 2008
            [
                "director_id" => 24,
                "directory_id" => 2008,
                "role" => "Presidenta",
                "position" => 1
            ],
            [
                "director_id" => 27,
                "directory_id" => 2008,
                "role" => "Vicepresidenta",
                "position" => 2
            ],
            [
                "director_id" => 13,
                "directory_id" => 2008,
                "role" => "Tesorera",
                "position" => 3
            ],
            [
                "director_id" => 7,
                "directory_id" => 2008,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 22,
                "directory_id" => 2008,
                "role" => "",
                "position" => 5
            ],
            // 2009
            [
                "director_id" => 24,
                "directory_id" => 2009,
                "role" => "Presidenta",
                "position" => 1
            ],
            [
                "director_id" => 27,
                "directory_id" => 2009,
                "role" => "Vicepresidenta",
                "position" => 2
            ],
            [
                "director_id" => 13,
                "directory_id" => 2009,
                "role" => "Tesorera",
                "position" => 3
            ],
            [
                "director_id" => 7,
                "directory_id" => 2009,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 22,
                "directory_id" => 2009,
                "role" => "",
                "position" => 5
            ],
            [
                "director_id" => 12,
                "directory_id" => 2009,
                "role" => "",
                "position" => 6
            ],
            [
                "director_id" => 1,
                "directory_id" => 2009,
                "role" => "",
                "position" => 7
            ],
            // 2011
            [
                "director_id" => 7,
                "directory_id" => 2011,
                "role" => "Presidenta",
                "position" => 1
            ],
            [
                "director_id" => 24,
                "directory_id" => 2011,
                "role" => "Vicepresidenta",
                "position" => 2
            ],
            [
                "director_id" => 9,
                "directory_id" => 2011,
                "role" => "Tesorera",
                "position" => 3
            ],
            [
                "director_id" => 16,
                "directory_id" => 2011,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 28,
                "directory_id" => 2011,
                "role" => "",
                "position" => 5
            ],
            [
                "director_id" => 6,
                "directory_id" => 2011,
                "role" => "",
                "position" => 6
            ],
            [
                "director_id" => 1,
                "directory_id" => 2011,
                "role" => "",
                "position" => 7
            ],
            // 2013
            [
                "director_id" => 1,
                "directory_id" => 2013,
                "role" => "Presidente",
                "position" => 1
            ],
            [
                "director_id" => 7,
                "directory_id" => 2013,
                "role" => "Vicepresidenta",
                "position" => 2
            ],
            [
                "director_id" => 9,
                "directory_id" => 2013,
                "role" => "",
                "position" => 3
            ],
            [
                "director_id" => 2,
                "directory_id" => 2013,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 5,
                "directory_id" => 2013,
                "role" => "",
                "position" => 5
            ],
            [
                "director_id" => 28,
                "directory_id" => 2013,
                "role" => "",
                "position" => 6
            ],
            [
                "director_id" => 22,
                "directory_id" => 2013,
                "role" => "",
                "position" => 7
            ],
            // 2015
            [
                "director_id" => 1,
                "directory_id" => 2015,
                "role" => "Presidente",
                "position" => 1
            ],
            [
                "director_id" => 2,
                "directory_id" => 2015,
                "role" => "Vicepresidenta",
                "position" => 2
            ],
            [
                "director_id" => 3,
                "directory_id" => 2015,
                "role" => "",
                "position" => 3
            ],
            [
                "director_id" => 5,
                "directory_id" => 2015,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 8,
                "directory_id" => 2015,
                "role" => "",
                "position" => 5
            ],
            [
                "director_id" => 4,
                "directory_id" => 2015,
                "role" => "",
                "position" => 6
            ],
            // 2017
            [
                "director_id" => 20,
                "directory_id" => 2017,
                "role" => "Presidente",
                "position" => 1
            ],
            [
                "director_id" => 6,
                "directory_id" => 2017,
                "role" => "Vicepresidenta",
                "position" => 2
            ],
            [
                "director_id" => 3,
                "directory_id" => 2017,
                "role" => "Tesorero",
                "position" => 3
            ],
            [
                "director_id" => 25,
                "directory_id" => 2017,
                "role" => "",
                "position" => 4
            ],
            [
                "director_id" => 8,
                "directory_id" => 2017,
                "role" => "",
                "position" => 5
            ],
        ];

        foreach ($directorDirectories as $directoryDirectory) {
            $director = Director::find($directoryDirectory["director_id"]);
            $director->directories()->attach($directoryDirectory["directory_id"], [
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
                "role" => $directoryDirectory["role"],
                "position" => $directoryDirectory["position"]
            ]);
        }
    }
}
