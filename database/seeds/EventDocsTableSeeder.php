<?php

use App\Event;
use Illuminate\Database\Seeder;

class EventDocsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_doc')->delete();

        $eventDocs = [
            [
                "event_id" => 1,
                "doc_id" => 33,
            ],
            [
                "event_id" => 1,
                "doc_id" => 34,
            ],
            [
                "event_id" => 1,
                "doc_id" => 35,
            ],
            [
                "event_id" => 7,
                "doc_id" => 33,
            ],
            [
                "event_id" => 7,
                "doc_id" => 34,
            ],
            [
                "event_id" => 7,
                "doc_id" => 35,
            ],
        ];

        foreach ($eventDocs as $eventDoc) {
            $event = Event::find($eventDoc["event_id"]);
            $event->docs()->attach($eventDoc["doc_id"], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }
    }
}
