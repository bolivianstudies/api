<?php

use App\LocationMarker;
use Illuminate\Database\Seeder;

class LocationMarkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('location_marker')->delete();

        $locationMarkers = [
            [
                "location_id" => 1,
                "latitude" => "-19.047159",
                "longitude" => "-65.258260"
            ],
            [
                "location_id" => 1,
                "latitude" => "-19.048910",
                "longitude" => "-65.260762"
            ],
            [
                "location_id" => 1,
                "latitude" => "-19.047508",
                "longitude" => "-65.260159"
            ],
        ];

        foreach ($locationMarkers as $locationMarker) {
            LocationMarker::create($locationMarker);
        }
    }
}
