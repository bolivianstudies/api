<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emails = [
            "alejandro.heredia.c@gmail.com",
            "hernan.pruden@gmail.com",
            "josefa.salmon@gmail.com",
            "p.revillao@gmail.com"
        ];

        foreach ($emails as $email) {
            $user = User::where("email", $email)->first();
            if ($user) {
                $user->roles()->attach([1, 2], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
            }
        }
    }
}
