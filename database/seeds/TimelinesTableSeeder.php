<?php

use App\Timeline;
use Illuminate\Database\Seeder;

class TimelinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('timelines')->delete();

        $timelines = [
            [
                "id" => 1,
                "lang" => "es",
                "date" => "2017-03-31",
                "description" => "Fecha límite para el envío de propuestas de ponencias."
            ],
            [
                "id" => 2,
                "lang" => "es",
                "date" => "2017-04-03",
                "description" => "Respuestas sobre aceptación de los resúmenes de ponencia a cargo de los coordinadores de cada simposio. "
            ],
            [
                "id" => 3,
                "lang" => "es",
                "date" => "2017-05-15",
                "description" => "Fecha límite para inscribirse al IX Congreso con descuento. La membresía también debe estar pagada."
            ],
            [
                "id" => 4,
                "lang" => "es",
                "date" => "2017-06-15",
                "description" => "Difusión del programa oficial con el detalle de las ponencias aceptadas."
            ],
            [
                "id" => 5,
                "lang" => "es",
                "date" => "2017-07-24",
                "description" => "Inscripciones e inauguración oficial IX Congreso AEB"
            ],
            [
                "id" => 6,
                "lang" => "es",
                "date" => "2017-07-25",
                "description" => "Desarrollo del congreso"
            ],
            [
                "id" => 7,
                "lang" => "es",
                "date" => "2017-07-28",
                "description" => "Clausura IX Congreso AEB"
            ],
            [
                "id" => 8,
                "lang" => "en",
                "date" => "2017-03-31",
                "description" => "Deadline for submitting proposals for papers."
            ],
            [
                "id" => 9,
                "lang" => "en",
                "date" => "2017-04-03",
                "description" => "Responses on acceptance of abstracts by the coordinators of each symposium."
            ],
            [
                "id" => 10,
                "lang" => "en",
                "date" => "2017-05-15",
                "description" => "Deadline to register for the IX Congress at a discount. Membership must also be paid."
            ],
            [
                "id" => 11,
                "lang" => "en",
                "date" => "2017-06-15",
                "description" => "Dissemination of the official program with the details of the accepted papers."
            ],
            [
                "id" => 12,
                "lang" => "en",
                "date" => "2017-07-24",
                "description" => "Registration and official opening IX AEB Congress"
            ],
            [
                "id" => 13,
                "lang" => "en",
                "date" => "2017-07-25",
                "description" => "Development of the congress"
            ],
            [
                "id" => 14,
                "lang" => "en",
                "date" => "2017-07-28",
                "description" => "Closing IX AEB Congress"
            ],
        ];

        foreach ($timelines as $timeline) {
            Timeline::create($timeline);
        }
    }
}

