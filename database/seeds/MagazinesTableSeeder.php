<?php

use App\Magazine;
use Illuminate\Database\Seeder;

class MagazinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('magazines')->delete();

        $magazines = [
            [
                "id" => 1,
                "lang" => "es",
                "title" => "Nueva Poesía Boliviana",
                "volume" => 10,
                "number" => 2,
                "date" => "2014-02-01",
                "content" => "<p>Special issue dedicated to poetry / Edición especial de poesía reciente boliviana.</p><p>To the memory of Steve Jacobs / A la memoria de Steve Jacobs</p>",
                "image_id" => 26
            ]
        ];

        foreach ($magazines as $magazine) {
            Magazine::create($magazine);
        }

    }
}
