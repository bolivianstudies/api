<?php

use App\Doc;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DocFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doc_feature')->delete();

        $docFeatures = [
            [
                "doc_id" => 22,
                "feature_id" => 7,
            ],
            [
                "doc_id" => 22,
                "feature_id" => 8,
            ],
            [
                "doc_id" => 22,
                "feature_id" => 9,
            ],
            [
                "doc_id" => 22,
                "feature_id" => 10,
            ],
            [
                "doc_id" => 22,
                "feature_id" => 11,
            ],
            [
                "doc_id" => 24,
                "feature_id" => 12,
            ],
            [
                "doc_id" => 24,
                "feature_id" => 13,
            ],
            [
                "doc_id" => 24,
                "feature_id" => 14,
            ],
            [
                "doc_id" => 24,
                "feature_id" => 15,
            ],
            [
                "doc_id" => 24,
                "feature_id" => 16,
            ],
            [
                "doc_id" => 25,
                "feature_id" => 17,
            ],
            [
                "doc_id" => 25,
                "feature_id" => 18,
            ],
            [
                "doc_id" => 25,
                "feature_id" => 19,
            ],
            [
                "doc_id" => 25,
                "feature_id" => 20,
            ],
            [
                "doc_id" => 25,
                "feature_id" => 21,
            ],
            [
                "doc_id" => 25,
                "feature_id" => 22,
            ],
            [
                "doc_id" => 27,
                "feature_id" => 23,
            ],
            [
                "doc_id" => 27,
                "feature_id" => 24,
            ],
            [
                "doc_id" => 27,
                "feature_id" => 25,
            ],
            [
                "doc_id" => 27,
                "feature_id" => 26,
            ],
            [
                "doc_id" => 27,
                "feature_id" => 27,
            ],
            [
                "doc_id" => 27,
                "feature_id" => 28,
            ],
            [
                "doc_id" => 28,
                "feature_id" => 29,
            ],
            [
                "doc_id" => 28,
                "feature_id" => 30,
            ],
            [
                "doc_id" => 29,
                "feature_id" => 31,
            ],
            [
                "doc_id" => 29,
                "feature_id" => 32,
            ],
            [
                "doc_id" => 29,
                "feature_id" => 33,
            ],
            [
                "doc_id" => 30,
                "feature_id" => 34,
            ],
            [
                "doc_id" => 30,
                "feature_id" => 35,
            ],
        ];

        foreach ($docFeatures as $docFeature) {
            $doc = Doc::find($docFeature["doc_id"]);
            $doc->features()->attach($docFeature["feature_id"], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }
}
