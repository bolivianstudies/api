<?php

use App\Directory;
use Illuminate\Database\Seeder;

class DirectoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directories')->delete();

        $directories = [
            [
                'id' => 2003,
                'en' => ['label' => 'Founding Executive Committee (2001-2003)'],
                'es' => ['label' => 'Comité ejecutivo fundador (2001-2003)']
            ],
            [
                'id' => 2005,
                'en' => ['label' => 'Second Executive Committee (2004-2005)'],
                'es' => ['label' => 'Segundo Comité Ejecutivo (2004-2005)']
            ],
            [
                'id' => 2007,
                'en' => ['label' => 'Third Executive Committee (2005-2007)'],
                'es' => ['label' => 'Tercer Comité Ejecutivo (2005-2007)']
            ],
            [
                'id' => 2008,
                'en' => ['label' => 'Fourth Executive Committee (2007-2008)'],
                'es' => ['label' => 'Cuarto Comité Ejecutivo (2007-2008)']
            ],
            [
                'id' => 2009,
                'en' => ['label' => 'Fifth Executive Committee (2008-2009)'],
                'es' => ['label' => 'Quinto Comité Ejecutivo (2008-2009)']
            ],
            [
                'id' => 2011,
                'en' => ['label' => 'Sixth Executive Committee (2009-2011)'],
                'es' => ['label' => 'Sexto Comité Ejecutivo (2009-2011)']
            ],
            [
                'id' => 2013,
                'en' => ['label' => 'Seventh Executive Committee (2011-2013)'],
                'es' => ['label' => 'Séptimo Comité Ejecutivo (2011-2013)']
            ],
            [
                'id' => 2015,
                'en' => ['label' => 'Eighth Executive Committee (2013-2015)'],
                'es' => ['label' => 'Octavo Comité Ejecutivo (2013-2015)']
            ],
            [
                'id' => 2017,
                'en' => ['label' => 'Ninth Executive Committee (2015-2017)'],
                'es' => ['label' => 'Noveno Comité Ejecutivo (2015-2017)']
            ],
        ];

        foreach ($directories as $directory) {
            Directory::create($directory);
        }
    }
}
