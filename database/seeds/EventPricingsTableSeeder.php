<?php

use App\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EventPricingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('event_pricing')->delete();

        $eventPricings = [
            [
                "event_id" => 1,
                "pricing_id" => 1,
                "order" => 1,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 2,
                "order" => 2,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 3,
                "order" => 3,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 4,
                "order" => 4,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 5,
                "order" => 5,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 6,
                "order" => 6,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 7,
                "order" => 7,
            ],
            [
                "event_id" => 1,
                "pricing_id" => 8,
                "order" => 8,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 1,
                "order" => 1,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 2,
                "order" => 2,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 3,
                "order" => 3,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 4,
                "order" => 4,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 5,
                "order" => 5,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 6,
                "order" => 6,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 7,
                "order" => 7,
            ],
            [
                "event_id" => 7,
                "pricing_id" => 8,
                "order" => 8,
            ],
        ];

        foreach ($eventPricings as $eventPricing) {
            $event = Event::find($eventPricing["event_id"]);
            $event->pricing()->attach($eventPricing["pricing_id"], ["order" => $eventPricing["order"], "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }
}

