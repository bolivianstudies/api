<?php

use App\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EventFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('event_feature')->delete();

        $eventFeatures = [
            [
                "event_id" => 1,
                "feature_id" => 1,
            ],
            [
                "event_id" => 1,
                "feature_id" => 2,
            ],
            [
                "event_id" => 1,
                "feature_id" => 3,
            ],
            [
                "event_id" => 7,
                "feature_id" => 4,
            ],
            [
                "event_id" => 7,
                "feature_id" => 5,
            ],
            [
                "event_id" => 7,
                "feature_id" => 6,
            ],
        ];

        foreach ($eventFeatures as $eventFeature) {
            $event = Event::find($eventFeature["event_id"]);
            $event->features()->attach($eventFeature["feature_id"], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }
}

