<?php

use App\Page;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PagePricingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('page_pricing')->delete();

        $pagePricings = [
            [
                "page_id" => 16,
                "pricing_id" => 1,
                "order" => 1,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 2,
                "order" => 2,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 3,
                "order" => 3,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 4,
                "order" => 4,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 5,
                "order" => 5,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 6,
                "order" => 6,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 7,
                "order" => 7,
            ],
            [
                "page_id" => 16,
                "pricing_id" => 8,
                "order" => 8,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 1,
                "order" => 1,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 2,
                "order" => 2,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 3,
                "order" => 3,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 4,
                "order" => 4,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 5,
                "order" => 5,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 6,
                "order" => 6,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 7,
                "order" => 7,
            ],
            [
                "page_id" => 27,
                "pricing_id" => 8,
                "order" => 8,
            ],
        ];

        foreach ($pagePricings as $pagePricing) {
            $page = Page::find($pagePricing["page_id"]);
            $page->pricing()->attach($pagePricing["pricing_id"], ["order" => $pagePricing["order"], "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }
}

