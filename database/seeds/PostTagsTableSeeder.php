<?php

use App\Post;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PostTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_tag')->delete();
        $postTags = [
            [
                "post_id" => 1,
                "tag_id" => 1,
            ],
            [
                "post_id" => 1,
                "tag_id" => 2,
            ],
            [
                "post_id" => 2,
                "tag_id" => 3,
            ],
            [
                "post_id" => 2,
                "tag_id" => 4,
            ],
            [
                "post_id" => 3,
                "tag_id" => 5,
            ],
            [
                "post_id" => 4,
                "tag_id" => 12,
            ],
            [
                "post_id" => 7,
                "tag_id" => 6,
            ],
            [
                "post_id" => 7,
                "tag_id" => 7,
            ],
            [
                "post_id" => 8,
                "tag_id" => 8,
            ],
            [
                "post_id" => 8,
                "tag_id" => 9,
            ],
            [
                "post_id" => 9,
                "tag_id" => 10,
            ],
            [
                "post_id" => 9,
                "tag_id" => 11,
            ],

        ];

        foreach ($postTags as $postTag) {
            $post = Post::find($postTag["post_id"]);
            $post->tags()->attach($postTag["tag_id"], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }
    }
}
