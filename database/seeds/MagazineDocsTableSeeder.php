<?php

use App\Magazine;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MagazineDocsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('magazine_doc')->delete();

        $magazineDocs = [
            [
                "magazine_id" => 1,
                "doc_id" => 20,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 21,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 22,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 23,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 24,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 25,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 26,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 27,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 28,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 29,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 30,
            ],
            [
                "magazine_id" => 1,
                "doc_id" => 31,
            ],
        ];

        foreach ($magazineDocs as $magazineDoc) {
            $magazine = Magazine::find($magazineDoc["magazine_id"]);
            $magazine->docs()->attach($magazineDoc["doc_id"], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }
}
