<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('faqs')->delete();

        $faqs = [
            [
                "id" => 1,
                "lang" => "es",
                "question" => "¿Qué necesito para inscribirme?",
                "answer" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consectetur commodo nunc, quis euismod elit aliquet sed. Integer nibh ex, ultricies eu nunc sed, pharetra lacinia mi. Fusce euismod consequat iaculis. Morbi quis nulla euismod, fringilla lectus sit amet, porta purus. Fusce in sodales urna. Donec tincidunt lorem id imperdiet feugiat.",
            ],
            [
                "id" => 2,
                "lang" => "es",
                "question" => "¿Cómo postulo para ser expositor?",
                "answer" => "Morbi vel lacus ut nunc ultricies pretium vel ut quam. Maecenas nec ex eu felis volutpat pharetra. Aenean nec nibh nec nisl egestas facilisis. Integer gravida rhoncus bibendum. Sed dapibus diam in ultricies gravida. Proin pretium urna vel tortor congue scelerisque.",
            ],
            [
                "id" => 3,
                "lang" => "es",
                "question" => "¿Dónde puedo acceder a los simposios?",
                "answer" => "Curabitur eget arcu sed velit ultricies imperdiet. Duis bibendum rutrum neque, a interdum purus gravida sit amet. Vestibulum egestas cursus eros. Aliquam ac massa vulputate, tincidunt nulla at, condimentum velit.",
            ],
            [
                "id" => 4,
                "lang" => "es",
                "question" => "¿Quiénes están a cargo de la AEB?",
                "answer" => "Phasellus augue libero, consectetur eu convallis a, imperdiet non justo. Aenean suscipit orci dolor, eget pellentesque leo commodo eu. Nulla aliquet tortor quis nisl malesuada, sed convallis felis lobortis. ",
            ],
            [
                "id" => 5,
                "lang" => "es",
                "question" => "¿Cuándo vence mi membresía?",
                "answer" => "Morbi aliquam nibh at neque sollicitudin, eget tristique mauris maximus. Pellentesque ligula magna, hendrerit nec sollicitudin a, porta ut purus.",
            ],
            [
                "id" => 6,
                "lang" => "es",
                "question" => "¿Cuánto cuestan las membresías?",
                "answer" => "Etiam eget dictum libero, id egestas felis. Pellentesque tincidunt ipsum vitae fermentum tempus. Vivamus eu posuere enim. Maecenas ac euismod tortor. Maecenas eleifend sed lectus ut porta. Maecenas cursus enim purus, vitae tempor nisl luctus at. ",
            ],
            [
                "id" => 7,
                "lang" => "es",
                "question" => "¿Dónde es el próximo congreso de la AEB?",
                "answer" => "Sed pretium ante id leo bibendum, id consectetur elit volutpat. Nam magna ante, ultrices ac porttitor a, tempor at nisl. Nam justo erat, egestas id sapien at, dictum imperdiet quam.",
            ],
            [
                "id" => 8,
                "lang" => "en",
                "question" => "What do I need to register?",
                "answer" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consectetur commodo nunc, quis euismod elit aliquet sed. Integer nibh ex, ultricies eu nunc sed, pharetra lacinia mi. Fusce euismod consequat iaculis. Morbi quis nulla euismod, fringilla lectus sit amet, porta purus. Fusce in sodales urna. Donec tincidunt lorem id imperdiet feugiat.",
            ],
            [
                "id" => 9,
                "lang" => "en",
                "question" => "How do I apply to be an exhibitor?",
                "answer" => "Morbi vel lacus ut nunc ultricies pretium vel ut quam. Maecenas nec ex eu felis volutpat pharetra. Aenean nec nibh nec nisl egestas facilisis. Integer gravida rhoncus bibendum. Sed dapibus diam in ultricies gravida. Proin pretium urna vel tortor congue scelerisque.",
            ],
            [
                "id" => 10,
                "lang" => "en",
                "question" => "Where can I access the symposia?",
                "answer" => "Curabitur eget arcu sed velit ultricies imperdiet. Duis bibendum rutrum neque, a interdum purus gravida sit amet. Vestibulum egestas cursus eros. Aliquam ac massa vulputate, tincidunt nulla at, condimentum velit.",
            ],
            [
                "id" => 11,
                "lang" => "en",
                "question" => "Who is in charge of the AEB?",
                "answer" => "Phasellus augue libero, consectetur eu convallis a, imperdiet non justo. Aenean suscipit orci dolor, eget pellentesque leo commodo eu. Nulla aliquet tortor quis nisl malesuada, sed convallis felis lobortis. ",
            ],
            [
                "id" => 12,
                "lang" => "en",
                "question" => "When does my membership expire?",
                "answer" => "Morbi aliquam nibh at neque sollicitudin, eget tristique mauris maximus. Pellentesque ligula magna, hendrerit nec sollicitudin a, porta ut purus.",
            ],
            [
                "id" => 13,
                "lang" => "en",
                "question" => "How much do memberships cost?",
                "answer" => "Etiam eget dictum libero, id egestas felis. Pellentesque tincidunt ipsum vitae fermentum tempus. Vivamus eu posuere enim. Maecenas ac euismod tortor. Maecenas eleifend sed lectus ut porta. Maecenas cursus enim purus, vitae tempor nisl luctus at. ",
            ],
            [
                "id" => 14,
                "lang" => "en",
                "question" => "Where is the next AEB congress?",
                "answer" => "Sed pretium ante id leo bibendum, id consectetur elit volutpat. Nam magna ante, ultrices ac porttitor a, tempor at nisl. Nam justo erat, egestas id sapien at, dictum imperdiet quam.",
            ]
        ];

        foreach ($faqs as $faq) {
            Faq::create($faq);
        }
    }
}
