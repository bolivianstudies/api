<?php

use App\Event;
use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('events')->delete();

        $events = [
            [
                "id" => 1,
                "lang" => "es",
                "title" => "XI Congreso de la AEB",
                "slug" => "congreso",
                "start_date" => "2017-03-05",
                "end_date" => "2017-03-07",
                "start_time" => "08:30:00",
                "end_time" => "19:00:00",
                "content" => "<p>Una de las principales actividades de la Asociación de Estudios Bolivianos (AEB) es el encuentro e intercambio en congresos académicos. El primer congreso se realizó en New Orleáns (2002), el segundo en La Paz (2003), el tercero en Miami (2005), el cuarto en Sucre (2006). En cada caso, acreditadas instituciones co-auspiciaron el evento: la Universidad de Loyola en New Orleáns, la Carrera de Literatura de la Universidad Mayor de San Andrés (UMSA), el Centro de América Latina y el Caribe de la Universidad Internacional de la Florida en Miami. En el año 2006, varias instituciones y sus coordinadores, desde diferentes puntos del país, apoyaron decididamente el cuarto congreso: el Archivo de La Paz (ALP), de la Universidad Mayor de San Andrés; Antropólogos del Sur Andino (ASUR), la Fundación Cultural La Plata, en Sucre; el Programa para la Investigación Estratégica en Bolivia (PIEB), el Institut de Recherche pour le Développement (IRD) en La Paz; el Instituto Francés de Estudios Andinos (IFEA) en Lima, así como representantes del CIDES (UMSA, La Paz), Universidad Católica Boliviana (La Paz), Universidad Nur (Santa Cruz) y, por supuesto, el Archivo y Biblioteca Nacionales de Bolivia en Sucre.</p>",
                "image_id" => 7,
                "location_id" => 1,
            ],
            [
                "id" => 7,
                "lang" => "en",
                "title" => "XI AEB Congress",
                "slug" => "congreso",
                "start_date" => "2017-03-05",
                "end_date" => "2017-03-07",
                "start_time" => "08:30:00",
                "end_time" => "19:00:00",
                "content" => "<p>Una de las principales actividades de la Asociación de Estudios Bolivianos (AEB) es el encuentro e intercambio en congresos académicos. El primer congreso se realizó en New Orleáns (2002), el segundo en La Paz (2003), el tercero en Miami (2005), el cuarto en Sucre (2006). En cada caso, acreditadas instituciones co-auspiciaron el evento: la Universidad de Loyola en New Orleáns, la Carrera de Literatura de la Universidad Mayor de San Andrés (UMSA), el Centro de América Latina y el Caribe de la Universidad Internacional de la Florida en Miami. En el año 2006, varias instituciones y sus coordinadores, desde diferentes puntos del país, apoyaron decididamente el cuarto congreso: el Archivo de La Paz (ALP), de la Universidad Mayor de San Andrés; Antropólogos del Sur Andino (ASUR), la Fundación Cultural La Plata, en Sucre; el Programa para la Investigación Estratégica en Bolivia (PIEB), el Institut de Recherche pour le Développement (IRD) en La Paz; el Instituto Francés de Estudios Andinos (IFEA) en Lima, así como representantes del CIDES (UMSA, La Paz), Universidad Católica Boliviana (La Paz), Universidad Nur (Santa Cruz) y, por supuesto, el Archivo y Biblioteca Nacionales de Bolivia en Sucre.</p>",
                "image_id" => 7,
                "location_id" => 1,
            ]
        ];

        foreach ($events as $event) {
            Event::create($event);
        }

    }
}

