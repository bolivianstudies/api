<?php

use App\Location;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('locations')->delete();

        $locations = [
            [
                "id" => 1,
                "address" => "ABNB, Biblioteca Pública Gunnar Mendoza, MUSEF-Sucre y la Casa de la Libertad",
                "location" => "Sucre, Departamento de Chuquisaca, Bolivia",
                "city" => "Sucre",
                "country_id" => "BO",
                "latitude" => "-19.0205659",
                "longitude" => "-65.2948117"
            ]
        ];

        foreach ($locations as $location) {
            Location::create($location);
        }

    }
}

