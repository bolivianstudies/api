<?php

use App\Director;
use Illuminate\Database\Seeder;

class DirectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('directors')->delete();

        $directors = [
            [
                "id" => 1,
                "name" => "Laurent Lacroix",
                "email" => "laurent.lacroix@ehess.fr",
                "image_id" => 22,
            ],
            [
                "id" => 2,
                "name" => "Gabriela Canedo",
                "email" => "gcanedovasquez@yahoo.com",
                "image_id" => 22,
            ],
            [
                "id" => 3,
                "name" => "Martín Mendoza-Botelho",
                "email" => "mendolona@netzero.net",
                "image_id" => 22,
            ],
            [
                "id" => 4,
                "name" => "Lucía Querejazu",
                "email" => "luciaquerejazu@yahoo.com",
                "image_id" => 22,
            ],
            [
                "id" => 5,
                "name" => "Josefa Salmon",
                "email" => "salmon@loyno.edu",
                "image_id" => 22,
            ],
            [
                "id" => 6,
                "name" => "Paola Revilla Orías",
                "email" => "paola.revillaor@gmail.com",
                "image_id" => 22,
            ],
            [
                "id" => 7,
                "name" => "Ana María Lema",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 8,
                "name" => "Andrea Barrero",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 9,
                "name" => "Annabelle Conroy",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 10,
                "name" => "Edmundo Paz Soldán",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 11,
                "name" => "Eduardo Gamarra",
                "email" => "",
                "image_id" => 22,
            ],[
                "id" => 12,
                "name" => "Elayne Zorn",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 13,
                "name" => "Emma Sordo",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 14,
                "name" => "Fernando Unzueta",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 15,
                "name" => "Françoise Martínez",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 16,
                "name" => "Gabriela Kuenzli",
                "email" => "",
                "image_id" => 22,
            ],[
                "id" => 17,
                "name" => "George Gray",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 18,
                "name" => "Guillermo Calvo Ayaviri",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 19,
                "name" => "Heather Thiessen-Riley",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 20,
                "name" => "Hernán Pruden",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 21,
                "name" => "Hugo Poppe Entrambasaguas",
                "email" => "",
                "image_id" => 22,
            ],[
                "id" => 22,
                "name" => "Isabel Scarborough",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 23,
                "name" => "John Redmann",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 24,
                "name" => "Marcela Inch",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 25,
                "name" => "Nicholas A. Robins",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 26,
                "name" => "Rob Albro",
                "email" => "",
                "image_id" => 22,
            ],[
                "id" => 27,
                "name" => "Rossana Barragán",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 28,
                "name" => "Stephen Jacobs",
                "email" => "",
                "image_id" => 22,
            ],
            [
                "id" => 29,
                "name" => "Tom Cruse",
                "email" => "",
                "image_id" => 22,
            ]
        ];

        foreach ($directors as $director) {
            Director::create($director);
        }
    }
}
