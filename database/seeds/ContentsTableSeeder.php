<?php

use App\Content;
use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('contents')->delete();

        $contents = [
            [
                "id" => 1,
                "title" => "Acerca de la AEB",
                "subtitle" => "",
                "slug" => "about",
                "content" => "<p>La Asociación de Estudios Bolivianos (AEB) es una organización sin fines de lucro y sin fines políticos cuyo propósito es promover e impulsar la investigación y la difusión del conocimiento sobre Bolivia. De acuerdo con esta misión, la AEB pretende ser un foro interdisciplinario y un recurso para fomentar la colaboración entre investigadores bolivianos y los del resto del mundo.</p>",
                "lang" => "es"
            ],
            [
                "id" => 2,
                "title" => "Modalidades de inscripción",
                "subtitle" => "Miembros nuevos",
                "slug" => "new-members",
                "content" => "<p>La membresía está abierta a todas las personas alrededor del mundo que compartan interés en Bolivia. La AEB contempla dos periodos de inscripción al año con la posibilidad de membresía anual y bi-anual.</p>",
                "lang" => "es"
            ],
            [
                "id" => 3,
                "title" => "Renovación de la membresía",
                "subtitle" => "Miembros actuales",
                "slug" => "current-members",
                "content" => "<p>Las personas que quieran renovar su membresía y pagar por la inscripción al congreso pueden realizar un solo pago siguiendo las <b>instrucciones descritas abajo de los costos de inscripción.</b></p>",
                "lang" => "es"
            ],
            [
                "id" => 4,
                "title" => "En Bolivia",
                "subtitle" => "Cómo inscribirse",
                "slug" => "bolivian",
                "content" => "<p>En Bolivia, por favor deposite el monto de la inscripción en la cuenta=> 201-50710848-3-60 del Banco de Crédito a nombre de Alba María Paz Soldán y Rowena Gabriela Canedo Vásquez.</p><p>Para tener constancia de la inscripción, los miembros deberán enviar una copia escaneada del comprobante y el formulario de membrecía que aparace abajo, al correo <a href='mailto=>aeb.bsa@gmail.com'>aeb.bsa@gmail.com</a></p>",
                "lang" => "es"
            ],
            [
                "id" => 5,
                "title" => "Fuera de bolivia",
                "subtitle" => "Cómo inscribirse",
                "slug" => "non-bolivian",
                "content" => "<p>Para pagos con tarjeta de crédito, débito o con información bancaria, por favor llene el formulario que aparece en la parte inferior de esta página.</p><blockquote>IMPORTANTE. Por favor a tiempo de enviar el pago especificar el propósito del mismo, por ejemplo=> “Membresía bi-anual categoría estudiante residiendo fuera de Latinoamérica y costo de inscripción al Congreso”.</blockquote><p>Si no puede pagar con tarjeta de crédito, también tiene la posibilidad de pagar con cheque. Para hacerlo de este modo, por favor llene formulario que aparece abajo, imprímalo y envíelo por correo junto con su cheque a=></p><blockquote>Bolivian Studies Association Prof. Martín Mendoza-Botelho<br> Department of Political Science<br> Eastern Connecticut State University<br> Willimantic, CT 06226 - USA </blockquote>",
                "lang" => "es"
            ],
            [
                "id" => 6,
                "title" => "About the AEB",
                "subtitle" => "",
                "slug" => "about",
                "content" => "<p>The Association of Bolivian Studies (AEB) is a non-profit and non-profit organization whose purpose is to promote and promote research and dissemination of knowledge about Bolivia. According to this mission, the AEB aims to be an interdisciplinary forum and a resource to foster collaboration between Bolivian researchers and those from the rest of the world.</p>",
                "lang" => "en"
            ],
            [
                "id" => 7,
                "title" => "Types of registration",
                "subtitle" => "New members",
                "slug" => "new-members",
                "content" => "<p>Membership is open to all people around the world who share interest in Bolivia. The AEB includes two enrollment periods per year with the possibility of annual and bi-annual membership.</p>",
                "lang" => "en"
            ],
            [
                "id" => 8,
                "title" => "Renewing Membership",
                "subtitle" => "Current members",
                "slug" => "current-members",
                "content" => "<p>People who want to renew their membership and pay for conference registration can make a single payment by following the <b>instructions outlined below.</b></p>",
                "lang" => "en"
            ],
            [
                "id" => 9,
                "title" => "In Bolivia",
                "subtitle" => "How to enroll",
                "slug" => "bolivian",
                "content" => "<p>In Bolivia, please deposit the amount of the registration in the account=> 201-50710848-3-60 of the Bank of Credit in the name of Alba María Paz Soldán and Rowena Gabriela Canedo Vásquez.</p> <p>To have record of Registration, members should send a scanned copy of the voucher and the membership form below, to <a href='mailto=>aeb.bsa@gmail.com'> aeb.bsa@gmail.com </a></p>",
                "lang" => "en"
            ],
            [
                "id" => 10,
                "title" => "Outside Bolivia",
                "subtitle" => "How to enroll",
                "slug" => "non-bolivian",
                "content" => "<p>For payments by credit card, debit card or bank information, please fill out the form at the bottom of this page. </p> <blockquote> IMPORTANT. Please, in time to send the payment specify the purpose of the same, for example=> 'Bi-annual membership category student residing outside Latin America and cost of registration to Congress.'</blockquote> <p>If you can not pay by card Credit, you also have the possibility to pay by check. To do so, please fill out the form below, print it and mail it along with your check to=> Bolivian Studies Association Prof. Martín Mendoza-Botelho Department of Political Science <br> > Eastern Connecticut State University in Willimantic, CT 06226 - USA </blockquote>",
                "lang" => "en"
            ]
        ];

        foreach ($contents as $content) {
            Content::create($content);
        }

    }
}

