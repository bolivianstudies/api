<?php

use App\Feature;
use Illuminate\Database\Seeder;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('features')->delete();

        $features = [
            [
                "id" => 1,
                "lang" => "es",
                "title" => "Participantes",
                "amount" => 350
            ],
            [
                "id" => 2,
                "lang" => "es",
                "title" => "Simposios",
                "amount" => 10
            ],
            [
                "id" => 3,
                "lang" => "es",
                "title" => "Expositores",
                "amount" => 20
            ],
            [
                "id" => 4,
                "lang" => "en",
                "title" => "Participants",
                "amount" => 350
            ],
            [
                "id" => 5,
                "lang" => "en",
                "title" => "Symposiums",
                "amount" => 10
            ],
            [
                "id" => 6,
                "lang" => "en",
                "title" => "Exhibitors",
                "amount" => 20
            ],
            [
                "id" => 7,
                "lang" => "es",
                "title" => "La Vita Nuova",
                "amount" => 0,
            ],
            [
                "id" => 8,
                "lang" => "es",
                "title" => "Tres poemas",
                "amount" => 0,
            ],
            [
                "id" => 9,
                "lang" => "es",
                "title" => "Caminar sobre la arena",
                "amount" => 0,
            ],
            [
                "id" => 10,
                "lang" => "es",
                "title" => "Ella tejía",
                "amount" => 0,
            ],
            [
                "id" => 11,
                "lang" => "es",
                "title" => "Cuando pase la lluvia",
                "amount" => 0,
            ],
            [
                "id" => 12,
                "lang" => "es",
                "title" => "Te cubres",
            ],
            [
                "id" => 13,
                "lang" => "es",
                "title" => "A las escondidas",
                "amount" => 0,
            ],
            [
                "id" => 14,
                "lang" => "es",
                "title" => "Urna en la nieve",
                "amount" => 0,
            ],
            [
                "id" => 15,
                "lang" => "es",
                "title" => "La muerte de Santos Marka Tula",
                "amount" => 0,
            ],
            [
                "id" => 16,
                "lang" => "es",
                "title" => "Alas",
                "amount" => 0,
            ],
            [
                "id" => 17,
                "lang" => "es",
                "title" => "QHANATATANXIWA (Ya está amaneciendo)",
                "amount" => 0,
            ],
            [
                "id" => 18,
                "lang" => "es",
                "title" => "LAYQA PHICHHITANKA (Brujita gorrión)",
                "amount" => 0,
            ],
            [
                "id" => 19,
                "lang" => "es",
                "title" => "KACHARPAYITA (Despídame)",
                "amount" => 0,
            ],
            [
                "id" => 20,
                "lang" => "es",
                "title" => "ROSAS PANQARA (Flor de rosa)",
                "amount" => 0,
            ],
            [
                "id" => 21,
                "lang" => "es",
                "title" => "JUKUMARI (El oso andino)",
                "amount" => 0,
            ],
            [
                "id" => 22,
                "lang" => "es",
                "title" => "SARNAQAÑA (La vida, SARAÑA Viajar)",
                "amount" => 0,
            ],
            [
                "id" => 23,
                "lang" => "es",
                "title" => "Los labios",
                "amount" => 0,
            ],
            [
                "id" => 24,
                "lang" => "es",
                "title" => "Una oscura señal",
                "amount" => 0,
            ],
            [
                "id" => 25,
                "lang" => "es",
                "title" => "Te hablo del silencio",
                "amount" => 0,
            ],
            [
                "id" => 26,
                "lang" => "es",
                "title" => "Amor",
                "amount" => 0,
            ],
            [
                "id" => 27,
                "lang" => "es",
                "title" => "Como un ave",
                "amount" => 0,
            ],
            [
                "id" => 28,
                "lang" => "es",
                "title" => "Por el regalo de la luz",
                "amount" => 0,
            ],
            [
                "id" => 29,
                "lang" => "es",
                "title" => "Horal",
                "amount" => 0,
            ],
            [
                "id" => 30,
                "lang" => "es",
                "title" => "El ejército se dispersa",
                "amount" => 0,
            ],
            [
                "id" => 31,
                "lang" => "es",
                "title" => "Pájaro carpintero en incendio",
                "amount" => 0,
            ],
            [
                "id" => 32,
                "lang" => "es",
                "title" => "Efímera ave",
                "amount" => 0,
            ],
            [
                "id" => 33,
                "lang" => "es",
                "title" => "Desnudez",
                "amount" => 0,
            ],
            [
                "id" => 34,
                "lang" => "es",
                "title" => "Polilla",
                "amount" => 0,
            ],
            [
                "id" => 35,
                "lang" => "es",
                "title" => "Zancudo",
                "amount" => 0,
            ]
        ];

        foreach ($features as $feature) {
            Feature::create($feature);
        }

    }
}

