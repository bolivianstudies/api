<?php

use App\PostComment;
use Illuminate\Database\Seeder;

class PostCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_comment')->delete();

        $postComments = [
            [
                "post_id" => 1,
                "author" => "Gabriel Barceló",
                "email" => "g.barcelo@gmail.com",
                "content" => "Cras et odio nisi. Ut accumsan ex non enim ullamcorper aliquet.",
                "created_at" => "2016-11-05 21:35:25"
            ],
            [
                "post_id" => 1,
                "author" => "Mariana Dotzauer",
                "email" => "mariana.dotzauer@hotmail.com",
                "content" => "In et rutrum nunc.",
                "created_at" => "2016-11-10 10:21:34"
            ],
            [
                "post_id" => 1,
                "author" => "Simón Bolivar",
                "email" => "simon_33@icloud.com",
                "content" => "Vivamus ut ante in diam eleifend vestibulum nec quis felis.",
                "created_at" => "2016-11-11 09:52:53"
            ],
            [
                "post_id" => 1,
                "author" => "Juana Azurduy",
                "email" => "juana.azurduy.p@gmail.com",
                "content" => "Nulla tincidunt, ipsum cursus iaculis gravida, risus ex finibus ante, non gravida ligula nisi et purus.",
                "created_at" => "2016-11-08 13:02:10"
            ],
            [
                "post_id" => 2,
                "author" => "Martín Villegas",
                "email" => "m.villegas@msn.com",
                "content" => "Nulla tincidunt, ipsum cursus iaculis gravida, risus ex finibus ante, non gravida ligula nisi et purus.",
                "created_at" => "2016-11-05 21:35:25"
            ],
            [
                "post_id" => 2,
                "author" => "Omar Palacios",
                "email" => "omar_p55@hotmail.es",
                "content" => "Donec tortor neque.",
                "created_at" => "2016-11-11 09:52:53"
            ]
        ];

        foreach ($postComments as $postComment) {
            PostComment::create($postComment);
        }
    }
}
