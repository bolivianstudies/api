<?php

use App\Pricing;
use Illuminate\Database\Seeder;

class PricingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('pricings')->delete();

        $pricings = [
            [
                "id" => 1,
                "price_usd" => 28,
                "price_bob" => 200,
                "en" => [
                    "membership" => "Bi-Annual",
                    "nationality" => "In Latin America",
                    "member" => "Professionals",
                    "description" => "Bi-annual membership for professionals in Latin America",
                ],
                "es" => [
                    "membership" => "Bi-Anual",
                    "nationality" => "En Latinoamérica",
                    "member" => "Profesionales",
                    "description" => "Membresía bi-anual para profesionales en Latinoamérica",
                ],
            ],
            [
                "id" => 2,
                "price_usd" => 12,
                "price_bob" => 80,
                "en" => [
                    "membership" => "Bi-Annual",
                    "nationality" => "In Latin America",
                    "member" => "Students",
                    "description" => "Bi-annual membership for students in Latin America",
                ],
                "es" => [
                    "membership" => "Bi-Anual",
                    "nationality" => "En Latinoamérica",
                    "member" => "Estudiantes",
                    "description" => "Membresía bi-anual para estudiantes en Latinoamérica",
                ],
            ],
            [
                "id" => 3,
                "price_usd" => 55,
                "price_bob" => 380,
                "en" => [
                    "membership" => "Bi-Annual",
                    "nationality" => "Outside Latin America",
                    "member" => "Professionals",
                    "description" => "Bi-annual membership for professionals outside Latin America",
                ],
                "es" => [
                    "membership" => "Bi-Anual",
                    "nationality" => "Fuera de Latinoamérica",
                    "member" => "Profesionales",
                    "description" => "Membresía bi-anual para profesionales fuera de Latinoamérica",
                ],
            ],
            [
                "id" => 4,
                "price_usd" => 35,
                "price_bob" => 240,
                "en" => [
                    "membership" => "Bi-Annual",
                    "nationality" => "Outside Latin America",
                    "member" => "Students",
                    "description" => "Bi-annual membership for students outside Latin America",
                ],
                "es" => [
                    "membership" => "Bi-Anual",
                    "nationality" => "Fuera de Latinoamérica",
                    "member" => "Estudiantes",
                    "description" => "Membresía bi-anual para estudiantes fuera de Latinoamérica",
                ],
            ],
            [
                "id" => 5,
                "price_usd" => null,
                "price_bob" => 120,
                "en" => [
                    "membership" => "Exhibitors",
                    "nationality" => "In Bolivia",
                    "member" => "Professionals",
                    "description" => "Congress inscription for professionals as exhibitors",
                ],
                "es" => [
                    "membership" => "Expositores",
                    "nationality" => "En Bolivia",
                    "member" => "Profesionales",
                    "description" => "Inscripción al congreso para profesionales como expositores",
                ],
            ],
            [
                "id" => 6,
                "price_usd" => null,
                "price_bob" => 70,
                "en" => [
                    "membership" => "Exhibitors",
                    "nationality" => "In Bolivia",
                    "member" => "Students",
                    "description" => "Congress inscription for students as exhibitors",
                ],
                "es" => [
                    "membership" => "Expositores",
                    "nationality" => "En Bolivia",
                    "member" => "Estudiantes",
                    "description" => "Inscripción al congreso para estudiantes como expositores",
                ],
            ],
            [
                "id" => 7,
                "price_usd" => null,
                "price_bob" => 80,
                "en" => [
                    "membership" => "Certified attendee",
                    "nationality" => "In Bolivia",
                    "member" => "Professionals",
                    "description" => "Congress inscription for professionals as certified attendees",
                ],
                "es" => [
                    "membership" => "Público con certificación",
                    "nationality" => "En Bolivia",
                    "member" => "Profesionales",
                    "description" => "Inscripción al congreso para profesionales como público con certificación",
                ],
            ],
            [
                "id" => 8,
                "price_usd" => null,
                "price_bob" => 50,
                "en" => [
                    "membership" => "Certified attendee",
                    "nationality" => "In Bolivia",
                    "member" => "Students",
                    "description" => "Congress inscription for students as certified attendees",
                ],
                "es" => [
                    "membership" => "Público con certificación",
                    "nationality" => "En Bolivia",
                    "member" => "Estudiantes",
                    "description" => "Inscripción al congreso para estudiantes como público con certificación",
                ],
            ],
        ];

        foreach ($pricings as $pricing) {
            Pricing::create($pricing);
        }
    }
}

