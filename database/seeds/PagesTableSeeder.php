<?php

use App\Page;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('pages')->delete();

        $pages = [
            [
                "id" => 1,
                "order" => 1,
                "title" => "Inicio",
                "excerpt" => "",
                "content" => "",
                "slug" => "home",
                "lang" => "es",
                "image_id" => 1
            ],
            [
                "id" => 2,
                "order" => 2,
                "title" => "Nosotros",
                "slug" => "nosotros",
                "lang" => "es",
                "image_id" => 1,
                "children" => [
                    [
                        "id" => 13,
                        "order" => 1,
                        "title" => "Sobre la AEB",
                        "excerpt" => "",
                        "content" => "<p>La Asociación de Estudios Bolivianos (AEB) es una organización sin fines de lucro y sin fines políticos cuyo propósito es promover e impulsar la investigación y la difusión del conocimiento sobre Bolivia. De acuerdo con esta misión, la AEB pretende ser un foro interdisciplinario y un recurso para fomentar la colaboración entre investigadores bolivianos y los del resto del mundo.</p><p>La AEB organiza un congreso internacional y transdisciplinario que reúne cada dos años a bolivianistas de diferentes partes del mundo.</p><p>Para contribuir a la difusión de la actualidad de la investigación bolivianista la AEB publica una revista semestral, la Revista de Estudios Bolivianos / Bolivian Research Review (ver pestaña <a href='publicaciones/revista'>Revista AEB</a>), actualiza regularmente noticias sobre publicaciones y otras actividades en su página Facebook, así como en su página web (ver pestaña <a href='noticias'>Novedades bolivianistas</a> y <a href='publicaciones/novedades-bibliograficas'>Publicaciones</a>).</p><p>La AEB está abierta a todos aquellos que estén interesados en estudiar Bolivia y los invita cordialmente a formar parte de su colectivo.</p>",
                        "slug" => "aeb",
                        "lang" => "es",
                        "image_id" => 1
                    ],
                    [
                        "id" => 14,
                        "order" => 2,
                        "title" => "Comité Ejecutivo",
                        "excerpt" => "",
                        "content" => "<p>Las elecciones para el Comité Ejecutivo de la AEB se llevan a cabo cada dos años durante el congreso internacional de la Asociación. Para ser candidato se necesita haber sido socio por lo menos un año antes de presentar la candidatura.</p>",
                        "slug" => "mesa-directiva",
                        "lang" => "es",
                        "image_id" => 2
                    ],
                    [
                        "id" => 15,
                        "order" => 3,
                        "title" => "Estatutos",
                        "excerpt" => "",
                        "content" => "<h2>Artículo 1:</h2><h1>Nombre</h1><ol><li>El nombre de esta asociación será la Asociación de Estudios Bolivianos con la sigla AEB. En este documento también está referido como la Asociación.</li><li>La AEB es y seguirá siendo una corporación sin fines de lucro y permanencería calificada libre de impuestos federales bajo la sección 501 (c) 3 del código interno de rentas públicas.</li><li>La AEB es y seguirá siendo una organización no sectaria, no política y no participará en la promoción política.</li></ol><h2>Artículo 2:</h2><h1>La Misión</h1><p>La misión y objetivo de la AEB es avanzar y promover la investigación y el conocimiento de Bolivia creando un foro interdisciplinario y recursos para la colaboración, investigación, estudio, financiación y las oportunidades de empleo.</p><h2>Artículo 3:</h2><h1>Composición</h1><ol><li>La membresía de la Asociación está abierta a todos los que comparten un interés en Bolivia, basado en el esquema de tarifas elaborados por el Consejo Ejecutivo.</li><li>Miembros de pleno derecho pueden votar y participar en el Consejo Ejecutivo, como oficiales de la Asociación, y como ponentes y participantes de los congresos.</li><li>Para calificar como miembro estudiante, un individuo debe estar matriculado y en proceso de conseguir un título universitario. Un individuo solamente puede mantener esta categoría durante un periodo máximo de siete años.</li></ol><h2>Artículo 4:</h2><h1>Las cuotas</h1><p>El Consejo Ejecutivo puede establecer y modificar las cuotas por un voto de la mayoría en apoyo de tales cambios. El Consejo Ejecutivo podrá establecer tasas diferentes para categorías especiales de miembros.</p><h2>Artículo 5:</h2><h1>Los oficiales y el Consejo Ejecutivo</h1><ol><li>Los oficiales serán un Presidente, Vice-Presidente y Tesorero.</li><li>El Consejo Ejecutivo, cuyos miembros tienen voto, administran los asuntos de la AEB, y por propósitos de la corporación serán considerado su Consejo Directivo.</li><li>Desde el primero de enero del 2001 hasta el primero de enero del 2003, La Asociación de Estudios Bolivianos fue gobernada bajo una estructura provisoria por un Consejo Ejecutivo de fundadores formado por un Presidente, Vice-Presidente y tres miembros de pleno derecho con voz y voto. En enero del 2002, otros dos miembros con voz y voto fueron elegidos al Consejo Ejecutivo por un mandato de dos años.</li><li>Todos los miembros del Consejo Ejecutivo tendrán un mandato de dos años y servirán en forma escalonada, comenzando el primero de enero. No hay reelección del presidente, sin embargo miembros del Consejo Ejecutivo podrían ser reelegidos consecutivamente por no más de dos mandatos.</li><li>Algunos miembros del Consejo Ejecutivo, con la excepción del Presidente y Vice-Presidente, individualmente tienen la responsabilidad de desarrollar, gestionar y mejorar áreas específicas. Estos incluyen, pero no se limitan a responsabilidades asociadas a la revista electrónica de la AEB (conjuntamente con un Consejo Editorial), la Revista de Investigaciones Bolivianas, el boletín, la organización de conferencias, la recaudación de fondos y el desarrollo estratégico institucional.</li><li>El Consejo Ejecutivo se esforzará por asegurar que por lo menos tres disciplinas estén representadas en el Consejo Ejecutivo en todo momento.</li><li>El Presidente y el Consejo Ejecutivo realizarán los propósitos de la AEB y promoverán los intereses profesionales de ésta.</li><li>El Consejo Ejecutivo realizará y supervisará los asuntos de la Asociación, recibirá donaciones, subsidios y ejecutará el presupuesto anual, y actuará en beneficio de la Asociación.</li><li>Los miembros del Consejo Ejecutivo están obligados a respetar el carácter interdisciplinario y multinacional de la AEB, y trabajar para lograr la diversidad por regiones y disciplinas. También, ellos deben asegurar que la AEB no sea utilizada para avanzar o promover objectivos políticos o religiosos, convicciones o ideologías. La falta de hacerlo será causa para la destitución del cargo.</li><li>La función primaria del Presidente y Vice-Presidente es avanzar la misión de la AEB, y coordinar la labor del Consejo Ejecutivo y de los miembros para estos fines.</li><li>El Presidente actuará como director del Consejo Ejecutivo y será responsable por la preparacíon del presupuesto anual y el informe de finanzas anuales para la presentación al Consejo Ejecutivo para su examinación.</li><li>El Presidente, con el apoyo de la mayoría del Consejo Ejecutivo, podrá designar los comités que se consideren útil en beneficio de la misión de la Asociación.</li><li>El Presidente mantendrá una lista electrónica de todos los miembros de la AEB, de libre acceso vía el Internet a los miembros de la AEB.</li><li>El Presidente convocará reuniones del Consejo Ejecutivo, a las que podrán ser asistidas por teléfono por lo menos una vez al año, y será requerida por petición escrita de cuatro miembros de Consejo Ejecutivo.</li><li>El Presidente será responsable de la distribución, el recuento y la presentación al Consejo Ejecutivo y los miembros los resultados de todos los votos a favor de posiciones y enmiendas.</li><li>El Tesorero es el oficial principalmente responsable para la supervisión de los asuntos financieros de la Asociación. El Tesorero debería elaborar, revisar y reportar los informes financieros trimestrales al Consejo Ejecutivo.</li><li>Destitución <ul><li>A. En caso de que un miembro del Consejo Ejecutivo falte en persona o por teléfono a dos reuniones consecutivas del Consejo Ejecutivo, ese miembro dejará el cargo y una elección se llevará a cabo de acuerdo a los procedimientos generales descirtos en este documento para llenar el puesto vacante.</li><li>B. En caso de ausencia, muerte, renuncia o incapacidad del Presidente, sus funciones deberían correr a cargo del Vice-Presidente, quien completará el período del Presidente anterior.</li><li>C. En caso de que el Vice-Presidente no pueda o no quiera servir de Presidente, o en caso de ausencia, muerte, renuncia o incapacidad del Vice-Presidente, el Consejo Ejecutivo eligirá un miembro del Consejo Ejecutivo para completar el mandato del previo Presidente.</li><li>D. En caso de que el Vice-Presidente asuma el cargo de Presidente por causa de ausencia, muerte, renuncia o incapacidad del Presidente, el Consejo Ejecutivo nombrará un miembro del Consejo Ejecutivo para servir como Vice-Presidente para completar el mandato del previo Vice-Presidente.</li></ul></li><li>Destitución <br> Se podrá destituir a un funcionario o miembro del Comité Ejecutivo mediante demanda electrónica o escrita firmada y remitida por dos tercios de los miembros de la Asociación. En caso de que un funcionario o miembro del comité infringiera el reglamento de la Asociación, éste podrá ser depuesto con la votación de cinco de los siete miembros del Comité Ejecutivo. En el supuesto caso de que el número de miembros del Comité Ejecutivo sea superior, el proceso de destitución requerirá el apoyo de dos tercios del los miembros del Comité. Los miembros del Consejo pueden ser depuestos a través del voto mayoritario del Comité Ejecutivo.</li><li>Indemnización <br> Cualquier persona en la condición de ex funcionario sujeta a un juicio o proceso legal será indemnizada por la Asociación para compensar los gastos justificados que hayan sido realizados por el ex funcionario en el proceso legal o juicio, exceptuando los casos en los que el ex funcionario haya sido responsable de negligencia o mal ejercicio de sus funciones.</li></ol><h2>Articulo 6:</h2><h1>Elecciones.</h1><ol><li>Durante los dos primeros años posteriores a la fundación de la AEB, por motivos administrativos desde el 1ro de enero 2001 hasta el 1ro de enero 2003, se conforma un gobierno interino que facilita el desenvolvimiento de la organización y la rotación de autoridades. Durante este periodo, los fundadores de la AEB se constituyen como Presidente y Vicepresidente, quienes al cabo de un año de gestión oficial mantendrán el título de Presidente Vitalicio. En enero de 2001, ellos nominarán, a través del voto mayoritario del Comité Ejecutivo, y eligirán tres miembros adicionales para el Comité Ejecutivo fundador. En enero de 2002, ellos nominrán, a través del voto mayoritario del Comité Ejecutivo, y eligirán dos miembros adicionales para el Comité Ejecutivo y así facilitar la rotación de autoridades.</li><li>Posterior al 1ro de enero de 2003, el Presidente, el Vicepresidente, el Tesorero y el resto de los miembros del Comité Ejecutivo serán elegidos por votación por la mayoría de los integrantes de la AEB durante una elección. El primer y segundo ex presidentes de la AEB son parte del Comité Ejecutivo por un periodo de dos años después de sus respectivas gestiones como presidentes.</li><li>Los aspirantes a todos los cargos pueden ser nominados por otros miembros o postularse por su cuenta, sin embargo todas las nominaciones deben ser recibidas por el Comité ejecutivo en un plazo máximo de dos meses antes de la elección. Todos los aspirantes pueden presentar a los miembros una carta de no más de 350 palabras. Tal documento será distribuido por escrito y electrónicamente a todos los miembros en un plazo no superior a seis semanas antes de la votación.</li><li>Después del 1ro de enero de 2003, los candidatos al Comité Ejecutivo deben tener un buen expediente de ejercicio activo por al menos 1 año antes de postularse al cargo.</li><li>Las elecciones para Presidente de la AEB y otros puestos vacantes serán llevadas a cabo entre el 10 de octubre al 15 de octubre de 2002, para un periodo de dos años comenzando el 1ro de enero de 2003. La votación cierra a las 11:50 pm hora central de Estados Unidos, el 15 de Octubre de cada elección. Los posteriores votos para la presidencia serán conducidos de la misma manera cada dos años.</li><li>La votación será llevada a cabo electrónicamente vía email, y los resultados serán anunciados por el Presidente de la AEB a todos los miembros vía email hasta el 17 de Octubre del año de elección. La dirección de correo electrónico debe estar registrada en referencia a cada votante.</li><li>La elección de los miembros del Comité Ejecutivo no tendrá lugar antes de las seis semanas a priori de la conclusión del mandato del puesto a ser ocupado. La votación será realizada electrónicamente y los resultados serán comunicados a todos los miembros dentro de las 48 hrs. posteriores al cierre de la votación.</li><li>8. La elección de los candidatos es garantizada por la mayoría de los miembros votantes, por consiguiente puede llevarse a cabo una segunda vuelta de ser necesario. Esta debe llevarse a cabo dos semanas después de que los resultados de las primeras elecciones se den a conocer. En esta instancia los votos serán realizados en un periodo de cinco días, cerrando el quinto día a las 11:59 p.m., hora central de los Estados Unidos, los resultados serán dados a conocer dentro de las 48 posteriores al cierre de la votación.</li></ol><h2>Articulo 7:</h2><h1>Política y Designación de los integrantes del Consejo Consultivo.</h1><p>El Comité Ejecutivo de la AEB y cualquiera de los integrantes pueden acudir a los recursos de un Consejo Consultivo. Si bien son designados por el Comité Ejecutivo, cualquier miembro de la AEB puede nombrar postulantes para el Consejo Consultivo. Los integrantes de esta instancia servirán por un periodo de dos años, aunque el puesto en el Consejo puede ser renovado posteriormente por voto mayoritario del Comité Ejecutivo. Todos los ex presidentes que no son miembros del Comité Ejecutivo se convierten en miembros del Consejo Consultivo automáticamente por un período de cuatro años. </p><h2>Articulo 8:</h2><h1>Creación de los Comités</h1><ol><li>El Comité Ejecutivo, por voto mayoritario, puede crear comités para fines específicos cuya duración no debe exceder un año. El Presidente del Comité Ejecutivo designará al presidente e integrantes de los comités y entre sus miembros debe incluirse a una persona que integre el Comité Ejecutivo.</li><li>El presidente de cada comité deberá preparar un informe dirigido al Comité Ejecutivo antes del final de la gestión comitiva o en su defecto a petición del Comité Ejecutivo.</li><li>Los nombres de los integrantes del comité serán dados a conocer a todos los miembros de la AEB a través de una publicación en la página web de la AEB.</li><li>Ningún comité podrá crear sub comités o solicitar fondos sin la autorización escrita del Presidente.</li></ol><h2>Artículo 9:</h2><h1>Congreso</h1><ol><li>La AEB llevará a cabo un congreso cada 18 meses.</li><li>De dos congresos, uno debe ser realizado en Bolivia.</li><li>Las secciones son creadas a partir de la solicitud escrita de cualquier miembro del Comité Ejecutivo. Se debe demostrar que la sección propuesta tiene por lo menos 10 integrantes.</li><li>Los directores de cada sección ejercerán este cargo por un periodo de dos años, sin posibilidad de reelección.</li><li>Los paneles, que pueden ser propuestos por los miembros, deben tener al menos cuatro integrantes confirmados además del moderador/analista.</li></ol><h2>Artículo 10:</h2><h1>Juntas de trabajo</h1><ol><li>En cada congreso se realizará una junta de trabajo, durante el cual los miembros con buenos expedientes de ejercicio pueden someter a votación asuntos de acuerdo al reglamento.</li><li>Ninguna modificación al reglamento será aprobado durante las junta de trabajo.</li><li>La junta de trabajo incluirá un informe del Presidente sobre el estado de la Asociación y los proyectos para los siguientes 18 meses. Al mismo tiempo incluirán informes de los comités y del Tesorero.</li><li>La agenda de la junta de trabajo será elaborada por el Presidente de la AEB.</li><li>Habrá tiempo adecuado para discusión y exposición durante la junta de trabajo.</li><li>Cualquier acción legislativa, con la excepción de las modificaciones de los Estatutos, que se inicie en la reunión de trabajo será presentada a todos los miembros para la votación electrónica y se requerirá una mayoría de votantes para apoyar esa acción.</li><li>Las actas de las Juntas de Trabajo se ajustarán a las Reglas de Orden Robert, recientemente revisado.</li><li>Para todos los votos en la reunión de negocios se requiere un quórum, que estará integrado por el 15% de los miembros inscritos en el Congreso.</li><li>En cada ocasión, para la votación, el oficial que preside dicha votación determinará si hay quórum, y los votantes tendrán las opciones del voto, sí, no, y de abstenerse.</li><li>Los miembros pueden solicitar que se incluyan propuestas temáticas en el programa de la Reunión de Trabajo, pero esas solicitudes se deben recibir por lo menos 45 días antes de la reunión. Estas solicitudes deberán ser respetadas a menos que haya una razón de peso para no incluirlas en la junta.</li></ol><h2>Artículo 11:</h2><h1>La Revista de Investigaciones sobre Bolivia</h1><ol><li>La Revista de Investigaciones sobre Bolivia sera la revista oficial de la Asociación.</li><li>El nombramiento del editor o editors será hecho de acuerdo al voto mayoritario de los miembros del Comite Ejecutivo.</li><li>3El editor/los editors servirá(n) por un period de dos años. Este periodo podrá ser renovado una vez consecutivamente.</li><li>El/los editors serán responsables de la solicitud de manuscritos, de su evaluación y de la preparación final para su distribución a todos los miembros.</li><li>La <strong>Revista de Investigaciones sobre Bolivia</strong> será publicada y distribuída a todos los miembros cada seis meses y estará disponible en el sitio web de la AEB.</li><li>El/los editors podrán nombrar miembros del comité editorial para que ayuden en la evaluación y publicación de la revista.</li><li>La Revista de Investigaciones sobre Bolivia se guiará por las mismas reglas que la Asociación.</li></ol><h2>Artículo 12:</h2><h1>Conducta Profesional</h1><p>Es obligatorio que todos los miembros del Comite Ejecutivo y de la Junta Consultiva conduzcan sus acciones profesionales de una forma que demuestre respeto por la AEB como organización y por el estatus y derechos de todos sus miembros independiente de género, edad, nacionalidad, orientación sexual, etnicidad, raza o creencia. </p><h2>Artículo 13:</h2><h1>Enmiendas</h1><ol><li>Enmiendas al estatuto de la Asociación pueden ser propuestas por el Comité Ejecutivo con el apoyo de cinco de los siete miembros con voto de dicho Comité o por petición de un tercio de los miembros de la Asociación. En caso que se desee expandir el Comite Ejecutivo, esta propuesta deberia ser aprobada por dos tercios del Comité Ejecutivo.</li><li>Enmiendas bajo consideración deben ser distribuidas a los miembros con bastante anticipación.</li><li>La ratificación de enmiendas requiere la aprobación de tres cuartos de todos los miembros activos.</li><li>Durante la propuesta y el proceso de ratificación, aquellos que se oponen a dicha enmienda deberán tener igual oportunidad para ser escuchados y presentar sus objecciones y razones para rechazar dichas enmiendas.</li><li>Habrá un periodo de 60 días entre la propuesta de una enmienda y su voto y todos los miembros deberian estar al tanto de dicha propuesta de enmienda.</li><li>Las fechas de la votación deben ser claramente indicadas por correspondencia especial con los miembros sobre las enmiendas propuestas.</li><li>Los elementos centrales de la Asociacion de ser una asociación apolítica, sin fines de lucro, y no-sectaria no pueden ser objeto de enmiendas.</li></ol><h2>Artículo 14:</h2><h1>Auditoría Annual</h1><p>En el evento que se tenga que disolver la Asociación de Estudios Bolivianos, los fondos serán distribuidos para uno o más de los propósitos exentos dentro del significado de la sección 501 (C)(3) del Código de Rentas Públicas (Internal Revenue), o la sección correspondiente de codigos futuros de impuestos, o será distribuido al gobierno federal, o al gobierno estatal o local para un fin público. Cualquier fondo no dispuesto de dicha manera sera dispuesto por la Corte de Competent Jurisdiction del condado en la que reside la sede principal de la corporación, para uso exclusivo de dichos propósitos o a dicha organización u organizaciones según dicha Corte determine, que estén organizadas y exclusivamente operen para dichos fines. </p><h2>Artículo 15:</h2><h1>Disolución</h1><p>En caso de la disolución de la AEB, los bienes serán distribuídos de acuerdo a las reglas de la sección 501 (C)(3) del Código de la Renta Interna de EEUU, o la sección correspondiente del código que esté vigente en ese momento, o sera distribuído al gobierno federal, o a un gobierno local o estatal para cualquier propósito público. Cualquier bien no distribuído de esta manera sera distribuído por la Corte que tenga jurisdicción en el condado donde se encuentre la oficina principal de la corporación, de acuerdo a lo que la Corte determine.</p><p>Hemos suscrito aquí nuestros nombres este día del mes de del año 2002.</p><hr><p>Josefa Salmón Nicholas Robins</p><p>Presidenta Vice Presidente </p><hr><p>Hugo Poppe Entrambasaguas Ana Rebeca Prada</p><p>Director Director </p><p>John Redman</p><p>Director</p>",
                        "slug" => "estatutos",
                        "lang" => "es",
                        "image_id" => 3
                    ],
                    [
                        "id" => 16,
                        "order" => 4,
                        "title" => "Membresía",
                        "excerpt" => "",
                        "content" => "<p>Ser miembro de la AEB tiene varios beneficios. En primer lugar, la inscripción les permite a los miembros participar en el congreso que se celebra cada dos años tanto como panelistas, asi como organizadores de mesas redondas o paneles. Los miembros de la AEB también tienen accesso a los últimos numerous de la RevistaE y el Boletín.</p><h2>Miembros Nuevos</h2><h1>Modalidades de inscripción</h1><p>La membresía está abierta a todas las personas alrededor del mundo que compartan interés en Bolivia. La AEB contempla dos periodos de inscripción al año con la posibilidad de membresía anual y bi-anual.</p><h2>Miembros Actuales</h2><h1>Renovación de la Membresía</h1><p>Las personas que quieran renovar su membresía y pagar por la inscripción al congreso pueden realizar un solo pago siguiendo las instrucciones descritas abajo de los costos de inscripción.</p><h2>Cómo Inscribirse</h2><h1>En Bolivia</h1><p>En Bolivia, por favor deposite el monto de la inscripción en la cuenta=> 201-50710848-3-60 del Banco de Crédito a nombre de Alba María Paz Soldán y Rowena Gabriela Canedo Vásquez.</p><p>Para tener constancia de la inscripción, los miembros deberán enviar una copia escaneada del comprobante y el formulario de membrecía que aparace abajo, al correo <a href='mailto:aeb.bsa@gmail.com'>aeb.bsa@gmail.com</a></p><h2>Cómo Inscribirse</h2><h1>Fuera de Bolivia</h1><p>Para pagos con tarjeta de crédito, débito o con información bancaria, por favor llene el formulario que aparece en la parte inferior de esta página.</p><blockquote>IMPORTANTE. Por favor a tiempo de enviar el pago especificar el propósito del mismo, por ejemplo: “Membresía bi-anual categoría estudiante residiendo fuera de Latinoamérica y costo de inscripción al Congreso”.</blockquote><p>Si no puede pagar con tarjeta de crédito, también tiene la posibilidad de pagar con cheque. Para hacerlo de este modo, por favor llene formulario que aparece abajo, imprímalo y envíelo por correo junto con su cheque a:</p><blockquote>Bolivian Studies Association Prof. Martín Mendoza-Botelho<br>Department of Political Science<br>Eastern Connecticut State University<br>Willimantic, CT 06226 - USA</blockquote>",
                        "slug" => "membresia",
                        "lang" => "es",
                        "image_id" => 4
                    ],
                    [
                        "id" => 17,
                        "order" => 5,
                        "title" => "Miembros",
                        "excerpt" => "",
                        "content" => "",
                        "slug" => "miembros",
                        "lang" => "es",
                        "image_id" => 5
                    ]
                ]
            ],
            [
                "id" => 3,
                "order" => 3,
                "title" => "Congreso",
                "excerpt" => "",
                "content" => "",
                "slug" => "event({id:'congreso'})",
                "lang" => "es",
                "image_id" => 1
            ],
            [
                "id" => 4,
                "order" => 4,
                "title" => "Novedades Bolivianistas",
                "excerpt" => "",
                "content" => "",
                "slug" => "posts",
                "lang" => "es",
                "image_id" => 1
            ],
            [
                "id" => 5,
                "order" => 5,
                "title" => "Publicaciones",
                "excerpt" => "",
                "content" => "",
                "slug" => "publicaciones",
                "lang" => "es",
                "image_id" => 1,
                "children" => [
                    [
                        "id" => 19,
                        "order" => 1,
                        "title" => "Novedades Bibliográficas",
                        "excerpt" => "",
                        "content" => "",
                        "slug" => "novedades-bibliograficas",
                        "lang" => "es",
                        "image_id" => 4
                    ],
                    [
                        "id" => 20,
                        "order" => 2,
                        "title" => "Revista AEB",
                        "excerpt" => "",
                        "content" => "<p>La Revista Boliviana de Investigación (Bolivian Research Review) es una revista electrónica bi-anual e interdisciplinaria dedicada a promover el avance de la investigación y el conocimiento sobre Bolivia. La Revista de Investigaciones sobre Bolivia se publica con la ayuda de la <a href=\"http://library.loyno.edu/\" target=\"_blank\">Biblioteca J. Edgar and Louise S. Monroe</a> de la <a href=\"http://www.loyno.edu\">Universidad de Loyola, New Orleans</a>, USA. La actual directora de la Revista es la Dra. Josefa Salmón y Oscar Vega Camacho. </p><p>La Revista Boliviana de Investigación invita la propuesta de artículos en cualquier disciplina, ya sea en español, aymara, quechua, guaraní o ingles. Los trabajos deberán ser inéditos y no deben estar actualment bajo consideración en otra revista. Por lo general, los artículos no deben exceder las 10,000 palabras. El Comité Editorial evaluará anónimamente y decidirá la publicación de los trabajos sometidos. La revista también publicará reseñas de libros recientes y anuncios sobre Bolivia.</p><h3>Recepción y admisión de artículos</h3><p>Si está interesado en proponer un artículo o formar parte del comité editorial escriba a la Dra. Josefa Salmón, <a href=\"mailto:salmon@loyno.edu\"><b>salmon@loyno.edu</b></a>. Guía para las contribuciones.</p>",
                        "slug" => "revista",
                        "lang" => "es",
                        "image_id" => 5
                    ]
                ]
            ],
            [
                "id" => 6,
                "order" => 6,
                "title" => "Contacto",
                "slug" => "contacto",
                "lang" => "es",
                "image_id" => 1,
                "children" => [
                    [
                        "id" => 22,
                        "order" => 1,
                        "title" => "Escríbenos",
                        "excerpt" => "",
                        "content" => "",
                        "slug" => "escribenos",
                        "lang" => "es",
                        "image_id" => 2
                    ]
                ]
            ],
            [
                "id" => 7,
                "order" => 1,
                "title" => "Home",
                "excerpt" => "",
                "content" => "",
                "slug" => "home",
                "lang" => "en",
                "image_id" => 1
            ],
            [
                "id" => 8,
                "order" => 2,
                "title" => "About Us",
                "slug" => "nosotros",
                "lang" => "en",
                "image_id" => 1,
                "children" => [
                    [
                        "id" => 24,
                        "order" => 1,
                        "title" => "About the AEB",
                        "excerpt" => "",
                        "content" => "<p>The Association of Bolivian Studies (AEB) is a non-profit and non-politic organization whose purpose is to promote and promote research and dissemination of knowledge about Bolivia. According to this mission, the AEB aims to be an interdisciplinary forum and a resource to foster collaboration between Bolivian researchers and the rest of the world.</p><p>The AEB organizes an international and interdisciplinary conference that gathers bolivian folks from around the world every two years.</p><p>The AEB publish a weekly magazine for contribute to spread the bolivian research news, the Revista de Estudios Bolivianos / Bolivian Research Review (see <a href='publicaciones/revista'>AEB Magazine</a> tab) regularly updates news about publications and other activities on Facebook as well as the web site (see <a href='noticias'>Bolivian news</a> and <a href='publicaciones/novedades-bibliograficas'>Publications</a> tab).</p><p>The AEB is open to everyone who are interested on study about Bolivia and cordially invite you to be part of their community.</p>",
                        "slug" => "aeb",
                        "lang" => "en",
                        "image_id" => 1
                    ],
                    [
                        "id" => 25,
                        "order" => 2,
                        "title" => "Executive Committee",
                        "excerpt" => "",
                        "content" => "<p>Elections for the AEB Executive Committee are held every two years at the same time as the International Congress of the Association. To be a candidate you must have been a member for at least one year before submitting your candidature.</p>",
                        "slug" => "mesa-directiva",
                        "lang" => "en",
                        "image_id" => 2
                    ],
                    [
                        "id" => 26,
                        "order" => 3,
                        "title" => "Statutes",
                        "excerpt" => "",
                        "content" => "<h2>Article 1:</h2><h1>Name</h1><ol><li>The name of this association will be the Association of Bolivian Studies with the acronym AEB. This document is also referred to as the Association.</li><li>The AEB is and will remain a non-profit corporation and would remain qualified as a federal tax free under section 501 (c)</li><li>The AEB is and will continue to be a non-sectarian, non-political organization and will not participate in political advocacy.</li></ol><h2>Article 2:</h2><h1>The mission</h1><p>The mission and objective of the AEB is to advance and promote research and knowledge of Bolivia by creating an interdisciplinary forum and resources for collaboration, research, study, financing and employment opportunities.</p><p>The membership of the Association is open to all those who share an interest in Bolivia, based on the elaborated tariff scheme.</p><ol><li>By the Executive Board.</li><li>Full members may vote and participate in the Executive Board, as officers of the Association, and as speakers and conference participants.</li><li>To qualify as Student member, an individual must be enrolled and in the process of obtaining a college degree. An individual may only maintain this category for a maximum period of seven years.</li></ol><h2>Article 4:</h2><h1>Quotas</h1><p>The Executive Board may establish And modify the quotas by a majority vote in support of such changes. The Executive Board may establish different rates for special categories of members. </p><h2>Article 5: <h1>Officials and the Executive Board</h1><ol><li>Officials will be a President, Vice President and Treasurer.</li><li>The Executive Board, whose members have voting rights, administers the affairs of the AEB, and for purposes of the corporation shall be considered its Board of Directors. From January 1, 2001 to January 1, 2003, the Association of Bolivian Studies was governed under a provisional structure by an Executive Board of founders consisting of a President, Vice-President and three full members with voice and vote . In January 2002, two other members with a voice and a vote were elected to the Executive Council for a term of two years.</li><li>All members of the Executive Board will serve for two years and serve on a staggered basis, beginning January first. There is no re-election of the president, however, members of the Executive Council could be re-elected consecutively for no more than two terms.</li><li>Some members of the Executive Board, with the exception of the President and Vice President, are individually responsible for developing, managing and improving specific areas. These include, but are not limited to, responsibilities associated with the AEB e-journal (together with an Editorial Board), the Bolivian Journal of Research, newsletter, conference organization, fundraising and strategic institutional development.</li><li>The Executive Board shall endeavor to ensure that at least three disciplines are represented on the Executive Board at all times.</li><li>The President and the Executive Board shall carry out the purposes of the AEB and promote the latter's professional interests.</li><li>The Executive Council will conduct and supervise the affairs of the Association, receive donations, subsidies and implement the annual budget, and act for the benefit of the Association.</li><li>Members of the Executive Board are obliged to respect the interdisciplinary and multinational character of the AEB and work towards achieving diversity by region and discipline. Also, they must ensure that the AEB is not used to advance or promote political or religious goals, convictions or ideologies. Failure to do so will be grounds for dismissal.</li><li>The primary role of the President and Vice-President is to advance the AEB's mission, and to coordinate the work of the Executive Board and members for this purpose.</li><li>The President shall act as Director of the Executive Board and shall be responsible for the preparation of the annual budget and annual financial report for submission to the Executive Board for examination.</li><li>The President, with the support of a majority of the Executive Council, may designate any committees deemed useful for the purpose of the Association's mission.</li><li>The President will maintain an electronic list of all members of the AEB, freely accessible via the Internet to members of the AEB.</li><li>The President shall convene meetings of the Executive Board, which may be assisted by telephone at least once a year, and shall be requested by written request of four members of the Executive Board.</li><li>The President Would be responsible for the distribution, counting and presentation to the Executive Board and members of the results of all votes in favor of positions and amendments.</li><li>The Treasurer is the officer primarily responsible for supervising the financial affairs of the Association. The Treasurer should prepare, review and report quarterly financial reports to the Executive Board.</li><li>Dismissal <ul><li>A. In the event that a member of the Executive Board fails in person or by telephone to two consecutive meetings of the Executive Board, that member shall leave office and an election shall be held in accordance with the general procedures described in this document to fill the vacancy .</li><li>B. In the event of absence, death, resignation or incapacity of the President, his / her functions should be carried out by the Vice-President, who shall complete the term of the previous President.</li><li>C. In the event that the Vice-President is unable or unwilling to serve as President, or in case of absence, death, resignation or incapacity of the Vice-President, the Executive Council shall elect a member of the Executive Council to complete the term of the previous President.</li><li>D. In case the Vice-President assumes the position of President by reason of absence, death, resignation or incapacity of the President, the Executive Council will appoint a member of the Executive Council to serve as Vice-President to complete the mandate of the previous Vice-President .</li></li> </li><li>Dismissal <br> An official or member of the Executive Committee may be dismissed by electronic or written request signed by two-thirds of the members of the Association. In the event that an official or committee member violates the rules of the Association, the Association may be deposed by the vote of five of the seven members of the Executive Committee. In the event of a higher number of members of the Executive Committee, the removal process will require the support of two thirds of the members of the Committee. The members of the Council may be deposed by majority vote of the Executive Committee. Indemnification <br> Any person in the status of a former official subject to a trial or legal process will be compensated by the Association to compensate for the justified expenses that have been incurred by the former official in the legal process or trial.</li><li>, Except in cases where the former official has been responsible for negligence or poor performance of his duties. During the first two years after the founding of the AEB, for administrative reasons from the beginning of the January 1, 2001 until January 1, 2003, an interim government is formed that facilitates the organization's development and the rotation of authorities. During this period, the founders of AEB are constituted as President and Vice President, who after a year of official management will maintain the title of President Life. In January 2001, they will nominate, through majority vote of the Executive Committee, and elect three additional members for the founding Executive Committee. In January 2002, they will nominate, through the majority vote of the Executive Committee, and elect two additional members for the Executive Committee and thus facilitate the rotation of authorities.</li><li>After January 1, 2003, the President, Vice-President, Treasurer and other members of the Executive Committee shall be elected by majority vote of the members of the AEB during an election. The first and second ex-presidents of the AEB are part of the Executive Committee for a period of two years after their respective efforts as presidents.</li><li>Applicants to all positions may be nominated by other members or nominated on their own, however all nominations must be received by the Executive Committee within a maximum of two months prior to the election. All applicants can submit to the members a letter of no more than 350 words. Such a document shall be distributed in writing and electronically to all members within a period not exceeding six weeks before the vote.</li><li>After January 1, 2003, candidates for the Executive Committee must have a good active exercise record for at least 1 year before running for office.</li><li>Elections for President of the AEB and other vacancies will be held between 10 October and 15 October 2002 for a period of two years beginning on 1 January 2003. Voting Closes at 11:50 pm US Central Time, on October 15 of each election. The subsequent votes for the presidency will be conducted in the same way every two years.</li><li>The voting will be carried out electronically via email, and the results will be announced by the President Of the AEB to all members via email until October 17 of the year of election. The email address must be registered in reference to each voter.</li><li>The election of members of the Executive Committee shall not take place before six weeks prior to the end of the term of office of the post to be filled. The voting will be done electronically and the results will be communicated to all members within 48 hours. After the close of voting.</li><li>8. The election of the candidates is guaranteed by the majority of the voting members, therefore a second round can be carried out if necessary. This should be carried out two weeks after the results of the first elections are announced. In this instance the votes will be made in a period of five days, closing the fifth day at 11:59 p.m., Central American time, the results will be released within 48 after the close of voting.</li></ol><h2>Article 7: </h2><h1>Policy and appointment of the members of the Advisory Board. </h1> <p>The Executive Committee of AEB and any of the members can go to the resources of an Advisory Board. Although appointed by the Executive Committee, any member of the AEB may nominate applicants for the Advisory Board. The members of this body will serve for a period of two years, although the position in the Council can be renewed later by majority vote of the Executive Committee. All former presidents who are not members of the Executive Committee automatically become members of the Advisory Council for a period of four years. <h2>Article 8: </h2><h1>Establishment of Committees</h1><ol><li>The Executive Committee, by majority vote, may create specific-purpose committees whose duration should not exceed one year. The Chairman of the Executive Committee shall designate the chairman and members of the committees and among its members a person on the Executive Committee shall be included.</li><li>The chair of each committee shall prepare a report to the Executive Committee before the end of the committee process or, failing that, at the request of the Executive Committee.</li><li>The names of the members of the committee will be made known to all members of the AEB through a publication on the AEB website.</li><li>No committee may create sub-committees or request funds without the written authorization of the President.</li></ol><h2>Article 9:</h2><h1>Congress</h1><ol><li>The AEB will hold a congress every 18 months.</li><li>Of two congresses, one must be held in Bolivia.</li><li>The sections are created upon the written request of any member of the Executive Committee. It must be shown that the proposed section has at least 10 members.</li><li>The directors of each section will hold this position for a period of two years, with no possibility of re-election.</li><li>The panels, which can be proposed by members, must have at least four confirmed members besides the moderator / analyst.</li></ol><h2>Article 10:</h2><h1>Working meetings</h1><ol><li>At each congress, a working meeting will be held, during which members Good exercise files may subject matters to voting in accordance with the regulations.</li><li>No change to the regulations will be approved during the working meetings.</li><li>The board will include a report by the President on the status of the Association and the projects for the next 18 months. At the same time they will include reports from committees and Treasurer.</li><li>The agenda of the working meeting will be prepared by the President of the AEB.</li><li>There will be adequate time for discussion and exposure during the working meeting.</li><li>Any legislative action, with the exception of amendments to the Statutes, which starts at the working meeting will be presented to all members for electronic voting and a majority of voters will be required to support that action .</li><li>The minutes of the Workshops will conform to the recently revised Rules of Order Robert.</li><li>For all votes in the business meeting a quorum is required, which will be composed of 15% of the members registered in the Congress.</li><li>On each occasion, for the vote, the presiding officer determines if there is a quorum, and the voters will have the voting options, yes, no, and abstain.</li><li>Members may request that thematic proposals be included in the agenda of the Working Meeting, but such requests should be received at least 45 days before the meeting. These applications must be respected unless there is a compelling reason not to include them in the meeting.</li></ol><h2>Article 11:</h2><h1>The Research Journal on Bolivia</h1><ol><li>The Journal of Research on Bolivia will be the official journal of the Association.</li><li>The appointment of the editor or editors will be made according to the majority vote of the members of the Executive Committee.</li><li>3The publisher / publishers will serve for a period of two years. This period may be renewed once consecutively.</li><li>The editors will be responsible for requesting manuscripts, for their evaluation and final preparation for distribution to all members. The <strong> Bolivia Research Review </strong> strong> will be published and distributed to all members every six months and will be available on the AEB website.</li><li>The Journal of Research on Bolivia will be guided by the same rules as the journal.</li><li>The editors will be able to appoint members of the editorial committee to assist in the evaluation and publication of the journal.</li></ol><h2>Article 12:</h2><h1>Professional Conduct</h1><p>It is mandatory for all members of the Executive Committee and the Advisory Board to conduct their professional activities In a manner that demonstrates respect for the AEB as an organization and for the status and rights of all its members regardless of gender, age, nationality, sexual orientation, ethnicity, race or belief.<h2>Article 13: </h2><h1>Amendments</h1><ol><li>Amendments to the statute of the Association may be proposed by the Executive Committee with the support of five of the seven members.</li><li>With vote of said Committee or at the request of a third of the members of the Association. Should the Executive Committee wish to expand, this proposal should be approved by two-thirds of the Executive Committee.</li><li>Amendments under consideration should be distributed to members well in advance.</li><li>Ratification of amendments requires the approval of three quarters of all active members.</li><li>During the proposal and the ratification process, those who oppose such an amendment should have an equal opportunity to be heard and present their objections and grounds for rejecting such amendments.</li><li>There will be a period of 60 days between the proposed amendment and its vote and all members should be aware of the proposed amendment.</li><li>The dates of the voting must be clearly indicated by special correspondence with the members on the proposed amendments.</li><li>The central elements of the Association of being an apolitical, non-profit, and non-sectarian association can not be amended.</li></ol><h2>Article 14:</h2><h1>Annual Audit</h1><p>In the event that the Association of Bolivian Studies has to be dissolved, the funds will be distributed to one Or more of the exempted purposes within the meaning of section 501 (C) (3) of the Internal Revenue Code, or the corresponding section of future tax codes, or shall be distributed to the federal government or to the state government Or place for a public purpose. Any fund not disposed of in such a manner shall be disposed by the Jurisdiction Court of the county in which the principal place of business of the corporation resides, for the exclusive use of said purposes or to such organization or organizations as the Court determines, that they are organized and exclusively operate For such purposes.</p><h2>Article: 15</h2><h1>Dissolution</h1><p> In case of dissolution of the AEB, the assets will be distributed according to the rules of section 501 (C) (3) of the US Internal Revenue Code, or the corresponding section of the code then in effect, or distributed to the federal government, or to a local or state government for any public purpose. Any good not distributed in this way will be distributed by the Court having jurisdiction in the county where the main office of the corporation is located, according to what the Court determines.</p><p>We have subscribed here our names east Day of the month of the year 2002.</p><p>Hugo Poppe Entrambasaguas Ana Rebeca Prada </p><p>Director</p><p>John Redman</p><p>Director</p>",
                        "slug" => "estatutos",
                        "lang" => "en",
                        "image_id" => 3
                    ],
                    [
                        "id" => 27,
                        "order" => 4,
                        "title" => "Membership",
                        "excerpt" => "",
                        "content" => "<p>Being a member of the AEB has several benefits. First, the registration allows members to participate in the conference held every two years both as panelists, as well as organizers of round tables or panels. Members of the AEB also have access to the latest issues of Magazine and the Newsletter.</p><h2>New Members</h2><h1>Types of registration</h1><p>Membership is open to all people around the world who share interest in Bolivia. The AEB includes two enrollment periods per year with the possibility of annual and bi-annual membership.</p><h2>Current Members</h2><h1>Membership Renewal</h1><p>Individuals who wish to renew their membership and pay for conference registration may make a single payment following the instructions described below for registration fees.</p><h2>How to Register</h2><h1>In Bolivia</h1><p>In Bolivia, please deposit the amount of the registration in the account: 201-50710848-3-60 of the Bank of Credit to Alba María Paz Soldán and Rowena Gabriela Canedo Vásquez.</p><p>In order to be registered, members must send a scanned copy of the receipt and the membership form that appears below, to the email <a href='mailto:aeb.bsa@gmail.com'>aeb.bsa@gmail.com</a></p><h2>How to register</h2><h1>Outside of Bolivia</h1><p>For credit, debit or credit card payments, please fill out the form at the bottom of this page.</p><blockquote>IMPORTANT. Please in time to send the payment specify the purpose of the same, for example: \"Bi-annual membership category student residing outside of Latin America and registration fee to Congress.</blockquote><p>If you can not pay by credit card, you also have the possibility to pay by check. To do so, please fill out the form below, print it and mail it along with your check to:</p><blockquote>Bolivian Studies Association Prof. Martín Mendoza-Botelho<br>Department of Political Science<br>Eastern Connecticut State University<br>Willimantic, CT 06226 - USA</blockquote>",
                        "slug" => "membresia",
                        "lang" => "en",
                        "image_id" => 4
                    ],
                    [
                        "id" => 28,
                        "order" => 5,
                        "title" => "Members",
                        "excerpt" => "",
                        "content" => "",
                        "slug" => "miembros",
                        "lang" => "en",
                        "image_id" => 5
                    ]
                ]
            ],
            [
                "id" => 9,
                "order" => 3,
                "title" => "Congress",
                "excerpt" => "",
                "content" => "",
                "slug" => "event({id:'congreso'})",
                "lang" => "en",
                "image_id" => 1
            ],
            [
                "id" => 10,
                "order" => 4,
                "title" => "Bolivian news",
                "excerpt" => "",
                "content" => "",
                "slug" => "posts",
                "lang" => "en",
                "image_id" => 1
            ],
            [
                "id" => 11,
                "order" => 5,
                "title" => "Publications",
                "slug" => "publicaciones",
                "lang" => "en",
                "image_id" => 1,
                "children" => [
                    [
                        "id" => 30,
                        "order" => 1,
                        "title" => "Bibliographic Innovations",
                        "excerpt" => "",
                        "content" => "",
                        "slug" => "novedades-bibliograficas",
                        "lang" => "en",
                        "image_id" => 4
                    ],
                    [
                        "id" => 31,
                        "order" => 2,
                        "title" => "AEB Magazine",
                        "excerpt" => "",
                        "content" => "<p>The Bolivian Research Review (Bolivian Research Review) is a bi-annual and interdisciplinary electronic journal dedicated to promoting the advancement of research and knowledge about Bolivia. The Journal of Research on Bolivia is published with the help of the <a href=\"http://library.loyno.edu\" target=\"_blank\"> J. Edgar and Louise S. Monroe Library</a> The <a href=\"http://www.loyno.edu\">University of Loyola, New Orleans </a>, USA. The current director of the Journal is Dr. Josefa Salmon and Oscar Vega Camacho.</p><p>The Bolivian Journal of Research invites the proposal of articles in any discipline, whether in Spanish, Aymara, Quechua, Guarani or English. The works must be unpublished and should not be currently under consideration in another journal. Generally, articles should not exceed 10,000 words. The Editorial Committee shall evaluate anonymously and decide to publish the submitted papers. The magazine will also publish reviews of recent books and announcements about Bolivia. </p> <h3>Reception and admission of articles</h3> <p>If you are interested in proposing an article or being part of the editorial committee, Josefa Salmon, <a href=\"mailto:salmon@loyno.edu\"><b>salmon@loyno.edu</b></a>. Guide for contributions.</p>",
                        "slug" => "revista",
                        "lang" => "en",
                        "image_id" => 5
                    ]
                ]
            ],
            [
                "id" => 12,
                "order" => 6,
                "title" => "Contact",
                "slug" => "contacto",
                "lang" => "en",
                "image_id" => 1,
                "children" => [
                    [
                        "id" => 33,
                        "order" => 1,
                        "title" => "Write us",
                        "excerpt" => "",
                        "content" => "",
                        "slug" => "escribenos",
                        "lang" => "en",
                        "image_id" => 2
                    ]
                ]
            ]
        ];

        Page::buildTree($pages);

    }
}
