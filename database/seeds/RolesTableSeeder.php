<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->delete();

        $roles = [
            [
                'id' => 1,
                'name' => 'super',
                'title' => 'Super Admin',
                'level' => 1,
            ],
            [
                'id' => 2,
                'name' => 'admin',
                'title' => 'Admin',
                'level' => 2,
            ],
            [
                'id' => 3,
                'name' => 'member',
                'title' => 'Member',
                'level' => 3,
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }

    }
}