<?php

use App\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class EventTimelinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('event_timeline')->delete();

        $eventTimelines = [
            [
                "event_id" => 1,
                "timeline_id" => 1,
            ],
            [
                "event_id" => 1,
                "timeline_id" => 2,
            ],
            [
                "event_id" => 1,
                "timeline_id" => 3,
            ],
            [
                "event_id" => 1,
                "timeline_id" => 4,
            ],
            [
                "event_id" => 1,
                "timeline_id" => 5,
            ],
            [
                "event_id" => 1,
                "timeline_id" => 6,
            ],
            [
                "event_id" => 1,
                "timeline_id" => 7,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 8,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 9,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 10,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 11,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 12,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 13,
            ],
            [
                "event_id" => 7,
                "timeline_id" => 14,
            ],
        ];

        foreach ($eventTimelines as $eventTimeline) {
            $event = Event::find($eventTimeline["event_id"]);
            $event->timeline()->attach($eventTimeline["timeline_id"], ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }

    }
}

