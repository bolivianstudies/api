<?php

use App\Image;
use Illuminate\Database\Seeder;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('images')->delete();

        $images = [
            [
                "id" => 1,
                "path" => "img/banners/banner-about.jpg",
                "thumb" => "img/banners/thumbs/banner-about.jpg"
            ],
            [
                "id" => 2,
                "path" => "img/banners/banner-directory.jpg",
                "thumb" => "img/banners/thumbs/banner-directory.jpg"
            ],
            [
                "id" => 3,
                "path" => "img/banners/banner-statuses.jpg",
                "thumb" => "img/banners/thumbs/banner-statuses.jpg"
            ],
            [
                "id" => 4,
                "path" => "img/banners/banner-membership.jpg",
                "thumb" => "img/banners/thumbs/banner-membership.jpg"
            ],
            [
                "id" => 5,
                "path" => "img/banners/banner-about.jpg",
                "thumb" => "img/banners/thumbs/banner-members.jpg"
            ],
            [
                "id" => 6,
                "path" => "img/banners/banner-about.jpg",
                "thumb" => "img/banners/thumbs/banner-events.jpg"
            ],
            [
                "id" => 7,
                "path" => "img/events/event-01.jpg",
                "thumb" => "img/events/thumbs/event-01.jpg",
            ],
            [
                "id" => 8,
                "path" => "img/events/event-02.jpg",
                "thumb" => "img/events/thumbs/event-02.jpg",
            ],
            [
                "id" => 9,
                "path" => "img/events/event-03.jpg",
                "thumb" => "img/events/thumbs/event-03.jpg",
            ],
            [
                "id" => 10,
                "path" => "img/events/event-04.jpg",
                "thumb" => "img/events/thumbs/event-04.jpg",
            ],
            [
                "id" => 11,
                "path" => "img/events/event-05.jpg",
                "thumb" => "img/events/thumbs/event-05.jpg",
            ],
            [
                "id" => 12,
                "path" => "img/events/event-06.jpg",
                "thumb" => "img/events/thumbs/event-06.jpg",
            ],
            [
                "id" => 13,
                "path" => "img/posts/01.jpg",
                "thumb" => "img/posts/thumbs/01.jpg"
            ],
            [
                "id" => 14,
                "path" => "img/posts/02.jpg",
                "thumb" => "img/posts/thumbs/02.jpg"
            ],
            [
                "id" => 15,
                "path" => "img/posts/03.jpg",
                "thumb" => "img/posts/thumbs/03.jpg"
            ],
            [
                "id" => 16,
                "path" => "img/posts/04.jpg",
                "thumb" => "img/posts/thumbs/04.jpg"
            ],
            [
                "id" => 17,
                "path" => "img/posts/05.jpg",
                "thumb" => "img/posts/thumbs/05.jpg"
            ],
            [
                "id" => 18,
                "path" => "img/posts/06.jpg",
                "thumb" => "img/posts/thumbs/06.jpg"
            ],
            [
                "id" => 19,
                "path" => "img/newsletters/default.svg",
                "thumb" => "img/newsletters/thumbs/default.svg"
            ],
            [
                "id" => 20,
                "path" => "img/magazines/01.jpg",
                "thumb" => "img/magazines/thumbs/01.jpg"
            ],
            [
                "id" => 21,
                "path" => "img/publications/default.svg",
                "thumb" => "img/publications/thumbs/default.svg"
            ],
            [
                "id" => 22,
                "path" => "img/users/default.svg",
                "thumb" => "img/users/thumbs/default.svg"
            ],
            [
                "id" => 23,
                "path" => "img/directory/laurent-lacroix.jpg",
                "thumb" => "img/directory/thumbs/laurent-lacroix.jpg"
            ],
            [
                "id" => 24,
                "path" => "img/directory/gabriela-canedo.jpg",
                "thumb" => "img/directory/thumbs/gabriela-canedo.jpg"
            ],
            [
                "id" => 25,
                "path" => "img/directory/martin-mendoza.jpg",
                "thumb" => "img/directory/thumbs/martin-mendoza.jpg"
            ],
            [
                "id" => 26,
                "path" => "img/directory/lucia-quejarazu.jpg",
                "thumb" => "img/directory/thumbs/lucia-quejarazu.jpg"
            ],
            [
                "id" => 27,
                "path" => "img/directory/josefa-salmon.jpg",
                "thumb" => "img/directory/thumbs/josefa-salmon.jpg"
            ],
            [
                "id" => 28,
                "path" => "img/directory/paola-revilla.jpg",
                "thumb" => "img/directory/thumbs/paola-revilla.jpg"
            ],
        ];

        foreach ($images as $image) {
            Image::create($image);
        }

        factory(App\Image::class, 52)->create();

    }
}