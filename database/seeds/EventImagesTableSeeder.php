<?php

use App\Event;
use Illuminate\Database\Seeder;

class EventImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_image')->delete();

        $eventImages = [
            [
                "event_id" => 1,
                "image_id" => 29,
                "order" => 1,
            ],
            [
                "event_id" => 1,
                "image_id" => 30,
                "order" => 2,
            ],
            [
                "event_id" => 1,
                "image_id" => 31,
                "order" => 3,
            ],
            [
                "event_id" => 1,
                "image_id" => 32,
                "order" => 4,
            ],
            [
                "event_id" => 1,
                "image_id" => 33,
                "order" => 5,
            ],
            [
                "event_id" => 1,
                "image_id" => 34,
                "order" => 6,
            ],
            [
                "event_id" => 1,
                "image_id" => 35,
                "order" => 7,
            ],
            [
                "event_id" => 1,
                "image_id" => 36,
                "order" => 8,
            ],
            [
                "event_id" => 1,
                "image_id" => 37,
                "order" => 9,
            ],
            [
                "event_id" => 1,
                "image_id" => 38,
                "order" => 10,
            ],
            [
                "event_id" => 1,
                "image_id" => 39,
                "order" => 11,
            ],
            [
                "event_id" => 1,
                "image_id" => 40,
                "order" => 12,
            ],
            [
                "event_id" => 1,
                "image_id" => 42,
                "order" => 13,
            ],
            [
                "event_id" => 1,
                "image_id" => 43,
                "order" => 14,
            ],
            [
                "event_id" => 1,
                "image_id" => 44,
                "order" => 15,
            ],
            [
                "event_id" => 1,
                "image_id" => 45,
                "order" => 16,
            ],
            [
                "event_id" => 1,
                "image_id" => 46,
                "order" => 17,
            ],
            [
                "event_id" => 1,
                "image_id" => 47,
                "order" => 18,
            ],
            [
                "event_id" => 1,
                "image_id" => 48,
                "order" => 19,
            ],
            [
                "event_id" => 1,
                "image_id" => 49,
                "order" => 20,
            ],
        ];

        foreach ($eventImages as $eventImage) {
            $event = Event::find($eventImage["event_id"]);
            $event->images()->attach($eventImage["image_id"], ["order" => $eventImage["order"], "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }
    }
}
