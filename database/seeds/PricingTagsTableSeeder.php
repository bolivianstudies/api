<?php

use App\PricingTag;
use Illuminate\Database\Seeder;

class PricingTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pricing_tags')->delete();

        $pricingTags = [
            [
                "id" => 1,
                "label" => "membership",
                "en" => [ "description" => "Membership pricing" ],
                "es" => [ "description" => "Costo de membresía" ],
            ],
            [
                "id" => 2,
                "label" => "congress",
                "en" => [ "description" => "Congress inscription Pricing" ],
                "es" => [ "description" => "Costo de inscripción al Congreso" ],
            ],
        ];

        foreach ($pricingTags as $pricingTag) {
            PricingTag::create($pricingTag);
        }

    }
}
