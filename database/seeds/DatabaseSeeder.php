<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RegionTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UserRolesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        $this->call(TimelinesTableSeeder::class);
        $this->call(PricingsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(EventTimelinesTableSeeder::class);
        $this->call(EventPricingsTableSeeder::class);
        $this->call(EventFeaturesTableSeeder::class);
        $this->call(ContentsTableSeeder::class);
        $this->call(PagePricingsTableSeeder::class);
        $this->call(DirectoriesTableSeeder::class);
        $this->call(DirectorsTableSeeder::class);
        $this->call(DirectoryDirectorsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(PostTagsTableSeeder::class);
        $this->call(PostCommentsTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(DocsTableSeeder::class);
        $this->call(EventDocsTableSeeder::class);
        $this->call(NewslettersTableSeeder::class);
        $this->call(DocFeaturesTableSeeder::class);
        $this->call(MagazinesTableSeeder::class);
        $this->call(MagazineDocsTableSeeder::class);
        $this->call(PublicationsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(EventImagesTableSeeder::class);
        $this->call(LocationMarkersTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(PricingTagsTableSeeder::class);
        $this->call(PricingTagPricingTableSeeder::class);
    }
}
