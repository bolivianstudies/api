<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();

        $tags = [
            [
                "id" => 1,
                "title" => "Asociación",
                "lang" => "es",
            ],
            [
                "id" => 2,
                "title" => "Noticias",
                "lang" => "es",
            ],
            [
                "id" => 3,
                "title" => "Comunicados",
                "lang" => "es",
            ],
            [
                "id" => 4,
                "title" => "Boletín",
                "lang" => "es",
            ],
            [
                "id" => 5,
                "title" => "Publicaciones",
                "lang" => "es",
            ],
            [
                "id" => 6,
                "title" => "Filosofía",
                "lang" => "es",
            ],
            [
                "id" => 7,
                "title" => "Investigación",
                "lang" => "es",
            ],
            [
                "id" => 8,
                "title" => "Eventos",
                "lang" => "es",
            ],
            [
                "id" => 9,
                "title" => "Conferencias",
                "lang" => "es",
            ],
            [
                "id" => 10,
                "title" => "Antropología",
                "lang" => "es",
            ],
            [
                "id" => 11,
                "title" => "Historia",
                "lang" => "es",
            ],
            [
                "id" => 12,
                "title" => "News",
                "lang" => "en",
            ],
        ];

        foreach ($tags as $tag) {
            Tag::create($tag);
        }

    }
}
