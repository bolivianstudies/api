<?php

use App\PricingTag;
use Illuminate\Database\Seeder;

class PricingTagPricingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pricing_tag_pricing')->delete();

        $relations = [
            [
                "pricing_tag_id" => 1,
                "pricing_id" => 1,
                "order" => 1,
            ],
            [
                "pricing_tag_id" => 1,
                "pricing_id" => 2,
                "order" => 2,
            ],
            [
                "pricing_tag_id" => 1,
                "pricing_id" => 3,
                "order" => 3,
            ],
            [
                "pricing_tag_id" => 1,
                "pricing_id" => 4,
                "order" => 4,
            ],
            [
                "pricing_tag_id" => 2,
                "pricing_id" => 5,
                "order" => 1,
            ],
            [
                "pricing_tag_id" => 2,
                "pricing_id" => 6,
                "order" => 2,
            ],
            [
                "pricing_tag_id" => 2,
                "pricing_id" => 7,
                "order" => 3,
            ],
            [
                "pricing_tag_id" => 2,
                "pricing_id" => 8,
                "order" => 4,
            ],
        ];

        foreach ($relations as $relation) {
            $tag = PricingTag::find($relation["pricing_tag_id"]);
            $tag->pricing()->attach($relation["pricing_id"], ["order" => $relation["order"], "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
        }
    }
}
