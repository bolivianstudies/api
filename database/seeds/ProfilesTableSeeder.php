<?php

use App\Profile;
use App\User;
use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->delete();

        $profiles = [
            [
                "discipline" => "Diseño y Desarrollo Web",
                "email" => "alejandro.heredia.c@gmail.com",
                "institution" => "Logus Graphics",
                "address_personal" => "Pasaje Copacabana #65 (Bajo Miraflores)",
                "address_institution" => "Alto Següencoma Calle 1 #219",
                "country_id" => "BO",
                "twitter" => "https://twitter.com/logusgraphics",
                "google" => "https://plus.google.com/105597416691764321074",
                "skype" => "alejandro.heredia.c",
                "content" => "<p>Logus Graphics is a quality brand; my name is Alejandro Heredia, the founder. I design, produce and deliver rich web and mobile experiences. </p><p> I craft web interfaces (UX, UI), Round Trip Applications (RTA), Single Page Applications (SPA) with sophisticated and modern latest technologies.</p><p>When you work with Logus you don't need to worry about a web designer, a front end developer, a back end developer and a software engineer for separate. </p><p> I'm specialized on integrating both the appearance and the functionality.</p><p>The ingredients of success in the web business are mostly the years of experience and a fresh vision of an evolving web environment. </p><p> You can check out my full resume and experience here.</p>",
                "birthday" => "1985-06-06",
                "phone" => "59177519326",
                "website" => "https://logus.us",
                "facebook" => "https://facebook.com/LogusGraphics",
                "city" => "La Paz",
                "areas" => "Diseño, Interacción, Desarrollo, Web, Móvil"
            ],
            [
                "discipline" => "Literatura",
                "email" => "hernan.pruden@gmail.com",
                "institution" => "AEB",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología",
                "email" => "josefa.salmon@gmail.com",
                "institution" => "Loyola University",
                "address_personal" => "",
                "address_institution" => "6363 St. Charles Avenue",
                "country_id" => "US",
                "city" => "New Orleans, LA 70118",
                "areas" => ""
            ],
            [
                "discipline" => "Filosofía",
                "email" => "p.revillao@gmail.com",
                "institution" => "Loyola University",
                "address_personal" => "",
                "address_institution" => "6363 St. Charles Avenue",
                "country_id" => "US",
                "city" => "New Orleans, LA 70118",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "vabecia@inspectoratebol.com",
                "institution" => "",
                "address_personal" => "Renè Moreno No. 171",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología",
                "email" => "thomas.abercrombie@nyu.edu",
                "institution" => "New York University",
                "address_personal" => "",
                "address_institution" => "25 Waverly Pl, NY 10003",
                "country_id" => "US",
                "city" => "New York",
                "areas" => ""
            ],
            [
                "discipline" => "Historia del Arte",
                "email" => "labramovich@noma.org",
                "institution" => "New Orleans Museum of Art",
                "address_personal" => "",
                "address_institution" => "1 Collins Diboll Circle",
                "country_id" => "US",
                "city" => "New Orleans, LA",
                "areas" => ""
            ],
            [
                "discipline" => "Sociología",
                "email" => "aguiluz@servidor.unam.mx",
                "institution" => "CEIICH, UNAM",
                "address_personal" => "",
                "address_institution" => "Torre Humanidades II, Piso 4, Coyoacán, México D.F. 04510",
                "country_id" => "MX",
                "city" => "D.F.",
                "areas" => "Estudios culturales; teoría y metodología sociológica (cultura y sociedad)"
            ],
            [
                "discipline" => "Antropología",
                "email" => "absi@ird.fr",
                "institution" => "CERMA",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "FR",
                "city" => "Paris",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "esail@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "albarra@acelerate.com",
                "institution" => "Fundación Bartolomé de Las Casas",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología Cultural",
                "email" => "ralbro@wheatonma.edu",
                "institution" => "Wheaton College, Dept.Sociol.and Anthrop.",
                "address_personal" => "",
                "address_institution" => "Norton, MA 02766",
                "country_id" => "US",
                "city" => "Norton",
                "areas" => "Antropología Cultura, Política, Populismo"
            ],
            [
                "discipline" => "",
                "email" => "roxanaalcoba@hotmail.com",
                "institution" => "Consultora independiente",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Musicología histór.",
                "email" => "alvarado@starpoint.net",
                "institution" => "Southwest State University",
                "address_personal" => "",
                "address_institution" => " Marshall, MN 56258",
                "country_id" => "US",
                "city" => "Marshall",
                "areas" => "Musicología Histórica, múscia en la colonia"
            ],
            [
                "discipline" => "Estudios Culturales",
                "email" => "claualv@hotmail.com",
                "institution" => "University of Connecticut",
                "address_personal" => "",
                "address_institution" => " 301, Ashford, Storrs. Conn.06269-9012",
                "country_id" => "US",
                "city" => "Storrs-Conn",
                "areas" => "Educación en áreas rurales, Estudios culturales"
            ],
            [
                "discipline" => "",
                "email" => "pikornia@hotmail.com",
                "institution" => "Universidad de Toulouse",
                "address_personal" => "",
                "address_institution" => "3 rue Jean Viollis Toulouse 31300",
                "country_id" => "FR",
                "city" => "Toulouse",
                "areas" => "Desarrollo y Política"
            ],
            [
                "discipline" => "Comunicación Social, Ciencias Políticas",
                "email" => "rafael.archondo@undp.org",
                "institution" => "PNUD",
                "address_personal" => "",
                "address_institution" => "Calle 14 de Calacoto",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Economía",
                "email" => "ferars@latinmail.com",
                "institution" => "UMSA",
                "address_personal" => "",
                "address_institution" => "UMSA, Av. Villazón",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Macroeconomía boliviana"
            ],
            [
                "discipline" => "",
                "email" => "argueta2@avantel.net",
                "institution" => "Universidad Nacional Autónoma de México",
                "address_personal" => "",
                "address_institution" => "Torre Humanidades II, Ciudad Universitaria, Mexico D.F. 01710",
                "country_id" => "MX",
                "city" => "México D.F. 01710",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "Wta2@georgetown.edu",
                "institution" => "Georgetown University, History Dept.",
                "address_personal" => "",
                "address_institution" => "History Dept.Georgetown Univ.",
                "country_id" => "US",
                "city" => "Washington DC",
                "areas" => "Nacionalismo"
            ],
            [
                "discipline" => "Arte y Cultura",
                "email" => "arrien@earthlink.net",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "silvia.arze@yahoo.com",
                "institution" => "Fundación Diálogo-Universidad Cordillera",
                "address_personal" => "",
                "address_institution" => "Chaco No. 1161, Cristo Rey",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "assies@colmich.edu.mx",
                "institution" => "El Colegio de Michoacan",
                "address_personal" => "",
                "address_institution" => "Col.Michoacan, Zamora, México",
                "country_id" => "MX",
                "city" => "Zamora",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "aayaviri@mara.scr.entelnet.bo",
                "institution" => "Multiestudios Consultores",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "aayo@utk.edu",
                "institution" => "University of Tennessee",
                "address_personal" => "",
                "address_institution" => "7305 Cresthill, Dr.No.A-10, Knoxville TN 37919",
                "country_id" => "US",
                "city" => "Knoxville, TN -",
                "areas" => ""
            ],
            [
                "discipline" => "Historia, Cultura",
                "email" => "kbarden@palaunet.com",
                "institution" => "",
                "address_personal" => "P.O. Box 96940",
                "address_institution" => "",
                "country_id" => "PW",
                "city" => "Republic of Palau",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "abas@mara.scr.entelnet.bo",
                "institution" => "",
                "address_personal" => "Casilla 455",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "rossanabarragan2003@yahoo.com",
                "institution" => "",
                "address_personal" => "Gobles 6389, Irpavi",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Derecho",
                "email" => "ignaciobarragan@bufeteaguirre-lawfirm.com",
                "institution" => "Bufete Aguirre",
                "address_personal" => "",
                "address_institution" => "Av.Arce No. 2071, piso 1, of. 6",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura. Lit.Andina",
                "email" => "lbary@louisiana.edu",
                "institution" => "University of Louisiana at Lafayette",
                "address_personal" => "",
                "address_institution" => "P.O.B. 43331-Lafayette LA",
                "country_id" => "US",
                "city" => "Lafayette, LA, 70504",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "oscar@worlddata.com",
                "institution" => "World Data",
                "address_personal" => "",
                "address_institution" => "1015 18th St. NW SUITE 710",
                "country_id" => "US",
                "city" => "Washington DC.20036",
                "areas" => ""
            ],
            [
                "discipline" => "Latinoamércia",
                "email" => "sergio@worlddata.com",
                "institution" => "George Washington University",
                "address_personal" => "",
                "address_institution" => "North Potomac, MD",
                "country_id" => "US",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "marc@yachana.org",
                "institution" => "Truman State University, Div.Soc.Science",
                "address_personal" => "",
                "address_institution" => "100 E Normal Kirksville, MO 63501",
                "country_id" => "US",
                "city" => "Kirksville, MO",
                "areas" => "Campesinado"
            ],
            [
                "discipline" => "",
                "email" => "rpbenson@ucsd.edu",
                "institution" => "University of California",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "California",
                "areas" => ""
            ],
            [
                "discipline" => "Historia y Ciencias Políticas",
                "email" => "lebieber@t-online.de",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "DE",
                "city" => "Berlín",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "lilbied@yahoo.com",
                "institution" => "University of Florida",
                "address_personal" => "",
                "address_institution" => "568 NW 39th Dr.",
                "country_id" => "US",
                "city" => "Gainesville, FL",
                "areas" => "community forestry, development"
            ],
            [
                "discipline" => "Historia",
                "email" => "db10@cornell.edu",
                "institution" => "Cornell University",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "USA",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "romain_braun@hotmail.com",
                "institution" => "IHEAL-ERSIPAL",
                "address_personal" => "",
                "address_institution" => "28 Rue St.Guillaume, Paris 75007",
                "country_id" => "FR",
                "city" => "Paris",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "cgbravo@rdc.cl",
                "institution" => "Universidad de Chile",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "CL",
                "city" => "Santiago",
                "areas" => ""
            ],
            [
                "discipline" => "Economía",
                "email" => "davidbravo@economista.com",
                "institution" => "UDABOL - CRI",
                "address_personal" => "",
                "address_institution" => "J.A.Arce entre Carrasco y Salinas NE 1723",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "Arqueología",
                "email" => "dlbroc@bellsouth.net",
                "institution" => "University of North Carolina",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Chapel Hill",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "lgbroc@bellsouth.net",
                "institution" => "Institute of African American Research, University of North Carolina at Chapel Hill",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => "Epoca colonial; Diaspora africana"
            ],
            [
                "discipline" => "Historia",
                "email" => "ray-brown@acck.edu",
                "institution" => "Associated Colleges of Central Kansas",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "McPherson, KS",
                "areas" => "Historia de la educación Superior"
            ],
            [
                "discipline" => "",
                "email" => "burciagacampos@gmail.com",
                "institution" => "Universidad Autónoma de Zacatecas",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "MX",
                "city" => "Guadalupe, Zacatecas",
                "areas" => "Historia"
            ],
            [
                "discipline" => "Arqueología",
                "email" => "burkholder@nku.edu",
                "institution" => "Northern Kentucky University, Anthropology",
                "address_personal" => "",
                "address_institution" => "Highland Heights, KY 41099",
                "country_id" => "US",
                "city" => "USA",
                "areas" => "Arqueología, Historia"
            ],
            [
                "discipline" => "",
                "email" => "andrecaly@hotmail.com",
                "institution" => "PRODENER",
                "address_personal" => "",
                "address_institution" => "F.Guachalla 820",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología",
                "email" => "pcalla@proeibandes.org",
                "institution" => "Universidad de la Cordillera",
                "address_personal" => "2422774",
                "address_institution" => "Chaco No. 1161, Cristo Rey",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Género, Interculturalidad"
            ],
            [
                "discipline" => "Historia",
                "email" => "guicalvo1@hotmail.com",
                "institution" => "Archivo-Biblioteca 'Monseñor Taborga'",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => "Archivos"
            ],
            [
                "discipline" => "",
                "email" => "dcaro@culturalpractice.com",
                "institution" => "Cultural Practice, LLC",
                "address_personal" => "",
                "address_institution" => "4300 Montgomery Avenue, Suite 305",
                "country_id" => "US",
                "city" => "Bethesda, MD 20814",
                "areas" => "Antropología, Género, Movimientos Sociales"
            ],
            [
                "discipline" => "Editor",
                "email" => "tcigen@worldnet.att.net",
                "institution" => "Caribbean Historical y Genealog.Journal",
                "address_personal" => "",
                "address_institution" => "PO Box 1312",
                "country_id" => "US",
                "city" => "Highland, CA",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "althusius57@hotmail.com",
                "institution" => "ACANAV",
                "address_personal" => "",
                "address_institution" => "Pedro Leòn Gallo 585",
                "country_id" => "CL",
                "city" => "Valparaìso",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "komadina@rocketmail.com",
                "institution" => "Laboratoire d’Anthropologie « Mémoire, Identité et Cognition Sociale »",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Cochabamba, Cochabamba",
                "areas" => "antropología del alcohol y del cuerpo"
            ],
            [
                "discipline" => "Political Science",
                "email" => "centellm@dickinson.edu",
                "institution" => "Dickinson College",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Carlisle, PA",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "chambers@umn.edu",
                "institution" => "University of Minnesota",
                "address_personal" => "Tel. 612 625 6376",
                "address_institution" => "267 19th Ave. S.",
                "country_id" => "US",
                "city" => "Minneapolis, MN 55455",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "isabelseeghers@hotmail.com",
                "institution" => "",
                "address_personal" => "Landaeta 745",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia aymara",
                "email" => "maeucho@ceibo.entelnet.bo",
                "institution" => "THOA (Taller de Historia Oral Andina)",
                "address_personal" => "",
                "address_institution" => "Casilla 6931",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "cricaser@mara.scr.entelnet.bo",
                "institution" => "Universidad S.Francisco Xavier",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "Derecho",
                "email" => "clavero@.us.es",
                "institution" => "Universidad de Sevilla, Facultad de Derecho",
                "address_personal" => "",
                "address_institution" => "Universidad de Sevilla",
                "country_id" => "ES",
                "city" => "Sevilla",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología",
                "email" => "isabelle@unete.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "mconea@att.net",
                "institution" => "International Fine Arts College",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Miami, FL.",
                "areas" => "Populismo,Migración, Siglo XX"
            ],
            [
                "discipline" => "Political Science",
                "email" => "annabelle.conroy@fulbrightmail.org",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => "Public policy, decentralization, development"
            ],
            [
                "discipline" => "Historia",
                "email" => "vivicont@imagine.com.ar",
                "institution" => "Universidad Nacional de Jujuy",
                "address_personal" => "",
                "address_institution" => "Otero 262",
                "country_id" => "AR",
                "city" => "4600, Jujuy",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "manuelco@iadb.org",
                "institution" => "Inter-American Development Bank",
                "address_personal" => "",
                "address_institution" => "15 Lawngate Ct",
                "country_id" => "US",
                "city" => "Potomac,MD 20854",
                "areas" => "Historia Económica y Social, Políticas Públicas, Educación"
            ],
            [
                "discipline" => "",
                "email" => "conzelma@colorado.edu",
                "institution" => "University of Colorado, Dept.Anthropology",
                "address_personal" => "",
                "address_institution" => "233, UCB, Boulder",
                "country_id" => "US",
                "city" => "Boulder,CO 80309",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "ximena_cordova@hotmail.com",
                "institution" => "Newcastle University",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "GB",
                "city" => "",
                "areas" => "Antropología, Filosofía, Estudios Culturales, Identidad, Cultura Popular, Folklore, Medios"
            ],
            [
                "discipline" => "",
                "email" => "jcrowder@uh.edu",
                "institution" => "University of Houston",
                "address_personal" => "",
                "address_institution" => "233 Mcllehenny Hall, 3107 Banbury",
                "country_id" => "US",
                "city" => "Houston TX 77027",
                "areas" => "El Alto, Titicaca"
            ],
            [
                "discipline" => "Economía",
                "email" => "walter.cuellar@upc.es",
                "institution" => "",
                "address_personal" => "Av.Diagonal 647, piso 7",
                "address_institution" => "",
                "country_id" => "ES",
                "city" => "Barcelona 8028",
                "areas" => ""
            ],
            [
                "discipline" => "Etnología",
                "email" => "daillant@mae.u-paris10.fr",
                "institution" => "Laborat.Ethnologie et Sociol.Comparative",
                "address_personal" => "",
                "address_institution" => "Nanterre",
                "country_id" => "FR",
                "city" => "Nanterre",
                "areas" => ""
            ],
            [
                "discipline" => "Sociología",
                "email" => "santiago.daroca@undp.org",
                "institution" => "Programa de NN.UU.para el desarrollo",
                "address_personal" => "",
                "address_institution" => "Calle 14 esq. S.Bustamante, Calacoto",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Sociología económica, sociología de la cultura, desarrollo humano"
            ],
            [
                "discipline" => "Política, Economía",
                "email" => "martincarrion@saisbologna.org",
                "institution" => "The John Hopkins University",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Baltimore, MD",
                "areas" => ""
            ],
            [
                "discipline" => "Cineasta, Videasta",
                "email" => "lidelaq@hotmail.com",
                "institution" => "Producciones Nicobis",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Negocios",
                "email" => "hdrocha@ceibo.entelnet.bo",
                "institution" => "",
                "address_personal" => "PO Box 8790",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "eveandrade@ig.com.br",
                "institution" => "Universidad de Guarulhos",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BR",
                "city" => "Guarulhos, S.Paulo-Brasil",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "suremain@ird.fr",
                "institution" => "Inst.de Investigaciones p.el Desarrollo",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "gabuhexe@hotmail.com",
                "institution" => "Alice Salomon Hochschule",
                "address_personal" => "",
                "address_institution" => "Alice-Salomon-Platz 3 12627",
                "country_id" => "DE",
                "city" => "Berlin",
                "areas" => "Peace building, conflict management and indigenous issues"
            ],
            [
                "discipline" => "",
                "email" => "gdelbarco@gmail.com",
                "institution" => "Alice Salomon Hochschule",
                "address_personal" => "",
                "address_institution" => "Alice-Salomon-Platz 5 D-12627 Berlin, Alemania",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Derechos Humanos, Derechos Indígenas, Gestión y Transformación de Conflictos, Construcción de Paz"
            ],
            [
                "discipline" => "Historia",
                "email" => "delpinof@filol.csic.es",
                "institution" => "Consejo Superior de Investigac. Científicas",
                "address_personal" => "",
                "address_institution" => "Calle Medinacelli, 6",
                "country_id" => "ES",
                "city" => "Madrid, 28014",
                "areas" => ""
            ],
            [
                "discipline" => "Economía",
                "email" => "mxd1@unm.edu",
                "institution" => "Univ. of New Mexico",
                "address_personal" => "",
                "address_institution" => "801 Yale NE",
                "country_id" => "US",
                "city" => "Albuquerque NM 87131",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología, Estudios Culturales",
                "email" => "guiller@cats.ucsc.edu",
                "institution" => "University of California",
                "address_personal" => "",
                "address_institution" => "1156 HighSt. SC.CA",
                "country_id" => "US",
                "city" => "SC CA 95064",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "edigman@hotmail.com",
                "institution" => "Northern Illinois University",
                "address_personal" => "",
                "address_institution" => "Northern Illinois University",
                "country_id" => "US",
                "city" => "DeKalb, IL.",
                "areas" => "PolíticaComparativa, AdministraciónPública"
            ],
            [
                "discipline" => "Ciencias Políticas",
                "email" => "mpdomingo@usal.es",
                "institution" => "Univ.de Salamanca, Estud.Iberoamérica",
                "address_personal" => "",
                "address_institution" => "Universidad de Salamanca",
                "country_id" => "ES",
                "city" => "Salamanca",
                "areas" => "Democracia, Derechos Humanos, Partidos Políticos"
            ],
            [
                "discipline" => "Biología",
                "email" => "dorn@loyno.edu",
                "institution" => "Loyola University",
                "address_personal" => "",
                "address_institution" => "6363 St.Charles Ave.",
                "country_id" => "US",
                "city" => "New Orleans, LA 70118",
                "areas" => "Chagas"
            ],
            [
                "discipline" => "",
                "email" => "ddressi@tulane.edu",
                "institution" => "Latin American Library",
                "address_personal" => "",
                "address_institution" => "Tulane University",
                "country_id" => "US",
                "city" => "New Orleans, LA",
                "areas" => "Colonia, Siglos 16-17"
            ],
            [
                "discipline" => "Antropología",
                "email" => "mdudley@tulane.edu",
                "institution" => "Tulane University",
                "address_personal" => "",
                "address_institution" => "Dept.of Anthropology",
                "country_id" => "US",
                "city" => "New Orleans, LA -",
                "areas" => "Antropología Ecológica, Identidades Étnicas"
            ],
            [
                "discipline" => "Historia, Política",
                "email" => "james.dunkerley@sas.ac.uk",
                "institution" => "Inst.Study of the Americas ISA. Univ.Londres",
                "address_personal" => "",
                "address_institution" => "31, Tavistock Square",
                "country_id" => "GB",
                "city" => "Lond, ENGL.WC1H9HA",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "echeniqu@up.edu",
                "institution" => "University of Portland",
                "address_personal" => "",
                "address_institution" => "5000 N Willame H. Blud",
                "country_id" => "US",
                "city" => "Portland OR. 97203",
                "areas" => "Literatura"
            ],
            [
                "discipline" => "",
                "email" => "wichitaloans@hotmail.com",
                "institution" => "Mortgage Network, Inc.",
                "address_personal" => "",
                "address_institution" => "155 N.Market Ste. 720",
                "country_id" => "US",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "Economía, Política",
                "email" => "percye@bpbany.com",
                "institution" => "Banco de la Provincia de Bs.Aires",
                "address_personal" => "",
                "address_institution" => "609 5ftAve. 3rd Floor",
                "country_id" => "US",
                "city" => "New York, NY 10017",
                "areas" => "Economía, Política"
            ],
            [
                "discipline" => "",
                "email" => "AndesMusicTravel@yahoo.com",
                "institution" => "Star of the Andes Music Travel Enterp. Inc",
                "address_personal" => "",
                "address_institution" => "Miami Beach. FL.",
                "country_id" => "US",
                "city" => "Miami Beach, FL -usa",
                "areas" => "Música, Ecología"
            ],
            [
                "discipline" => "Historia",
                "email" => "patriciafernandez@acelerate.com",
                "institution" => "Coordinadora de Historia",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Reportera, Investig.",
                "email" => "lindaclarefarthing@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "Ithaca, NY, 14850",
                "country_id" => "US",
                "city" => "Ithaca, NY 14850",
                "areas" => "Desarrollo de base, género, guerra contra las drogas"
            ],
            [
                "discipline" => "",
                "email" => "feldmani@georgetown.edu",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "Estudios Culturales",
                "email" => "Fabiola.Fernandez@asu.edu",
                "institution" => "Arizona State University, Dept.Lang.&amp;Literat.",
                "address_personal" => "",
                "address_institution" => "PO Box 870202",
                "country_id" => "US",
                "city" => "Tempe AZ 85287-0202",
                "areas" => "EtudiosCulturales - Literatura y Cine"
            ],
            [
                "discipline" => "",
                "email" => "babs_is_cool@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "GB",
                "city" => "Bromley, Kent",
                "areas" => "Culture, Language"
            ],
            [
                "discipline" => "",
                "email" => "T.C.Field@lse.ac.uk",
                "institution" => "London School of Economics",
                "address_personal" => "Edificio Columbia, 1001, Avenida Arce",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "la historia internacional contemporanea"
            ],
            [
                "discipline" => "Historia",
                "email" => "cfinegan@iup.edu",
                "institution" => "Indiana University of Pennsylvania",
                "address_personal" => "317, S. 13th Street",
                "address_institution" => "Indiana PA 15701",
                "country_id" => "US",
                "city" => "Indiana PA 15701",
                "areas" => "Historia Colonial, Comunidades Nativas Americanas"
            ],
            [
                "discipline" => "",
                "email" => "franzflo@hotmail.com",
                "institution" => "Univ.Mayor S.Francisco Xavier",
                "address_personal" => "Calle Calvo No. 413",
                "address_institution" => "Junín, esq. Estudiantes",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "renata_forste@byu.edu",
                "institution" => "Brigham Young University",
                "address_personal" => "Provo, UTAH 84602",
                "address_institution" => " Utah 84602",
                "country_id" => "US",
                "city" => "Provo, UTAH 84602",
                "areas" => "Fertilidad, Familia"
            ],
            [
                "discipline" => "",
                "email" => "travelling@viajesinfin.com",
                "institution" => "Travelling Producciones",
                "address_personal" => "",
                "address_institution" => "Pantaleón Rivarola 2427, 9",
                "country_id" => "AR",
                "city" => "Buenos Aires, C1417 FCA",
                "areas" => "Cultura Latinoamericana"
            ],
            [
                "discipline" => "Ciencias Políticas",
                "email" => "gamarrae@fiu.edu",
                "institution" => "Latin American &amp;Caribeean Center FIU",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "JM",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "agamarra@coteor.net.bo",
                "institution" => "Sociedad de Historia y Geografia",
                "address_personal" => "",
                "address_institution" => "Casilla 717, Murguía 396",
                "country_id" => "BO",
                "city" => "Oruro",
                "areas" => "Medicina-Historia"
            ],
            [
                "discipline" => "Sociólogo",
                "email" => "franco.gamboa@mailcity.com",
                "institution" => "Terry Sanford Institute of Public Policy",
                "address_personal" => "Edfi. Armonía No. 50",
                "address_institution" => "Duke University, Durham",
                "country_id" => "US",
                "city" => "Durham NC 27708-0237",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "Sumerced@mara.scr.entelnet.bo",
                "institution" => "Sumerced Hotel",
                "address_personal" => "",
                "address_institution" => "Azurduy No. 16",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => "Comercio, Negocios"
            ],
            [
                "discipline" => "Filosofía, Lingüística",
                "email" => "TXBCC@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "Sociología",
                "email" => "rgaagl@ceibo.entelnet.bo",
                "institution" => "Universidad Mayor de San Andres",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Movimientos Sociales"
            ],
            [
                "discipline" => "Literatura",
                "email" => "lgarcia@oregon.uoregon.edu",
                "institution" => "University of Oregon",
                "address_personal" => "",
                "address_institution" => "Eugene OR",
                "country_id" => "US",
                "city" => "Oregón",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "cgaviram@hotmail.com",
                "institution" => "Seville ESPA-A",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "ES",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "gegourel@club-internet.fr",
                "institution" => "Ecole des Hautes Etudes en Sciences Sociales - CERMA",
                "address_personal" => "",
                "address_institution" => " EHESS - CERMA",
                "country_id" => "FR",
                "city" => "Parìs",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "dineeh35@pacbell.net",
                "institution" => "University of Southern California",
                "address_personal" => "",
                "address_institution" => "Univ.Park Campus",
                "country_id" => "US",
                "city" => "LA. California 90089",
                "areas" => "Mujer, Salud, Educación, Area social y política"
            ],
            [
                "discipline" => "",
                "email" => "ngonzale@uiuc.edu",
                "institution" => "University of Illinois",
                "address_personal" => "",
                "address_institution" => "Urbana, Illinois 61821",
                "country_id" => "US",
                "city" => "Illinois 61821",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "kgoodson@bz.peacecorps.gov",
                "institution" => "Peace Corps Belize",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BZ",
                "city" => "",
                "areas" => "Cultura colla, Potosì"
            ],
            [
                "discipline" => "",
                "email" => "ggosalvez@vitro.com",
                "institution" => "VITRO",
                "address_personal" => "Ricardo Margain 52331 1007101",
                "address_institution" => "Ricardo Margain 52331 1007101",
                "country_id" => "MX",
                "city" => "Guadalajara, Jalisco-MEXICO",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "laura-gotkowitz@uiowa.edu",
                "institution" => "University of Iowa, Dept. History",
                "address_personal" => "Department of History",
                "address_institution" => "Iowa City, Iowa 52242",
                "country_id" => "US",
                "city" => " IowaCity 52242",
                "areas" => ""
            ],
            [
                "discipline" => "Economía Política, Antropología",
                "email" => "ggray@mpd.ucb.edu.bo",
                "institution" => "PROGRAMA NN.UU. PARA EL DESARROLLO",
                "address_personal" => "",
                "address_institution" => "Calle 14 esq. S.Bustamante, Calacoto",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Política pública"
            ],
            [
                "discipline" => "",
                "email" => "grunerdo@usc.edu",
                "institution" => "USC",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => "Anthropology, migration, identity"
            ],
            [
                "discipline" => "",
                "email" => "margui@club-internet.fr",
                "institution" => "Universidad Paul Valery",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "FR",
                "city" => "Montpellier",
                "areas" => ""
            ],
            [
                "discipline" => "Geografia",
                "email" => "guilbertmleti@gmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "FR",
                "city" => "Paris",
                "areas" => "medio ambiante - desarollo sostenible"
            ],
            [
                "discipline" => "Antropología",
                "email" => "dguss@emerald.tufts.edu",
                "institution" => "Tufts University, Dept.Sociology &amp;Anthrop.",
                "address_personal" => "Department of Sociology and Anthropology",
                "address_institution" => "Tufts University",
                "country_id" => "US",
                "city" => "Medford, MA 02143",
                "areas" => "Performance cultural, arte, rituales y fiestas"
            ],
            [
                "discipline" => "Antropologìa",
                "email" => "bdgustaf@artsci.wustl.edu",
                "institution" => "Dep. of Anthropology, Washington University",
                "address_personal" => "",
                "address_institution" => "Washington University",
                "country_id" => "US",
                "city" => "St.Louis, MO 63130",
                "areas" => "Guarani, educación bilingüe, movimientos regionales"
            ],
            [
                "discipline" => "Historia, Arqueología",
                "email" => "lbrockin@wpo.nccu.edu",
                "institution" => "North Carolina Central University",
                "address_personal" => "",
                "address_institution" => "Chapel Durham, North Carolina",
                "country_id" => "US",
                "city" => "North Carolina",
                "areas" => "Historia colonial"
            ],
            [
                "discipline" => "Historia",
                "email" => "hampemar@hotmail.com",
                "institution" => "Pontificia Universidad Católica del Peru",
                "address_personal" => "",
                "address_institution" => "Apartado 1761",
                "country_id" => "PE",
                "city" => "Lima 100, Perù",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "ebharlowe@stkate.edu",
                "institution" => "College of St. Catherine",
                "address_personal" => "",
                "address_institution" => " St. Paul Minnesota 55105",
                "country_id" => "US",
                "city" => "Minnesota 55105",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "jhillman@trentu.ca",
                "institution" => "Trent University",
                "address_personal" => "",
                "address_institution" => "Peterborough, Ontario K9l 1Z7",
                "country_id" => "CA",
                "city" => "Ontario",
                "areas" => "Minería"
            ],
            [
                "discipline" => "",
                "email" => "hippert.chri@uwlax.edu",
                "institution" => "University of Wisconsin-La Crosse",
                "address_personal" => "",
                "address_institution" => "1725 State St., 437C Wimberly Hall, Dept. of Sociology and Archaeology, La Crosse, WI",
                "country_id" => "US",
                "city" => "",
                "areas" => "ethnography, rural development, gender, ethnicity, popular participation"
            ],
            [
                "discipline" => "Antropología",
                "email" => "shirsch@fibertel.com.ar",
                "institution" => "",
                "address_personal" => "O Higgins 3350",
                "address_institution" => "",
                "country_id" => "AR",
                "city" => "1429, Cap.Federal",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "symholben@hotmail.com",
                "institution" => "Catholic University of America",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => "Irrigación, derecho consuetudinario"
            ],
            [
                "discipline" => "",
                "email" => "hollweg@bibosi.scz.entelnet.bo",
                "institution" => "UPSA",
                "address_personal" => "",
                "address_institution" => "Parque Industrial",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "rhgrs@hotmail.com",
                "institution" => "Florida International University, Dept.History",
                "address_personal" => "",
                "address_institution" => "University Park, Miami FL",
                "country_id" => "US",
                "city" => "Miami, FL.",
                "areas" => "Etnohistoria andina, historia del derecho"
            ],
            [
                "discipline" => "Historia",
                "email" => "hhuberabe@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "andyca7@hotmail.com",
                "institution" => "Policia Nacional",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "edgari@mara.scr.entelnet.bo",
                "institution" => "Univ.S.Francisco Xavier de Chuquisaca",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "mirurozqui@ceh.csic.es",
                "institution" => "CSIC",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "ES",
                "city" => "Madrid, 28014",
                "areas" => "Medical Anthropology Indigenous Movements Cultural and Performance Studies"
            ],
            [
                "discipline" => "",
                "email" => "jlizursa@wam.umd.edu",
                "institution" => "University of Maryland",
                "address_personal" => "",
                "address_institution" => "College Park, MD 20705",
                "country_id" => "US",
                "city" => "Maryland",
                "areas" => ""
            ],
            [
                "discipline" => "Antropología",
                "email" => "bbj17@columbia.edu",
                "institution" => "Columbia University",
                "address_personal" => "P.O. Box 3028",
                "address_institution" => "PO Box 250490",
                "country_id" => "US",
                "city" => "Amherst, MA 01004",
                "areas" => "Antropología Médica, movimientos indígenas, Estudios culturales"
            ],
            [
                "discipline" => "Historia, Arqueología, Antropología",
                "email" => "catherine.Julien@wmich.edu",
                "institution" => "Western Michigan University",
                "address_personal" => "Tel. 269387-4632",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Kalamazoo MI 49800-5334",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "akaempfe@richmond.edu",
                "institution" => "University of Richmond",
                "address_personal" => "",
                "address_institution" => "Richmond VA, 23173",
                "country_id" => "US",
                "city" => "Richmond VA 23172",
                "areas" => "Literatura Siglo XIX, Estudios Culturales"
            ],
            [
                "discipline" => "",
                "email" => "jeffthelinguist@juno.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Philadelphia PA",
                "areas" => "Movimientos Sociales y Derechos Humanos"
            ],
            [
                "discipline" => "",
                "email" => "akennem1@uncc.edu",
                "institution" => "University of North Carolina at Charlotte",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "bkohl@temple.edu",
                "institution" => "Temple University, Geography &amp; Urban Studies",
                "address_personal" => "Geograhy and Urban Studies",
                "address_institution" => "Philadelphia, PA 19122",
                "country_id" => "US",
                "city" => "Philadelphia PA 19122",
                "areas" => "Economìa y Polìtica"
            ],
            [
                "discipline" => "",
                "email" => "nonpresence@hotmail.com",
                "institution" => "",
                "address_personal" => "Seattle, WA 98107",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Seattle, WA 98107",
                "areas" => "Mujer, mineros"
            ],
            [
                "discipline" => "",
                "email" => "kinmane@missouri.edu",
                "institution" => "University of Missouri",
                "address_personal" => "",
                "address_institution" => "15 Stewart Hall",
                "country_id" => "US",
                "city" => "Columbia, MA 65203",
                "areas" => "Salud, desarrollo"
            ],
            [
                "discipline" => "",
                "email" => "nklahn@cats.ucsc.edu",
                "institution" => "UCSC",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Santa Cruz, California",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "tkruse@albatros.cnb.net",
                "institution" => "CEDLA",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Economìa Laboral"
            ],
            [
                "discipline" => "",
                "email" => "eckart.kuehne@gmx.net",
                "institution" => "Swiss Federal Inst. of Tecnology",
                "address_personal" => "Arquitecto ETHZ",
                "address_institution" => "",
                "country_id" => "CH",
                "city" => "Zurich",
                "areas" => "Arquitect.-Historia"
            ],
            [
                "discipline" => "Sociologia",
                "email" => "laurentlacroixdefaye@yahoo.fr",
                "institution" => "EHESS",
                "address_personal" => "41, boulevard Viala, 37700 Saint Pierre des Corps - France",
                "address_institution" => "190-195 Avenue de France. 75013 PARIS",
                "country_id" => "FR",
                "city" => "París",
                "areas" => "Social and indigenous movements, public policies and social/economic actors strategies, governance"
            ],
            [
                "discipline" => "",
                "email" => "sebastian@inkarri.net",
                "institution" => "Programa Indigena UNESCO ETXEA",
                "address_personal" => "",
                "address_institution" => "PO Box 1559, Vitoria-Gasteiz",
                "country_id" => "ES",
                "city" => "Pais Vasco, 01001",
                "areas" => "Investigación, Documentación"
            ],
            [
                "discipline" => "Historia",
                "email" => "brooke.larson@sunysb.edu",
                "institution" => "Stony Brook University, Depto. History",
                "address_personal" => "",
                "address_institution" => "Stony Brook University",
                "country_id" => "US",
                "city" => "Stony Brook, NY 11794",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "jean-pierre.lavaud@univ-lille1.fr",
                "institution" => "Institut de Sociologie Universite de Lille",
                "address_personal" => "",
                "address_institution" => "Universitè de Lille",
                "country_id" => "FR",
                "city" => "La Madeleine",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "lanitalema@gmail.com",
                "institution" => "Estoy relacionada con el PIEB, CIPCA,INVESTIGACRUZ, Bolivia",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => "Oriente, poblaciones indígenas, fuentes, siglos XIX - XX, herramientas de investigación"
            ],
            [
                "discipline" => "Pedagogía.Desarrollo",
                "email" => "bbj17@columbia.edu",
                "institution" => "Columbia University",
                "address_personal" => "P.O. Box 3028",
                "address_institution" => "PO Box 249490",
                "country_id" => "US",
                "city" => "Amherst, MA 01004",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "kmlennon@educ.umass.edu",
                "institution" => "University of Massachusetts, Amherst",
                "address_personal" => "",
                "address_institution" => "990 North Pleasant St, G-24",
                "country_id" => "US",
                "city" => "Amherst, MA 01002",
                "areas" => "Educación Popular y Salud, Ecología Política, Movimientos Indígenas"
            ],
            [
                "discipline" => "Español, Lingüística",
                "email" => "kleonard@iastate.edu",
                "institution" => "Iowa State University",
                "address_personal" => "",
                "address_institution" => "300 Pearson Hall",
                "country_id" => "US",
                "city" => "Ames, Iowa 50011",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "moxitano@sauce.ben.entelnet.bo",
                "institution" => "",
                "address_personal" => "Calle Penocos 63",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Mojos, Trinidad",
                "areas" => ""
            ],
            [
                "discipline" => "Género y Desarrollo",
                "email" => "lind@asu.edu",
                "institution" => "Arizona State University",
                "address_personal" => "",
                "address_institution" => "PO Box 873404",
                "country_id" => "US",
                "city" => "Tempe, Arizona 85287",
                "areas" => "Género y desarrollo, cultura política, movimientos sociales, política pública"
            ],
            [
                "discipline" => "Antropología",
                "email" => "lither@uiuc.edu",
                "institution" => "University of Illinois, Dept. Anthropology",
                "address_personal" => "",
                "address_institution" => "607 S. Mathews",
                "country_id" => "US",
                "city" => "Urbana, IL 61801",
                "areas" => "Género, sexualidad y economía política"
            ],
            [
                "discipline" => "Historia",
                "email" => "mclb@caoba.entelnet.bo",
                "institution" => "Carrera de Historia UMSA",
                "address_personal" => "",
                "address_institution" => "Av. 6 de Agosto 2080",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia, Arqueología",
                "email" => "alorandi@filo.uba.ar",
                "institution" => "Univ.de Bs.Aires. Facult.Filosofìa y Letras",
                "address_personal" => "",
                "address_institution" => "Puan 480 (CP 1406)",
                "country_id" => "AR",
                "city" => "Buenos Aires 1080",
                "areas" => ""
            ],
            [
                "discipline" => "Escritor",
                "email" => "hcf_mansilla@yahoo.com",
                "institution" => "",
                "address_personal" => "Casilla 2049",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Estudios Culturales",
                "email" => "guillermo_mariaca@hotmail.com",
                "institution" => "Universidad Mayor de San Andres",
                "address_personal" => "",
                "address_institution" => "Carrera de Literatura UMSA",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "marionj@emmanuel.edu",
                "institution" => "Emmanuel College",
                "address_personal" => "&nbsp;",
                "address_institution" => "400, The Fenway",
                "country_id" => "US",
                "city" => "Boston MA. 02115",
                "areas" => "Etnohistoria"
            ],
            [
                "discipline" => "Historia",
                "email" => "francoise.martinez3@libertysurf.fr",
                "institution" => "Paris University",
                "address_personal" => "Tel. 75505757",
                "address_institution" => "",
                "country_id" => "FR",
                "city" => "75018, Paris-Francia",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "pucarani@hotmail.com",
                "institution" => "Proyectos Imaginarios Urbanos",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Poesía",
                "email" => "arantza.mayo@pmb.ox.ac.uk",
                "institution" => "Pembroke College, University of Oxford",
                "address_personal" => "",
                "address_institution" => "Oxford University",
                "country_id" => "GB",
                "city" => " Oxfordshire",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "jaqueli@supernet.com.bo",
                "institution" => "CESU-UMSS",
                "address_personal" => "",
                "address_institution" => "Calama No. 0235",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "s_mcindoo@yahoo.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Albuquerque, NM",
                "areas" => "religion, politics"
            ],
            [
                "discipline" => "Antropología",
                "email" => "john.mcneish@sif.uib.no",
                "institution" => "Inst. of Anthropology. University of Bergen",
                "address_personal" => "Tel. 55589250 55589260",
                "address_institution" => "Fosswinckelsgate 6. 5007",
                "country_id" => "NO",
                "city" => "Bergen",
                "areas" => "Política, Descentralización, Pobreza y Desarrollo, Pueblos Indígenas, Aymaras"
            ],
            [
                "discipline" => "Sociologìa",
                "email" => "markomejia@hotmail.com",
                "institution" => "UAGRM-SOCIOLOGIA",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "mmp@servidor.unam.mx",
                "institution" => "Univ. Nal. Autonoma de Mexico",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "MX",
                "city" => "MEXICO D.F.",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "lmogro@get2net.dk",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "DK",
                "city" => "Nexoe",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "elizabethmonasterios@yahoo.com",
                "institution" => "SUNY at Stony Brook",
                "address_personal" => "",
                "address_institution" => "Port Jefferson, New York",
                "country_id" => "US",
                "city" => "NY",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "amontev@upa.cl",
                "institution" => "Universidad de Playa Ancha",
                "address_personal" => "",
                "address_institution" => "Av. Playa Ancha 858",
                "country_id" => "CL",
                "city" => "Valparaìso",
                "areas" => ""
            ],
            [
                "discipline" => "Economía",
                "email" => "rolando@caoba.entelnet.bo",
                "institution" => "CIESS-Econometrica",
                "address_personal" => "",
                "address_institution" => " Sanchez Lima 2340",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Polìtica Exterior",
                "email" => "morales@mail.ucf.edu",
                "institution" => "University of Central Florida",
                "address_personal" => "",
                "address_institution" => "PO. Box 161356",
                "country_id" => "US",
                "city" => "Orlando, Florida&nbsp; 32816-1356",
                "areas" => "Política Exterior, Historia política contemporánea, Control de narcóticos"
            ],
            [
                "discipline" => "",
                "email" => "Waltraud.Morales@ucf.edu",
                "institution" => "University of Central Florida",
                "address_personal" => "",
                "address_institution" => "Dept. Political Science, P.O. Box 161356, 4000 Central Florida Blvd., Orlando, FL 32816 -1356",
                "country_id" => "US",
                "city" => "Orlando, FL",
                "areas" => "Relaciones Internacionales, Politica y Historia"
            ],
            [
                "discipline" => "",
                "email" => "moratlu@earlham.edu",
                "institution" => "Earlham College",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Richmond, Indiana",
                "areas" => ""
            ],
            [
                "discipline" => "Economía y Ciencias Políticas",
                "email" => "jmoreno@mail.uark.edu",
                "institution" => "University of Arkansas",
                "address_personal" => "",
                "address_institution" => "Fayetteville, AR 72703",
                "country_id" => "US",
                "city" => "Fayetteville, AR 72703",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "potosi@worldnet.att.net",
                "institution" => "Kent State University",
                "address_personal" => "",
                "address_institution" => "MCLS Kent, Ohio",
                "country_id" => "US",
                "city" => "Ohio.",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "jmurillo@ceibo.entelnet.bo",
                "institution" => "CELPADS",
                "address_personal" => "",
                "address_institution" => "Av. 6 de Agosto 1504, Edfi.Gosalvez",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "amuyolema@wisc.edu",
                "institution" => "University of Wisconsin",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Madison WI",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura, Música",
                "email" => "silvia@nycap.rr.com",
                "institution" => "State University of New York. Dept. Hispacis Studies",
                "address_personal" => "Dept. of Hispanic Studies Humanities 215",
                "address_institution" => "Washinton Ave.",
                "country_id" => "US",
                "city" => "Albany, NY 12222",
                "areas" => "Literatura, música, estudios culturales, escritura de mujeres"
            ],
            [
                "discipline" => "",
                "email" => "javichobiker@hotmail.com",
                "institution" => "Carrera de Geografia UMSA",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Geografía"
            ],
            [
                "discipline" => "",
                "email" => "wnunezr@hotmail.com",
                "institution" => "",
                "address_personal" => "Pedro Caballero 6982",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz IRPAVI",
                "areas" => "Desarrollo productivo mypes"
            ],
            [
                "discipline" => "",
                "email" => "oblitasjorge@hotmail.com",
                "institution" => "",
                "address_personal" => "Av. Calancha 1297",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "nweavero@ucsd.edu",
                "institution" => "CILAS-UCSD",
                "address_personal" => "",
                "address_institution" => "9500 Gilman Drive # 0528",
                "country_id" => "CA",
                "city" => "La Jolla, CA",
                "areas" => ""
            ],
            [
                "discipline" => "Antropólogo",
                "email" => "jossio@pucp.edu.pe",
                "institution" => "Pontificia Universidad Católica del Peru",
                "address_personal" => "",
                "address_institution" => "Av. Universitaria s/n Pueblo Libre",
                "country_id" => "PE",
                "city" => "Lima 18",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "foswald.spanishlit@cox.net",
                "institution" => "University of California, Riverside",
                "address_personal" => "",
                "address_institution" => "2401 HMNSS Building",
                "country_id" => "US",
                "city" => "San Diego, CA 92119-1205",
                "areas" => "Literatura Testimonial de la época de las dictaduras militares, Literatura Indigenista, Autores marginales, obras poco conocidas de carácter social y de denuncia"
            ],
            [
                "discipline" => "Cineasta",
                "email" => "bisovando@hotmail.com",
                "institution" => "Producciones Nicobis",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "foviedo@caoba.entelnet.bo",
                "institution" => "",
                "address_personal" => "Edif.Montebello 301, Cota Cota",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "ppacheco@clarku.edu",
                "institution" => "Clark University, Graduate School of Geography",
                "address_personal" => "Graduate School of Geography 950 Main Street",
                "address_institution" => "950 Main Street",
                "country_id" => "US",
                "city" => "Worcester, MA 01610",
                "areas" => "Ecología política, desarrollo, uso de la tierras"
            ],
            [
                "discipline" => "Economía",
                "email" => "paulk@fiu.edu",
                "institution" => "Florida International University",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Miami FL",
                "areas" => "Economía, administración pública, etica"
            ],
            [
                "discipline" => "Cultura y Polìtica",
                "email" => "jep29@cornell.edu",
                "institution" => "Cornell University",
                "address_personal" => "",
                "address_institution" => "312 Morrill Hall",
                "country_id" => "US",
                "city" => "Ithaca NY 14853",
                "areas" => "Ficción y temas políticos y culturales"
            ],
            [
                "discipline" => "",
                "email" => "ampasz@ucb.edu.bo",
                "institution" => "Universidad Católica Boliviana San&gt; &gt; Pablo",
                "address_personal" => "",
                "address_institution" => "A.v. 14 de septiembre No 4807, OBRAJES",
                "country_id" => "BO",
                "city" => "IRPAVI, La Paz -",
                "areas" => "Literatura Artes, Cultura"
            ],
            [
                "discipline" => "Historia",
                "email" => "paulapena@cotas.com.bo",
                "institution" => "",
                "address_personal" => "Junín 151",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "epatino@caoba.entelnet.bo",
                "institution" => "ESPACIO PATIÑO",
                "address_personal" => "",
                "address_institution" => "Av- Ecuador, Sopocachi",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "peralta@ih.csic.es",
                "institution" => "CSIC",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "ES",
                "city" => "Madrid, 28014",
                "areas" => ""
            ],
            [
                "discipline" => "Ecología Política",
                "email" => "bernardo.peredo@green.ox.ac.uk",
                "institution" => "School of Geography, Univ. Of Oxford",
                "address_personal" => "",
                "address_institution" => "South Parks Road",
                "country_id" => "GB",
                "city" => "Oxford OX1, 3QY",
                "areas" => "Biodiversidad, Medioa Ambiente, Desarrollo Sostenible, Pobreza y Desigualdad"
            ],
            [
                "discipline" => "Antropología, Arqueología",
                "email" => "dpereira@bo.net",
                "institution" => "Museo UMSS",
                "address_personal" => "",
                "address_institution" => "UMSS",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "letijavi@supernet.com.bo",
                "institution" => "Fundación Millenium",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "cperez@csufresno.edu",
                "institution" => "California State University",
                "address_personal" => "",
                "address_institution" => "5340 North Campus Drive M/S SS97",
                "country_id" => "US",
                "city" => "Fresno, Cal 93740-8019",
                "areas" => "Caudillos polìticos"
            ],
            [
                "discipline" => "",
                "email" => "poncearauco@yahoo.com",
                "institution" => "Universidad Católica Boliviana",
                "address_personal" => "",
                "address_institution" => "Av. 14 de Septiembre 4807",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "johnpope@netnitco.net",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "abnb@mara.scr.entelnet.bo",
                "institution" => "Archivo y Biblioteca Nacional",
                "address_personal" => "",
                "address_institution" => "Calle España",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "gapotter@albatros.cnb.net",
                "institution" => "COCAMTROP-CONLACTRAHO",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "areprada@mail.megalink.com",
                "institution" => "Fac.Humanidades, UMSA",
                "address_personal" => "",
                "address_institution" => "Monoblock central piso 11",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Literatura boliviana y comparada, estudios culturales"
            ],
            [
                "discipline" => "Historia",
                "email" => "presta@mail.retina.ar",
                "institution" => "Universidad de Buenos Aires",
                "address_personal" => "",
                "address_institution" => "25 de Mayo 217 No. 2",
                "country_id" => "AR",
                "city" => "Bs. Aires",
                "areas" => ""
            ],
            [
                "discipline" => "Sociología, Historia",
                "email" => "isanday@ceibo.entelnet.bo",
                "institution" => "CELPADS",
                "address_personal" => "",
                "address_institution" => "Av. 6 de Agosto, Edif. Gosalvez",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Emeritus Professor of Spanish",
                "email" => "lquiroz@comcast.net",
                "institution" => "Saint Michael's College",
                "address_personal" => "6430 North Alison Lane, Fresno, CA 93711 USA",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Colchester, Vermont",
                "areas" => "Peninsular Literature"
            ],
            [
                "discipline" => "",
                "email" => "ravsh8@yahoo.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "MY",
                "city" => "Selangor",
                "areas" => "Política"
            ],
            [
                "discipline" => "",
                "email" => "ivan_rebolledo@yahoo.com",
                "institution" => "Naciones Unidas",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "New York, NE 10150",
                "areas" => "Política, integración, desarrollo y política económica"
            ],
            [
                "discipline" => "",
                "email" => "redmann@mindspring.com",
                "institution" => "",
                "address_personal" => "9701 Lake Forest Blvd",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "NOLA, 70127",
                "areas" => "Derecho, comercio internacional"
            ],
            [
                "discipline" => "",
                "email" => "wreiser@holycross.edu",
                "institution" => "Holy Cross College",
                "address_personal" => "",
                "address_institution" => "Worcester MA 01610",
                "country_id" => "US",
                "city" => "Worcester MA 01610",
                "areas" => "Cultura y Religión"
            ],
            [
                "discipline" => "",
                "email" => "tr364@nyu.edu",
                "institution" => "New York University, Dept. Anthropology",
                "address_personal" => "",
                "address_institution" => "University of New York",
                "country_id" => "US",
                "city" => "New York, NY",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "rice@unm.edu",
                "institution" => "Universityof New Mexico, Dept.Political Science",
                "address_personal" => "",
                "address_institution" => "Albequerque, NM 87131",
                "country_id" => "US",
                "city" => "Albuquerque NM 87131",
                "areas" => "Política Indígena"
            ],
            [
                "discipline" => "",
                "email" => "richarkj@wfu.edu",
                "institution" => "Wake Forest University",
                "address_personal" => "",
                "address_institution" => "Winston -Salem. NC",
                "country_id" => "US",
                "city" => "Winston-Salem",
                "areas" => "Literatura, Cine"
            ],
            [
                "discipline" => "Arqueología",
                "email" => "clauri68@yahoo.com",
                "institution" => "UMSA",
                "address_personal" => "Guyana 1784",
                "address_institution" => "Av. Villazón 1990",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "tessa@interlink.es",
                "institution" => "Embajada de Bolivia en España",
                "address_personal" => "",
                "address_institution" => "Embajada de Bolivia",
                "country_id" => "ES",
                "city" => "Madrid, 28014",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "oriverar@utk.edu",
                "institution" => "University of Tennessee",
                "address_personal" => "",
                "address_institution" => "701 McClung Tower",
                "country_id" => "US",
                "city" => "Knoxville, TN 37996",
                "areas" => "Literatura y Cultura"
            ],
            [
                "discipline" => "Antropología",
                "email" => "riviere@ehess.fr",
                "institution" => "CERMA - EHESS",
                "address_personal" => "",
                "address_institution" => "54 Bd. Raspail, 75006",
                "country_id" => "FR",
                "city" => "Paris 75006",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "nrobins1@yahoo.com",
                "institution" => "Lecturer, North Carolina State University",
                "address_personal" => "",
                "address_institution" => "Center for Latin American Studies",
                "country_id" => "US",
                "city" => "",
                "areas" => "rebeliones indigenas, conflictos etnicos"
            ],
            [
                "discipline" => "Historia",
                "email" => "jlrocag@acelerate.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "froig@ifc.org",
                "institution" => "International Finance Corp.",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Washington DC",
                "areas" => ""
            ],
            [
                "discipline" => "Ciencias políticas",
                "email" => "gorojas_99@yahoo.com",
                "institution" => "CIDES-UMSA",
                "address_personal" => "",
                "address_institution" => "Av. 14 de Septiembre , Obrajes",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "libreriaboliviana@yahoo.com",
                "institution" => "Librerìa Boliviana",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "Sociología",
                "email" => "prossell@hotmail.com",
                "institution" => "CEDLA",
                "address_personal" => "",
                "address_institution" => "Av. Jaimes Freyre 2940",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Economía y Empleo"
            ],
            [
                "discipline" => "",
                "email" => "vruiz@jburroughs.org",
                "institution" => "John Burroughs School",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "St. Louis, Missouri",
                "areas" => ""
            ],
            [
                "discipline" => "Arqueología",
                "email" => "sagmar@mail.megalink.com",
                "institution" => "Centro de Investigaciones Antropológicas Tiwanaku",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz, Boliva",
                "areas" => ""
            ],
            [
                "discipline" => "Literatura",
                "email" => "salmon@loyno.edu",
                "institution" => "Loyola University New Orleans",
                "address_personal" => "",
                "address_institution" => "6363 St.Charles Ave. Box 229",
                "country_id" => "US",
                "city" => "New Orleans. LA 70118",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "sanabria@pitt.edu",
                "institution" => "University of Pittsburgh",
                "address_personal" => "",
                "address_institution" => "Pittsburgh, PA 15260",
                "country_id" => "US",
                "city" => "Pittsburgh, PA 15260",
                "areas" => "Economia politica; antropologia cultural; demografia historica"
            ],
            [
                "discipline" => "",
                "email" => "fsanchezdelozada@6sens.com",
                "institution" => "Inst.Pludiscip.Estudios s.A.L.",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "FR",
                "city" => "Toulouse",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "daclasa@mara.scr.entelnet.bo",
                "institution" => "Universidad San Francisco Xavier de Chuquisaca",
                "address_personal" => "",
                "address_institution" => "Guatemala No. 50",
                "country_id" => "BO",
                "city" => "Sucre",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "s_an@msn.com",
                "institution" => "CUNY Graduate Center",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "New York",
                "areas" => "Historia, Música"
            ],
            [
                "discipline" => "Antropología",
                "email" => "scarboro@uiuc.edu",
                "institution" => "Universidad de Aquino de Bolivia -",
                "address_personal" => "",
                "address_institution" => "Unidad Académica Cochabamba",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "andreisch@mtu-net.ru",
                "institution" => "Inst.Historia Univ.Acad.Ciencias de Rusia",
                "address_personal" => "",
                "address_institution" => "Leninski pr. 32A",
                "country_id" => "RU",
                "city" => "Moscú 117334",
                "areas" => "Historia Siglo XIX, siglo XX"
            ],
            [
                "discipline" => "",
                "email" => "fs7@duke.edu",
                "institution" => "Romance Studies, Duke University",
                "address_personal" => "",
                "address_institution" => "Durham NC",
                "country_id" => "US",
                "city" => "Durham NC",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "jscholl@ufl.edu",
                "institution" => "University of Florida",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "",
                "areas" => "Colonial Charcas"
            ],
            [
                "discipline" => "",
                "email" => "fescully@loyno.edu",
                "institution" => "Loyola University New Orleans",
                "address_personal" => "",
                "address_institution" => "Loyola University, New Orleans",
                "country_id" => "US",
                "city" => "N.Orleans, LA 70118",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "Brian.Selmeski@rmc.ca",
                "institution" => "Royal Military College of Canada",
                "address_personal" => "",
                "address_institution" => "533 Portsmouth Ave 211",
                "country_id" => "CA",
                "city" => "Kingston ON K7M 7H7 Can.",
                "areas" => "Militares, Poblaciones indígenas"
            ],
            [
                "discipline" => "",
                "email" => "m.shakow@vanderbilt.edu",
                "institution" => "Vanderbilt University",
                "address_personal" => "124 Garland Hall, 2301 Vanderbilt Place",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Nashville, TN",
                "areas" => "Anthropology"
            ],
            [
                "discipline" => "",
                "email" => "shanqueros@berkeley.edu",
                "institution" => "University of California, Berkeley",
                "address_personal" => "Oakland, CA",
                "address_institution" => "",
                "country_id" => "CA",
                "city" => "Berkeley, CA",
                "areas" => "Regime change, race and nation, water and development"
            ],
            [
                "discipline" => "",
                "email" => "mshea@tulane.edu",
                "institution" => "Depart.Spanish &amp; Portuguese, Univ. Tulane",
                "address_personal" => "",
                "address_institution" => "Tulane University",
                "country_id" => "US",
                "city" => "New Orleans LA 70118",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "rsmale@mail.utexas.edu",
                "institution" => "University of Texas at Austin",
                "address_personal" => "",
                "address_institution" => "University of Texas",
                "country_id" => "US",
                "city" => "Austin, Texas 78712",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "boliviahugh@yahoo.com",
                "institution" => "Michigan State University",
                "address_personal" => "734 904 2955",
                "address_institution" => "A 176 PSSB, East Lansing",
                "country_id" => "US",
                "city" => "MI 48824",
                "areas" => "Desarrollo agrícola"
            ],
            [
                "discipline" => "",
                "email" => "msnipes@westga.edu",
                "institution" => "State University of West Georgia",
                "address_personal" => "",
                "address_institution" => "Carolton, GA 30118",
                "country_id" => "US",
                "city" => "USA",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "algundiasol@yahoo.com",
                "institution" => "New York University",
                "address_personal" => "",
                "address_institution" => "53 Washington Square Park South",
                "country_id" => "US",
                "city" => "New York, NY",
                "areas" => "Reforma Agraria"
            ],
            [
                "discipline" => "Historia",
                "email" => "emsordo@miami.edu",
                "institution" => "The Art Museum at Florida Internat.Univers.",
                "address_personal" => "",
                "address_institution" => "University Park, PC 110",
                "country_id" => "US",
                "city" => "Miami, Florida 33199",
                "areas" => "Historia Social y Cultural, Colonia (Siglos 16th y 17), Historia arquitectura, religión"
            ],
            [
                "discipline" => "",
                "email" => "haciendo@bolivia.com",
                "institution" => "Haciendo Bolivia",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Liderazgo, Etica, Democracia"
            ],
            [
                "discipline" => "",
                "email" => "souzamm@slu.com",
                "institution" => "Saint Louis University",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Saint Louis Missouri",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "bcstjohn@mtco.com",
                "institution" => "Independent Scholar",
                "address_personal" => "",
                "address_institution" => "1620 Northedge Court",
                "country_id" => "US",
                "city" => "Dunlap, Illinois 61525",
                "areas" => "Historia, Política Extranjera, Desarrollo económico"
            ],
            [
                "discipline" => "Sociólogo",
                "email" => "pablostefanoni@yahoo.com.ar",
                "institution" => "Fundac.Investig.Políticas y Sociales FIS Y P",
                "address_personal" => "",
                "address_institution" => "Potosí 4456 piso 12 depto. A.",
                "country_id" => "AR",
                "city" => "Buenos Aires, 1199",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "istein@upsa.edu.bo",
                "institution" => "Univ.Privada de Santa Cruz UPSA",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "Hist.Cultural-Arqueol.",
                "email" => "spjacobs@tulane.edu",
                "institution" => "Tulane University",
                "address_personal" => "",
                "address_institution" => "Tulane University, LA 70118",
                "country_id" => "US",
                "city" => "New Orleans, LA 70118",
                "areas" => "Arte, Arquitectura, música"
            ],
            [
                "discipline" => "Historia",
                "email" => "mstephen@purdue.edu",
                "institution" => "Purdue University, Dept.Foreign Languages",
                "address_personal" => "",
                "address_institution" => "West Lafayette, IN 47907",
                "country_id" => "US",
                "city" => "USA",
                "areas" => "Pueblos indígenas, género y teoría feminista"
            ],
            [
                "discipline" => "",
                "email" => "carolyn_stilwell@wsu.edu",
                "institution" => "Washington State University",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Pullman, WA",
                "areas" => "Anthropology, Conflict Resolution, Nonviolence"
            ],
            [
                "discipline" => "",
                "email" => "stovall.pete@uwlax.edu",
                "institution" => "University of Wisconsin-La Crosse",
                "address_personal" => "2216 State St., La Crosse, WI 54601",
                "address_institution" => "1725 State St., Morris Hall, Dept. of Educational Studies",
                "country_id" => "US",
                "city" => "",
                "areas" => "international education, education reform law, bilingual education"
            ],
            [
                "discipline" => "Sociología",
                "email" => "hugo.jose.suarez@undp.org",
                "institution" => "IDH - PNUD",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "suarez1@un.org",
                "institution" => "United Nations",
                "address_personal" => "",
                "address_institution" => "225 Hillside Place Eastchester",
                "country_id" => "US",
                "city" => "New York 10709",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "jmtalmage@aol.com",
                "institution" => "Tulane University",
                "address_personal" => "",
                "address_institution" => "New Orleans, LA 70118",
                "country_id" => "US",
                "city" => "New Orleans LA 70118",
                "areas" => ""
            ],
            [
                "discipline" => "Filosofía",
                "email" => "luistapiam@yahoo.com",
                "institution" => "CIDES",
                "address_personal" => "",
                "address_institution" => "Av. 14 de Septiembre, Obrajes",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "jeremytarbox@gmail.com",
                "institution" => "Universidad del Salvador",
                "address_personal" => "",
                "address_institution" => "Direccion de Cooperacion e Intercambio Internacional",
                "country_id" => "AR",
                "city" => "Buenos Aires, Distrito Federal",
                "areas" => "Relaciones Internacionales"
            ],
            [
                "discipline" => "",
                "email" => "etarica@socrates.berkeley.edu",
                "institution" => "University of California",
                "address_personal" => "",
                "address_institution" => "University of California",
                "country_id" => "US",
                "city" => "Berkeley, CA 94720",
                "areas" => "Literatura, Lenguaje y Etnicidad"
            ],
            [
                "discipline" => "Historia",
                "email" => "hthiessen@western.edu",
                "institution" => "Western State College",
                "address_personal" => "",
                "address_institution" => "Gunnison CO 81231",
                "country_id" => "US",
                "city" => "CO 81231",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "rtomicha@yahoo.com",
                "institution" => "Instituto de Misionología, UCB",
                "address_personal" => "44522670",
                "address_institution" => "Av. Ramón Rivero esq c/ Oruro",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "wgtorresarmas@yahoo.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Rockville, MD",
                "areas" => "Relaciones Internacionales"
            ],
            [
                "discipline" => "",
                "email" => "A.A.Tsolakis@warwick.ac.uk",
                "institution" => "University of Warwick",
                "address_personal" => "",
                "address_institution" => "28 Kenilworth Road",
                "country_id" => "GB",
                "city" => "Leamington Spa, United Kingdom",
                "areas" => "International Political Economy and research methods, globalisation and global governance, state theory"
            ],
            [
                "discipline" => "Literatura, Cultura",
                "email" => "unzueta.1@osu.edu",
                "institution" => "The Ohio State University",
                "address_personal" => "",
                "address_institution" => "298 Hagerty Hall, College Road",
                "country_id" => "US",
                "city" => "Columbus OH, 43210",
                "areas" => "Literatura y Cultura, Siglo XIX"
            ],
            [
                "discipline" => "Histria",
                "email" => "CASANOUR@CEIBO.ENTELNET.BO",
                "institution" => "Coordinadora de Historia",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "mur_cl@yahoo.com",
                "institution" => "Universidad de Chile, Dep. de Antropología",
                "address_personal" => "",
                "address_institution" => "Universidad de Chile",
                "country_id" => "CL",
                "city" => "Santiago",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "Blas_U@yahoo.com",
                "institution" => "Universidad De Potsdam",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "DE",
                "city" => "Berlin",
                "areas" => ""
            ],
            [
                "discipline" => "Ciencias Políticas",
                "email" => "sebasu76@hotmail.com",
                "institution" => "Institut d'Etudes Politiques de Paris",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "FR",
                "city" => "París",
                "areas" => "Pensamiento político y procesos ideológicos, despolitización, corrupción, estados de sitio, violencia, recuperación de la memoria colectiva sobre las dictaduras, Historia de América Latina en el siglo"
            ],
            [
                "discipline" => "",
                "email" => "valdivia@email.unc.edu",
                "institution" => "University of North Carolina Chapel Hill",
                "address_personal" => "410 Tinkerbell Rd., Chapel Hill, NC 27517 USA",
                "address_institution" => "Geography Department, Saunders Hall, UNC-CH, NC 27516",
                "country_id" => "US",
                "city" => "",
                "areas" => "agriculture, lowlands"
            ],
            [
                "discipline" => "Ciencias Políticas",
                "email" => "dvancott@tulane.edu",
                "institution" => "Department of Political Science",
                "address_personal" => "Tulane University",
                "address_institution" => "Norman Mayer Hall, New Orleans, 504-862-8307",
                "country_id" => "US",
                "city" => "LA 70118, Estados Unidos",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "hout0091@wxs.nl",
                "institution" => "",
                "address_personal" => "Schoonstraat 55 Heesch",
                "address_institution" => "",
                "country_id" => "NL",
                "city" => "Netherlands, 5348 AM",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "gvvalen@yahoo.com",
                "institution" => "200 C Madison St. NE",
                "address_personal" => "",
                "address_institution" => "Albuquerque, NM 87108",
                "country_id" => "US",
                "city" => "Albuquerque, NM 87108",
                "areas" => "Etnohistoria, Goma, Beni"
            ],
            [
                "discipline" => "",
                "email" => "vargas@fas.harvard.edu",
                "institution" => "Harvard University",
                "address_personal" => "",
                "address_institution" => "Cambridge, MA 02138",
                "country_id" => "US",
                "city" => "Cambridge, MA 02138",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "tany_sweetz@yahoo.com",
                "institution" => "Folklore Bolivia",
                "address_personal" => "",
                "address_institution" => "Lake Balboa, CA 91406",
                "country_id" => "US",
                "city" => "Lake Balboa, CA 91406",
                "areas" => "Cultura, Folclore"
            ],
            [
                "discipline" => "",
                "email" => "centuriasgjm@yahoo.es",
                "institution" => "Universidad de Aquino de Bolivia",
                "address_personal" => "",
                "address_institution" => "Km. 8.5 a Sacaba",
                "country_id" => "BO",
                "city" => "Cochabamba",
                "areas" => "Derecho, Folclore, Danza"
            ],
            [
                "discipline" => "",
                "email" => "mauriciovargas@hotmail.com",
                "institution" => "Universidad Autónoma de Nuevo León",
                "address_personal" => "",
                "address_institution" => "Monterrey, Nueva León",
                "country_id" => "MX",
                "city" => "Monterrey",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "yngridvespa@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "Santa Cruz",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "titov@mail.zuper.net",
                "institution" => "Univ. Autónoma G.René Moreno",
                "address_personal" => "",
                "address_institution" => "Campo Universitario",
                "country_id" => "BO",
                "city" => "Santa Cruz.",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "mvap@wam.umd.edu",
                "institution" => "University of Maryland at College Park",
                "address_personal" => "",
                "address_institution" => "1673 Columbia Road NW",
                "country_id" => "US",
                "city" => "Washington DC 20009",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "magda@jhuvms.hcf.jhu.edu",
                "institution" => "Johns Hopkins University",
                "address_personal" => "",
                "address_institution" => "Baltimore, Maryland 21210",
                "country_id" => "US",
                "city" => "Maryland 21210",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "vvonstruen@post.harvard.edu",
                "institution" => "Harvard School of Public Health",
                "address_personal" => "",
                "address_institution" => "Arlington, VA 22201",
                "country_id" => "US",
                "city" => "Arlington VA 22201",
                "areas" => "Derecho, Democracia, desarrollo, pueblos indígenas"
            ],
            [
                "discipline" => "",
                "email" => "davacano@princeton.edu",
                "institution" => "Princeton University",
                "address_personal" => "",
                "address_institution" => "Corwin Hall, Princeton",
                "country_id" => "US",
                "city" => "Princeton, NJ 08544",
                "areas" => "Economía, Política contemporánea"
            ],
            [
                "discipline" => "Arqueología",
                "email" => "jwalker@sas.upenn.edu",
                "institution" => "Univ.Pennsylvania. Museum Arch.&amp; Anth.",
                "address_personal" => "",
                "address_institution" => "33rd an Spruce Streets",
                "country_id" => "US",
                "city" => "Philadelphia, PA 19104",
                "areas" => "Arqueología Beni"
            ],
            [
                "discipline" => "",
                "email" => "mesquito@operamail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "",
                "areas" => ""
            ],
            [
                "discipline" => "",
                "email" => "waszkis@mindspring.com",
                "institution" => "",
                "address_personal" => "441 Wickford Pt. Road",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Wickford, RI 02852",
                "areas" => "Cultura aymara"
            ],
            [
                "discipline" => "",
                "email" => "aweldon@unca.edu",
                "institution" => "UNCA",
                "address_personal" => "828 251 6285",
                "address_institution" => "CPO 1640, One University Hgts.",
                "country_id" => "US",
                "city" => "Asheville, NC 28804",
                "areas" => "Cultura aymara, Literatura, temas de salud"
            ],
            [
                "discipline" => "Antropología",
                "email" => "wightman@uiuc.edu",
                "institution" => "University of Illinois",
                "address_personal" => "",
                "address_institution" => "Univ.of Illinois, Urbana-Champaign",
                "country_id" => "US",
                "city" => "Urbana, IL 61801",
                "areas" => "Religión, Pentecostalismo"
            ],
            [
                "discipline" => "",
                "email" => "david_mj_wood@yahoo.com",
                "institution" => "Universidad del Claustro de Sor Juana",
                "address_personal" => "",
                "address_institution" => "Calle Izazaga 92, Col. Centro, 06080 México D.F.",
                "country_id" => "MX",
                "city" => "D.F.",
                "areas" => "Cine, Video, Estudios Culturales"
            ],
            [
                "discipline" => "Historia",
                "email" => "artjrwormald@hotmail.com",
                "institution" => "",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "CA",
                "city" => "Mossissauga, Ontario CANADA",
                "areas" => "Historia, Aymara, Iglesia, Literatura, Quechua"
            ],
            [
                "discipline" => "",
                "email" => "twrightrn@aol.com",
                "institution" => "UCLA",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "US",
                "city" => "Los Angeles, CA",
                "areas" => "Homosexualidad Masculina"
            ],
            [
                "discipline" => "",
                "email" => "zallescueto@hotmail.com",
                "institution" => "Université Laval",
                "address_personal" => "",
                "address_institution" => "Cité Universitaire",
                "country_id" => "CA",
                "city" => "Quebec",
                "areas" => ""
            ],
            [
                "discipline" => "Genealogía",
                "email" => "elvirazilvetipen@mail.megalink.com",
                "institution" => "Inst. Boliviana de Genealogia",
                "address_personal" => "",
                "address_institution" => "",
                "country_id" => "BO",
                "city" => "La Paz",
                "areas" => "Genealogía"
            ],
            [
                "discipline" => "",
                "email" => "ezorn@mail.ucf.edu",
                "institution" => "Univ. of Central Florida, Dept.Sociol.&amp; Anth.",
                "address_personal" => "",
                "address_institution" => "Orlando FL 32816-1360",
                "country_id" => "US",
                "city" => "Orlando FL 32816-1360",
                "areas" => ""
            ],
            [
                "discipline" => "Historia",
                "email" => "azulawsk@smith.ed",
                "institution" => "Smith College",
                "address_personal" => "413-585-3727",
                "address_institution" => "Northampton MA 01063",
                "country_id" => "US",
                "city" => "Northampton MA 01063",
                "areas" => "Historia, L. Amer.Stud."
            ]
        ];

        foreach ($profiles as $profile) {
            $user = User::where('email', $profile["email"])->first();
            if ($user) {

                $userProfile = [
                    "name" => $user->name,
                    "discipline" => $profile["discipline"],
                    "institution" => $profile["institution"],
                    "address_personal" => $profile["address_personal"],
                    "address_institution" => $profile["address_institution"],
                    "areas" => $profile["areas"],
                    "city" => $profile["city"],
                    "country_id" => $profile["country_id"],
                    "user_id" => $user->id,
                ];

                if (isset($profile["birthday"])) {
                    $userProfile["birthday"] = $profile["birthday"];
                }

                if (isset($profile["content"])) {
                    $userProfile["content"] = $profile["content"];
                }

                if (isset($profile["facebook"])) {
                    $userProfile["facebook"] = $profile["facebook"];
                }

                if (isset($profile["twitter"])) {
                    $userProfile["twitter"] = $profile["twitter"];
                }

                if (isset($profile["google"])) {
                    $userProfile["google"] = $profile["google"];
                }

                if (isset($profile["skype"])) {
                    $userProfile["skype"] = $profile["skype"];
                }

                if (isset($profile["phone"])) {
                    $userProfile["phone"] = $profile["phone"];
                }

                if (isset($profile["website"])) {
                    $userProfile["website"] = $profile["website"];
                }

                Profile::create($userProfile);
            }
        }
    }
}
