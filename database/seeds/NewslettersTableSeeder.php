<?php

use App\Newsletter;
use Illuminate\Database\Seeder;

class NewslettersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('newsletters')->delete();

        $newsletters = [
            [
                "id" => 1,
                "lang" => "es",
                "title" => "Septiembre - Diciembre",
                "date" => "2013-09-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 1,
            ],
            [
                "id" => 2,
                "lang" => "es",
                "title" => "Abril - Agosto",
                "date" => "2013-04-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 2,
            ],
            [
                "id" => 3,
                "lang" => "es",
                "title" => "Enero Abril",
                "date" => "2013-01-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 3,
            ],
            [
                "id" => 4,
                "lang" => "es",
                "title" => "Septiembre - Diciembre",
                "date" => "2012-09-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 4,
            ],
            [
                "id" => 5,
                "lang" => "es",
                "title" => "Agosto",
                "date" => "2012-08-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 5,
            ],
            [
                "id" => 6,
                "lang" => "es",
                "title" => "Abril",
                "date" => "2012-04-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 6,
            ],
            [
                "id" => 7,
                "lang" => "es",
                "title" => "Diciembre",
                "date" => "2011-12-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 7,
            ],
            [
                "id" => 8,
                "lang" => "es",
                "title" => "Mayo",
                "date" => "2011-05-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 8,
            ],
            [
                "id" => 9,
                "lang" => "es",
                "title" => "Agosto",
                "date" => "2010-08-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 9,
            ],
            [
                "id" => 10,
                "lang" => "es",
                "title" => "Diciembre",
                "date" => "2008-12-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 10,
            ],
            [
                "id" => 11,
                "lang" => "es",
                "title" => "Junio",
                "date" => "2008-06-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 11,
            ],
            [
                "id" => 12,
                "lang" => "es",
                "title" => "Noviembre",
                "date" => "2007-11-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 12,
            ],
            [
                "id" => 13,
                "lang" => "es",
                "title" => "Mayo",
                "date" => "2006-05-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 13,
            ],
            [
                "id" => 14,
                "lang" => "es",
                "title" => "Enero",
                "date" => "2006-01-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 14,
            ],
            [
                "id" => 15,
                "title" => "Septiembre",
                "date" => "2005-09-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 15,
                "lang" => "es",
            ],
            [
                "id" => 16,
                "lang" => "es",
                "title" => "Marzo",
                "date" => "2002-03-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 16,
            ],
            [
                "id" => 17,
                "lang" => "es",
                "title" => "Octubre",
                "date" => "2001-10-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 17,
            ],
            [
                "id" => 18,
                "lang" => "es",
                "title" => "Marzo",
                "date" => "2001-03-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 18,
            ],
            [
                "id" => 19,
                "lang" => "es",
                "title" => "Enero - Diciembre",
                "date" => "2014-01-01",
                "excerpt" => "",
                "image_id" => 25,
                "doc_id" => 19,
            ]
        ];

        foreach ($newsletters as $newsletter) {
            Newsletter::create($newsletter);
        }
    }
}
