<?php

use App\Doc;
use Illuminate\Database\Seeder;

class DocsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('docs')->delete();

        $docs = [
            [
                "id" => 1,
                "title" => "AEB_Boletin_16.pdf",
                "author" => "",
                "path" => "boletin/AEB_Boletin_16.pdf",
            ],
            [
                "id" => 2,
                "title" => "AEB_Boletin_15.pdf",
                "author" => "",
                "path" => "boletin/AEB_Boletin_15.pdf",
            ],
            [
                "id" => 3,
                "title" => "AEB_Boletin_14.pdf",
                "author" => "",
                "path" => "boletin/AEB_Boletin_14.pdf",
            ],
            [
                "id" => 4,
                "title" => "AEB_Boletin_13.pdf",
                "author" => "",
                "path" => "boletin/AEB_Boletin_13.pdf",
            ],
            [
                "id" => 5,
                "title" => "AEBoletin012_201208.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin012_201208.pdf",
            ],
            [
                "id" => 6,
                "title" => "AEBoletin011_201204.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin011_201204.pdf",
            ],
            [
                "id" => 7,
                "title" => "AEBoletin010_201112.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin010_201112.pdf",
            ],
            [
                "id" => 8,
                "title" => "AEBoletin009_201105.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin009_201105.pdf",
            ],
            [
                "id" => 9,
                "title" => "AEBoletin008_201008.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin008_201008.pdf",
            ],
            [
                "id" => 10,
                "title" => "AEBoletin007_200812.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin007_200812.pdf",
            ],
            [
                "id" => 11,
                "title" => "AEBoletin006_200806.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin006_200806.pdf",
            ],
            [
                "id" => 12,
                "title" => "AEBoletin005_200711.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin005_200711.pdf",
            ],
            [
                "id" => 13,
                "title" => "AEBoletin003_200605.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin003_200605.pdf",
            ],
            [
                "id" => 14,
                "title" => "AEBoletin002_200601.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin002_200601.pdf",
            ],
            [
                "id" => 15,
                "title" => "AEBoletin001_200509.pdf",
                "author" => "",
                "path" => "boletin/AEBoletin001_200509.pdf",
            ],
            [
                "id" => 16,
                "title" => "AEBoletin003_200203",
                "author" => "",
                "path" => "boletin/AEBoletin003_200203.php",
            ],
            [
                "id" => 17,
                "title" => "AEBoletin002_200110",
                "author" => "",
                "path" => "boletin/AEBoletin002_200110.php",
            ],
            [
                "id" => 18,
                "title" => "AEBoletin001_200103",
                "author" => "",
                "path" => "boletin/AEBoletin001_200103.php",
            ],
            [
                "id" => 19,
                "title" => "AEB_Boletin_17-18.pdf",
                "author" => "",
                "path" => "boletin/AEB_Boletin_17-18.pdf",
            ],
            [
                "id" => 20,
                "title" => "Table of contents / Índice",
                "author" => "",
                "path" => "revista/10.2/000_PoesiaTitlePageIndex.pdf"
            ],
            [
                "id" => 21,
                "title" => "Nota de los Editores",
                "author" => "Josefa Salmón y Oscar Vega Camacho.",
                "path" => "revista/10.2/00_Nota_Editores.pdf"
            ],
            [
                "id" => 22,
                "title" => "Poemas del autor",
                "author" => "Rubén Vargas",
                "path" => "revista/10.2/01_Ruben_Vargas.pdf",
            ],
            [
                "id" => 23,
                "title" => "Exposición de alto riesgo",
                "author" => "Marcia Mogro",
                "path" => "revista/10.2/02_Marcia_Mogro.pdf"
            ],
            [
                "id" => 24,
                "title" => "Poemas del autor",
                "author" => "Vilma Tapia",
                "path" => "revista/10.2/03_Vilma_Tapia.pdf"
            ],
            [
                "id" => 25,
                "title" => "Poemas del autor",
                "author" => "Juan de Dios Yapita",
                "path" => "revista/10.2/04_Juan_de_Dios_Yapita.pdf"
            ],
            [
                "id" => 26,
                "title" => "Ciudad Negada",
                "author" => "Juan José Pacheco Balanza ",
                "path" => "revista/10.2/05_Juan_Jose_Pacheco_Balanza.pdf"
            ],
            [
                "id" => 27,
                "title" => "Poemas del autor",
                "author" => "Sulma Montero ",
                "path" => "revista/10.2/06_Sulma_Montero.pdf"
            ],
            [
                "id" => 28,
                "title" => "Poemas del autor",
                "author" => "Benjamín Chavez ",
                "path" => "revista/10.2/07_Benjamin_Chavez.pdf"
            ],
            [
                "id" => 29,
                "title" => "Poemas del autor",
                "author" => "Gary Daher Canedo",
                "path" => "revista/10.2/08_Gary_Daher_Canedo.pdf"
            ],
            [
                "id" => 30,
                "title" => "Poemas del autor",
                "author" => "Juan Cristóbal Mac Lean ",
                "path" => "revista/10.2/09_Juan_Cristobal_Mac_Lean.pdf"
            ],
            [
                "id" => 31,
                "title" => "Hielo",
                "author" => "María Montserrat Fernández Murillo ",
                "path" => "revista/10.2/10_Maria_Montserrat_Fernandez.pdf"
            ],
            [
                "id" => 33,
                "title" => "Programa General",
                "author" => "AEB",
                "path" => "downloads/Llamado_a_simposios_Congreso_AEB_2015.pdf"
            ],
            [
                "id" => 34,
                "title" => "LLamado a ponencias Congreso AEB - Octubre 2017",
                "author" => "AEB",
                "path" => "downloads/llamado-ponencias-2015.pdf"
            ],
            [
                "id" => 35,
                "title" => "Llamado a Simposios Congreso AEB - Febrero 2017",
                "author" => "AEB",
                "path" => "downloads/llamado-ponencias-2015.pdf"
            ],
        ];

        foreach ($docs as $doc) {
            Doc::create($doc);
        }
    }
}
