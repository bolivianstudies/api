<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('regions')->delete();

        $regions = [
            [
                'id' => 'AC',
                'name' => 'Asia Central',
            ],
            [
                'id' => 'AM',
                'name' => 'Asia Meridional',
            ],
            [
                'id' => 'AW',
                'name' => 'Asia Occidental',
            ],
            [
                'id' => 'AE',
                'name' => 'Asia Oriental',
            ],
            [
                'id' => 'AS',
                'name' => 'Sudeste Asiático',
            ],
            [
                'id' => 'CR',
                'name' => 'Caribe',
            ],
            [
                'id' => 'CA',
                'name' => 'Centro América',
            ],
            [
                'id' => 'EM',
                'name' => 'Europa Meridional',
            ],
            [
                'id' => 'EW',
                'name' => 'Europa Occidental',
            ],
            [
                'id' => 'EE',
                'name' => 'Europa Oriental',
            ],
            [
                'id' => 'ES',
                'name' => 'Europa septentrional',
            ],
            [
                'id' => 'ME',
                'name' => 'Melanesia',
            ],
            [
                'id' => 'MI',
                'name' => 'Micronesia',
            ],
            [
                'id' => 'NA',
                'name' => 'Norte América',
            ],
            [
                'id' => 'PO',
                'name' => 'Polinesia',
            ],
            [
                'id' => 'SA',
                'name' => 'Sud América',
            ],
            [
                'id' => 'FC',
                'name' => 'África Central',
            ],
            [
                'id' => 'FM',
                'name' => 'África Meridional',
            ],
            [
                'id' => 'FW',
                'name' => 'África Occidental',
            ],
            [
                'id' => 'FE',
                'name' => 'África Oriental',
            ],
            [
                'id' => 'FS',
                'name' => 'África Septentrional',
            ],
            [
                'id' => 'OC',
                'name' => 'Oceanía',
            ],
        ];
        foreach ($regions as $region) {
            Region::create($region);
        }

    }
}