<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Image::class, function (Faker\Generator $faker) {

    $categories = array('abstract', 'animals', 'business', 'cats', 'city', 'food', 'nightlife', 'fashion', 'people', 'nature', 'sports', 'technics', 'transport');

    $photo = $categories[rand(0, (count($categories) - 1))];

    return [
        'path' => $faker->imageUrl(800, 600, $photo),
        'thumb' => $faker->imageUrl(200, 200, $photo),
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'activation_token' => md5($faker->safeEmail),
        'active' => rand(0, 1),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Role::class, function () {
    return [];
});

$factory->define(App\UserRole::class, function () {
    return [
        'user_id' => rand(1, 50),
        'role_id' => rand(1, 3),
    ];
});