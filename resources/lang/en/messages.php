<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the response macro to provide
    | localized responses for different server response scenarios.
    |
    */

    'unauthorized' => 'Unauthorized access',
    "error" => "Error",
    "forbidden" => "Forbidden",
    "success" => "Success",
    "saved" => "Saved",
    "badRequest" => "Bad request",
    "updated" => "The :model has been successfully updated",
    "deleted" => "The :model has been successfully deleted",
    "invalidCredentials" => "Invalid credentials",
    "logout" => "Good bye",
    "notFound" => "Not found",
    "expired" => "Expired",

);
