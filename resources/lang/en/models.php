<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to name the different schema models.
    |
    */

    'user' => 'The user',
    'profile' => 'The profile',
    'photo' => 'The photo',
    'image' => 'The image',
    'albumImage' => 'The album image',
    'serviceImage' => 'The service image',
    'education' => 'The education',
    'album' => 'The album',
    'page' => 'The page',
    'pageImage' => 'The page image',
    'sectionImage' => 'The section image',
    'pageSectionImage' => 'The page section imagen',
    'pageSectionFeature' => 'The page section feature',
    'pageSectionFeatureImage' => 'The page section feature image',
    'post' => 'The post',
    'category' => 'The category',
    'tag' => 'The tag',
    'subscriber' => 'The subscriber',
    'comment' => 'The comment',
    'video' => 'The video',
    'file' => 'The file',
    'password' => 'The password',
    'email' => 'The email',
    'service' => 'The service',
    'feature' => 'The feature',
    'sectionFeature' => 'The section feature',
    'section' => 'The section',
    'pageSection' => 'The page section',
);
