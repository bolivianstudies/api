<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Reason Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are for response message reasons which
    | complement information to the original message of the response.
    |
    */

    "unauthorized" => "You are not authorized to this resource",
    "forbidden" => "Your request is not allowed",
    "permissions" => "You don't have permissions to perform this action",
    "badRequest" => "Your request was unable to comply",
    "notLogged" => "You need to be logged in to perform this action",
    "deleteOwnUser" => "You can't delete your own user",
    "error" => "The server encountered a problem processing your request",
    "success" => "Your request has been successful",
    "csrf" => "There is a token mismatch problem",
    "invalidCredentials" => "Invalid email and password for an account.",
    "logout" => "The session was terminated",
    "missingFields" => "There are missing fields for this resource",
    "notFound" => "The resource you are requested doesn't exist",
    "subscribed" => "You are successfully subscribed to our newsletter",
    "notSubscribed" => "You weren't subscribed to our newsletter due to a server error",
    "commented" => "Your comment was posted",
    "notImageData" => "Include the directory and file name",
    "savedImages" => "Image saved and thumbs generated successfully",
    "notSavedImage" => "Unable to save the uploaded image record",
    "notSavedThumb" => "Unable to save the uploaded image thumb record",
    "notSavedCover" => "Unable to save the uploaded image cover record",
    "notSavedOriginalImage" => "Unable to save the uploaded origina image record",
    "notUserData" => "Include name, email and password to register a new user",
    "notActivationToken" => "The activation token is not present on the link",
    "invalidActivationToken" => "The activation token is not valid",
    "activated" => "Your account was successfully activated, login",
    "errorActivate" => "There was a server error and your account wasn't activated",
    "invalidActivationLink" => "The activation link is invalid or has expired",
    "notRemindData" => "You need to enter a valid email address to request your password reset",
    "notResetData" => "You need to provide a new password to reset",
    "notUrlData" => "You need to provide a url to shorten",
    "notSent" => "The message wasn't sent",
    "sent" => "The message wast sent to :email",
    "notMessageData" => "You must include name, email and message to deliver",
    "alreadySubscribed" => "You are already subscribed to our newsletter",
    "expired" => "The session has expired, you'll be redirected",
    "noSuperUserUpdate" => "You can't update another super user",
    "noSuperUserDelete" => "You can't delete a super user",
    "fileNotUploaded" => "The file wasn't uploaded correctly",
    "notUpdateData" => "Please fill the required fields to continue",
    "errorUserImage" => "Unable to generate the user image",
    "errorPostImage" => "Unable to generate the post image",
    "errorVideoImage" => "Unable to generate the video image",
    "notPostData" => "Include title and excerpt to add a new post",
    "notVideoData" => "Please fill the required fields to add a new video",

    // Models
    "errorDelete" => ":model was't deleted",
    "notExists" => ":model doesn't exist or has already been deleted",
    "notCreated" => ":model wasn't created due to a server error",
    "notUpdated" => ":model wasn't updated due to a server error",
    "exists" => ":model already exists",
    "notBelongs" => ":model doesn't belong to :owner",
    "updated" => ":model was updated",
    "duplicate" => ":model it's already taken",
);
