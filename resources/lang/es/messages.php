<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the response macro to provide
    | localized responses for different server response scenarios.
    |
    */

    'unauthorized' => 'Acceso no autorizado',
    "error" => "Error",
    "forbidden" => "Prohibido",
    "success" => "Éxito",
    "created" => "Creado",
    "saved" => "Guardado",
    "badRequest" => "Advertencia",
    "updated" => ":model se actualizó",
    "deleted" => ":model se eliminó",
    "invalidCredentials" => "Credenciales no válidas",
    "logout" => "Hasta pronto",
    "notFound" => "No se encontró",
    "expired" => "Expiró",

);
