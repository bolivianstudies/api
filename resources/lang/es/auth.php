<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'El usuario y contraseña no coinciden con nuestros registros.',
    'token' => 'No se pudo crear la llave.',
    'throttle' => 'Muchos intentos de ingreso. Por favor inténtalo nuevameante en :seconds segundos.',

];
