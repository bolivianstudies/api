<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted" => ":attribute debe ser aceptado.",
    "active_url" => ":attribute no es una URL válida.",
    "after" => ":attribute debe ser una fecha posterior a :date.",
    "alpha" => ":attribute debe contener solamente letras.",
    "alpha_dash" => ":attribute puede solamente tener letras, números, y guiones.",
    "alpha_num" => ":attribute puede solamente tener letras y números.",
    "array" => ":attribute debe ser un vector.",
    "before" => ":attribute debe ser una fecha anterior a :date.",
    "between" => array(
        "numeric" => ":attribute debe estar entre :min y :max.",
        "file" => ":attribute debe estar entre :min y :max kilobytes.",
        "string" => ":attribute debe estar entre :min y :max caracteres.",
        "array" => ":attribute debe contener entre :min y :max items.",
    ),
    "confirmed" => ":attribute no coincide.",
    "date" => ":attribute no es una fecha válida.",
    "date_format" => ":attribute no coincide con el formato :format.",
    "different" => ":attribute y :other deben ser diferentes.",
    "digits" => ":attribute debe contener :digits dígitos.",
    "digits_between" => ":attribute debe tener entre :min y :max dígitos.",
    "email" => ":attribute debe ser una dirección de correo electrónico válida.",
    "exists" => ":attribute no es válida.",
    "image" => ":attribute debe ser una imagen.",
    "in" => ":attribute no es válida.",
    "integer" => ":attribute debe ser un entero.",
    "ip" => ":attribute debe ser una dirección de IP válida.",
    "max" => array(
        "numeric" => ":attribute no debe ser mayor a :max.",
        "file" => ":attribute no puede ser mayor a :max kilobytes.",
        "string" => ":attribute no debe ser mayor a :max caracteres.",
        "array" => ":attribute no debe contener más de  :max items.",
    ),
    "mimes" => ":attribute debe ser de tipo: :values.",
    "min" => array(
        "numeric" => ":attribute debe ser al menos :min.",
        "file" => ":attribute debe tener al menos :min kilobytes.",
        "string" => ":attribute debe ser de al menos :min caracteres.",
        "array" => ":attribute debe contener al menos :min items.",
    ),
    "not_in" => ":attribute es inválida.",
    "numeric" => ":attribute debe ser un número.",
    "regex" => "El formato de :attribute es inválido.",
    "required" => ":attribute es requerido.",
    "required_if" => ":attribute es requerido cuando :other es :value.",
    "required_with" => ":attribute es requerido cuando :values es present.",
    "required_with_all" => ":attribute es requerido cuando :values es present.",
    "required_without" => ":attribute es requerido cuando :values es not present.",
    "required_without_all" => ":attribute es requerido cuando ninguno de :values está presente.",
    "same" => ":attribute y :other deben coincidir.",
    "size" => array(
        "numeric" => ":attribute debe ser :size.",
        "file" => ":attribute debe tener :size kilobytes.",
        "string" => ":attribute debe tener :size caracteres.",
        "array" => ":attribute debe contener :size items.",
    ),
    "unique" => ":attribute ya está en uso y no está disponible.",
    "url" => ":attribute no es un formato URL válido.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(
        'active' => array(
            'boolean' => 'El estado de activación debe ser activo o inactivo',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(
        'title' => 'el título',
        'name' => 'el nombre',
        'short' => 'el nombre corto',
        'excerpt' => 'el resumen',
        'description' => 'la descripción',
        'email' => 'la dirección de correo electrónico',
        'password' => 'la contraseña',
        'token' => 'el token',
        'password_confirmation' => 'la confirmación de la contraseña',
        'passwordConfirmation' => 'la confirmación de la contraseña',
        'confirmNewPassword' => 'la confirmación de la nueva contraseña',
        'confirmPassword' => 'la confirmación de contraseña',
        'newPassword' => 'la nueva contraseña',
        'image_id' => 'la imagen',
        'role_id' => 'el rol',
        'page_id' => 'la página',
        'section_id' => 'la sección',
        'feature_id' => 'la característica',
        'breed_id' => 'la raza',
        'city_id' => 'la ciudad',
        'active' => 'el estado de activación',
        'message' => 'el mensaje',
        'gender' => 'el género',
        'color' => 'el color',
        'content' => 'el contenido',
        'birthday' => 'la fecha de nacimiento',
        'nurtured' => 'el estado de esterilización/castración',
        'primary_phone' => 'el teléfono primario',
        'secondary_phone' => 'el teléfono secundario',
        'alternative_phone' => 'el teléfono alternativo',
        'primary_email' => 'la dirección de correo electrónico primaria',
        'secondary_email' => 'la dirección de correo electrónico secundaria',
        'address_street' => 'la calle',
        'address_zone' => 'la zona',
        'province' => 'la provincia',
        'medical' => 'la información médica',
        'insurance' => 'la información del seguro',
        'other' => 'la información complementaria',
        'pet_id' => 'la mascota',
        'owner_id' => 'el dueño de la mascota',
        'location' => 'la ubicación',
        'lat_lng' => 'las coordenadas',
        'qr_id' => 'el código QR',
        'order_status_id' => 'El estado de la orden',
        'payment_method_id' => 'La referencia del pago',
        'payment_id' => 'El pago',
        'billing_name' => 'El nombre de facturación',
        'billing_nit' => 'El NIT de facturación',
        'billing_email' => 'El correo electrónico de facturación ',
        'billing_country_id' => 'El país de facturación',
        'billing_city' => 'La ciudad de facturación',
        'billing_address' => 'La dirección de facturación',
        'billing_phone' => 'El teléfono de facturación',
        'shipping_name' => 'El nombre de envío',
        'shipping_email' => 'El correo electrónico de envío',
        'shipping_country_id' => 'El país de envío',
        'shipping_city' => 'La ciudad de envío',
        'shipping_phone' => 'El teléfono de envío',
        'shipping_address' => 'La dirección de envío',
        'parent_id' => 'El padre',
        'payment_token' => 'La llave de pago',
        'order_id' => 'La orden',
        'payer_id' => 'El comprador',
        'social_client' => 'El cliente de red social',
        'social_id' => 'La identidad de red social',
    ),

);
