<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben ser de al menos 6 caracteres y coincidir con la confirmación.',
    'reset' => 'Tu contraseña ha sido restablecida!',
    'sent' => 'Le hemos enviado un email con instrucciones para restablecer su contraseña!',
    'token' => 'Esta llave para restablecer su contraseña es inválida.',
    'user' => "Non podemos encontrar al usuario que coincide con esta dirección de correo electrónico.",

];
