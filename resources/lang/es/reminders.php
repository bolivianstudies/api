<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" => "Las contraseñas deben ser de al menos 6 caracteres y coincidir con la confirmación.",

    "user" => "No se puede encontrar al usuario con esa dirección de correo electrónico.",

    "token" => "La llave para restablecer la contraseña es inválida.",

    "sent" => "El recordatorio de contraseña fue enviado!",

    "reset" => "La contraseña fue cambiada!",

    "remind" => "Recordatorio de contraseña",

    "register" => "Bienvenido a AEB",

    "order" => "Tu orden en bolivianstudies.org",

    "subscribe" => "Tu subscripción en bolivianstudies.org",

);
