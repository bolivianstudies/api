<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to name the different schema models.
    |
    */

    'user' => 'El usuario',
    'profile' => 'El perfil',
    'photo' => 'La foto',
    'image' => 'La imagen',
    'albumImage' => 'La imagen del album',
    'serviceImage' => 'La imagen del servicio',
    'education' => 'La educación',
    'album' => 'El album',
    'page' => 'La página',
    'pageImage' => 'La imagen de la página',
    'sectionImage' => 'La imagen de la sección',
    'pageSectionImage' => 'La imagen de la sección de la página',
    'pageSectionFeature' => 'La característica de la sección de la página',
    'pageSectionFeatureImage' => 'La imagen de la característica de la sección de la página',
    'post' => 'La publicación',
    'category' => 'La categoría',
    'tag' => 'La etiqueta',
    'subscriber' => 'La subscripción',
    'comment' => 'El comentario',
    'video' => 'El video',
    'file' => 'El archivo',
    'password' => 'La contraseña',
    'email' => 'El correo electrónico',
    'service' => 'El servicio',
    'feature' => 'La característica',
    'sectionFeature' => 'La característica de la sección',
    'section' => 'La sección',
    'pageSection' => 'La sección de la página',
    'message' => 'El mensaje',
    'subscriberOptions' => 'La configuración de la subscripción',
    'owner' => 'El dueño',
    'pet' => 'La mascota',
    'qr' => 'La código QR',
    'petQr' => 'El código QR de la mascota',
    'history' => 'El registro de visita al perfil de :pet',
    'ownerPrivacy' => 'La configuración de privacidad del dueño',
    'petPrivacy' => 'La configuración de privacidad de la mascota',
    'agency' => 'La agencia',
    'billing' => 'Los datos de facturación',
    'ad' => 'La pieza publicitaria',
    'order' => 'La orden de compra',
    'product' => 'El producto',
    'productCategory' => 'La categoría del producto',
    'productTrack' => 'La pista del producto',
);
