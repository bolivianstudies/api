<hr style="border-left: none; border-right: none; border-top: none; border-bottom: 1px solid #EBEBEB; margin: 20px 0;">

<p style="text-align: center; font-size: 12px; color: #999999; margin: 40px 0; letter-spacing: 0.08em;">
    Asociación de Estudios Bolivianos <br>
    estudiosbolivianos.aeb@gmail.com
</p>
<p style="text-align: center; font-size: 12px; color: #999999; margin: 40px 0; letter-spacing: 0.08em;">
    <a href="{{config('app.url')}}" target="_blank"
       style="color: #FF462E; text-decoration: none;">{{config('app.domain')}}</a>
</p>

</div>
</div>
</body>
</html>