<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
</head>
<body style="font-family: 'Helvetica', 'Helvetica Neue', 'Arial', sans-serif; text-align: center;color: #4D4D4D; font-size: 16px; background: #FAFAFA; font-weight: 400; padding: 30px; box-sizing: border-box;">

<div style="background:#ffffff; max-width: 600px; margin: 0 auto; border-radius: 2px; padding: 30px 40px;box-sizing: border-box;">

    <div style="display: inline-block; margin: 40px auto 0;">
        <img src="{{config('app.url')}}img/assets/logo-dark.png"
             alt="AEB">

        <hr style="border-left: none; border-right: none; border-top: none; border-bottom: 1px solid #EBEBEB; margin: 20px 0;">