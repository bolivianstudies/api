@include('includes.emails.header')

<p style="margin: 40px 0;text-align: left">
    {{trans('emails.order')}}
</p>

<p style="margin: 40px 0; text-align:center">
    <span style="display: inline-block; background: none; border: 1px solid #FF462E; border-radius:30px; color: #FF462E; font-size: 22px; font-weight: normal; padding: 15px 30px;">
        {{$order['id']}}
    </span>
</p>

<p>
    {{trans('emails.orderToken')}}
</p>

<p style="margin: 40px 0;text-align: center">
    <a href="{{config('app.url')}}pagar?order={{$order['id']}}&token={{$order['token']}}"
       style="display: inline-block; text-decoration: none; font-weight: bold; padding: 10px 15px; border-radius: 2px; text-transform: uppercase; color: white; letter-spacing: 0.08em; font-size: 12px; text-align: center; background: #FF462E;">
        {{trans('emails.payOrder')}}
    </a>
</p>

<?if($newUser && $token !== null):?>
<p style="margin: 40px 0;text-align: left">{{trans('emails.newUser')}}</p>
<p style="margin: 40px 0;text-align: center">
    <a href="{{config('app.url')}}restablecer?email={{$user['email']}}&token={{$token}}"
       style="display: inline-block; text-decoration: none; font-weight: bold; padding: 10px 15px; border-radius: 2px; text-transform: uppercase; color: white; letter-spacing: 0.08em; font-size: 12px; text-align: center; background: #FF462E;">
        {{trans('emails.setPassword')}}
    </a>
</p>
<?endif?>

<p style="color: #999999;font-size: 14px; margin: 40px 0;text-align: left; background: #FAFAFA; border-radius: 2px; padding: 10px 20px; font-weight: 400">
    {{trans('emails.notYourRequest')}}
</p>

@include('includes.emails.footer')