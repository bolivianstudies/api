@include('includes.emails.header')

<p style="margin: 40px 0;text-align: left">
    {{trans('emails.hello', array('name' => $name))}}. {{trans('emails.resetRequest')}}
    <a style="color:#FF462E; text-decoration: none;" href="{{Config::get('app.url')}}ingresar"
       target="_blank">{{config('app.domain')}}</a>.
</p>

<p style="margin: 40px 0;">
    {{trans('emails.resetToComplete')}}
</p>

<p style="margin: 40px 0;">
    <a href="{{Config::get('app.url')}}restablecer?email={{$email}}&token={{$token}}"
       style="display: block; width: 50%; margin: auto; text-decoration: none; font-weight: 400; padding: 16px 0; border-radius: 2px; text-transform: uppercase; color: white; letter-spacing: 0.05em; font-size: 12px; text-align: center; background: #FF462E;">
        {{trans('emails.reset')}}
    </a>
</p>

<p style="color: #999999;font-size: 14px; margin: 40px 0;text-align: left; background: #FAFAFA; border-radius: 2px; padding: 10px 20px; font-weight: 400">
    {{trans('emails.notYourRequest')}}
</p>


@include('includes.emails.footer')
