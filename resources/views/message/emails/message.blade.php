@include('includes.emails.header')

<p style="margin: 40px 0;text-align: left">
    {{trans('emails.contactDescription', array('name' => $name, 'email' => $email))}}.
</p>

<p>
    {{trans('emails.contactAdditional', array('phone' => $phone, 'institution' => $institution))}}.
</p>

<p style="color: #999999;font-size: 14px; margin: 40px 0;text-align: left; background: #FAFAFA; border-radius: 2px; padding: 10px 20px; font-weight: 400">
    {{$content}}
</p>

@include('includes.emails.footer')