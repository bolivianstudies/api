# AEB Backend-API

The backend API is based on [Laravel 5.2](laravel.com/docs/5.2) framework and [Dingo API](https://github.com/dingo/api) extension library.

## Requirements

* Apache 2.4>=
* PHP 5.6>=
* MySQL 5>=
* Composer

## Installation

To install the application locally follow these steps:

### Apache, PHP, MySQL

#### Linux (Ubuntu 14)

To install **Apache, PHP and MySQL** on Linux go to [this tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04) and follow the instructions. 
In summary, you should enter the following commands in the shell:
 
```
sudo apt-get update
sudo apt-get install apache2
sudo apt-get install curl
sudo apt-get install mysql-server
sudo mysql_secure_installation
sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
sudo service apache2 restart
```

#### Mac OSX (10.11)

On Mac OSX, Apache and PHP comes installed by default. To install MySQL [download it](https://dev.mysql.com/downloads/mysql/) and install it.
Alternately, you can use [homebrew](http://brew.sh/) (a dependency manager for OSX)
 
#### Windows (7,8,10)

It's highly recommended that you [download WAMP](https://sourceforge.net/projects/wampserver/files/WampServer%203/WampServer%203.0.0/wampserver3.0.6_x64_apache2.4.23_mysql5.7.14_php5.6.25-7.0.10.exe/download) and install it. Once installed you'll need to run wamp server and then add PHP into your **environment variables.** To do so follow [this tutorial](https://john-dugan.com/add-php-windows-path-variable/) which already comes with PHP and MySQL.

## Create virtual hosts

To test the app you'll need to create two virtual hosts: one for the frontend and one for the backend.  
You need to name the frontend virtual host: **http://butaca/** and the backend's **http://api.butaca/**

The frontend's virtual host must target the frontend **root directory.**  
The backend's virtual host must target the backend **public/ directory.**

## Create a new database

Once you have installed MySQL, create a new database with the following parameters:
 
**Database name:** butaca  
**User:** butaca   
**Password:** jJk1WdJ6mvGDo

## Composer

To install composer follow the [instructions on this link](https://getcomposer.org/doc/00-intro.md)

## Dependencies

The [composer.json](composer.json) dependencies file holds everything we need to run the backend API REST server.  
To install dependencies run the following command:

```
composer install
```

Once the installation has ended you can now start migrating and seeding the database with the following command:

```
php artisan migrate --seed
```

## Environment

To set the environment you'll need to create a file named `.env` with the contents `local` or `production` in it, depending on the environment settings you want to use [.local.env](.local.env) or [.production.env](.production.env).
 
## API Docs

To view all the API endpoints, methods, config headers, parameters available and responses [click here](api.md)