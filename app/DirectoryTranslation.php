<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirectoryTranslation extends Model
{
    protected $table = 'directories_translations';
    public $timestamps = false;
    protected $fillable = ['label'];
}