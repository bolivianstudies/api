<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class Pricing extends Model
{
    use Translatable;

    public $translationModel = 'App\PricingTranslation';

    public $translatedAttributes = ['membership', 'nationality', 'member', 'description'];

    protected $table = 'pricings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'membership', 'nationality', 'member', 'description', 'price_usd', 'price_bob'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function tags()
    {
        return $this->belongsToMany('App\PricingTag', 'pricing_tag_pricing')->orderBy('label', 'asc');
    }
}
