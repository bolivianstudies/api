<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Sofa\Eloquence\Eloquence;

class User extends Authenticatable
{

    use Eloquence;

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = ['created_at', 'updated_at', 'password', 'remember_token', 'activation_token', 'image_id'];

    protected $searchableColumns = ['name', 'email'];

    function image()
    {
        return $this->belongsTo('App\Image');
    }

    function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role');
    }

    function profile()
    {
        return $this->hasOne('App\Profile');
    }
}
