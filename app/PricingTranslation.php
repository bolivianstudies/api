<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PricingTranslation extends Model
{
    protected $table = 'pricings_translations';
    public $timestamps = false;
    protected $fillable = ['membership', 'nationality', 'member', 'description'];
}