<?php

namespace App\Providers;

use App\Category;
use App\Page;
use App\Role;
use App\Subscriber;
use App\User;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Support\ServiceProvider;
use Mail;

class InterceptorServiceProvider extends ServiceProvider
{

    use Helpers;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(function ($user) {
            $user->roles()->attach(3, ["created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
            $subscriber = new Subscriber;
            $subscriber->name = $user->name;
            $subscriber->email = $user->email;
            $subscriber->active = 1;
            $subscriber->activation_token = md5($user->email);
            if ($subscriber->save()) {
                /*Mail::send('subscriber.emails.subscribe', array(
                    'name' => $subscriber->name,
                    'email' => $subscriber->email,
                    'token' => $subscriber->activation_token,
                ), function ($message) use ($subscriber) {
                    $message->to($subscriber->email, $subscriber->name);
                    $message->subject(trans('reminders.subscribe'));
                });*/
            }
        });
        Category::creating(function ($category) {
            $category->slug = SlugService::createSlug(Category::class, 'slug', $category->title);
        });
        Role::creating(function ($role) {
            if (!$role->name) {
                $role->name = str_slug($role->title, '-');
            }
        });
        Page::creating(function ($page) {
            if (!$page->lang) {
                $page->lang = config('app.locale');
            }
            if (!$page->slug) {
                $page->slug = SlugService::createSlug(Page::class, 'slug', $page->title);
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
