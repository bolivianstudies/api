<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('unauthorized', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.unauthorized');
            }

            $response = array(
                'message' => $message,
                'status' => 401,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('expired', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.expired');
            }

            $response = array(
                'message' => $message,
                'status' => 419,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('forbidden', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.forbidden');
            }

            $response = array(
                'message' => $message,
                'status' => 403,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('badRequest', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.badRequest');
            }

            $response = array(
                'message' => $message,
                'status' => 400,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('success', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.success');
            }

            $response = array(
                'message' => $message,
                'status' => 200,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('created', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.created');
            }

            $response = array(
                'message' => $message,
                'status' => 201,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('error', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.error');
            }

            $response = array(
                'message' => $message,
                'status' => 500,
            );

            return Response::json($response, $response['status']);
        });

        Response::macro('notFound', function ($message = null) {

            if ($message === null) {
                $message = trans('reasons.notFound');
            }

            $response = array(
                'message' => $message,
                'status' => 404,
            );

            return Response::json($response, $response['status']);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
