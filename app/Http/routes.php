<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$params = [
    'version' => 'v1',
    'domain' => env('APP_DOMAIN')
];

$api->group($params, function ($api) {

    // Auth
    $api->post('auth/login', 'App\Api\v1\Controllers\AuthController@login');
    $api->post('auth/register', 'App\Api\v1\Controllers\AuthController@register');
    $api->post('auth/activate', 'App\Api\v1\Controllers\AuthController@activate');
    $api->post('auth/remind', 'App\Api\v1\Controllers\AuthController@remind');
    $api->post('auth/reset', 'App\Api\v1\Controllers\AuthController@reset');
    $api->post('auth/social', 'App\Api\v1\Controllers\AuthController@social');
    $api->get('auth/token', 'App\Api\v1\Controllers\AuthController@token');

    // Regions
    $api->get('regions', 'App\Api\v1\Controllers\RegionController@index');

    // Countries
    $api->get('countries', 'App\Api\v1\Controllers\CountryController@index');

    // Pages
    $api->get('pages', 'App\Api\v1\Controllers\PageController@index');
    $api->get('page/{slug}', 'App\Api\v1\Controllers\PageController@show');

    // Events
    $api->get('events', 'App\Api\v1\Controllers\EventController@index');
    $api->get('event/{id}', 'App\Api\v1\Controllers\EventController@show');

    // Posts
    $api->get('posts', 'App\Api\v1\Controllers\PostController@index');
    $api->get('post/{slug}', 'App\Api\v1\Controllers\PostController@show');

    // Contents
    $api->get('contents/{slug}', 'App\Api\v1\Controllers\ContentController@show');

    // Pricing
    $api->get('pricing', 'App\Api\v1\Controllers\PricingController@index');

    // Timeline
    $api->get('timeline', 'App\Api\v1\Controllers\TimelineController@index');

    // Directories
    $api->get('directories', 'App\Api\v1\Controllers\DirectoryController@index');

    // Directors
    $api->get('directors', 'App\Api\v1\Controllers\DirectorController@index');

    // Faqs
    $api->get('faqs', 'App\Api\v1\Controllers\FaqController@index');
    $api->get('faq/{id}', 'App\Api\v1\Controllers\FaqController@show');

    // Magazines
    $api->get('magazines', 'App\Api\v1\Controllers\MagazineController@index');
    $api->get('magazine/{id}', 'App\Api\v1\Controllers\MagazineController@show');

    // Newsletters
    $api->get('newsletters', 'App\Api\v1\Controllers\NewsletterController@index');

    // Publications
    $api->get('publications', 'App\Api\v1\Controllers\PublicationController@index');

    // Profiles
    $api->get('profiles', 'App\Api\v1\Controllers\ProfileController@index');

    // Subscribers
    $api->post('subscriber', 'App\Api\v1\Controllers\SubscriberController@store');
    $api->get('subscriber/{token}', 'App\Api\v1\Controllers\SubscriberController@show');
    $api->put('subscriber/{token}', 'App\Api\v1\Controllers\SubscriberController@update');

    // Messages
    $api->post('messages', 'App\Api\v1\Controllers\MessageController@store');

    // Registrations
    $api->post('registrations', 'App\Api\v1\Controllers\RegistrationController@enroll');

    // PayPal
    $api->post('paypal/token', ['as' => 'paypal.token', 'uses' => 'App\Api\v1\Controllers\PayPalController@order']);
    $api->get('paypal/done', ['as' => 'paypal.done', 'uses' => 'App\Api\v1\Controllers\PayPalController@done']);
    $api->get('paypal/cancel', ['as' => 'paypal.cancel', 'uses' => 'App\Api\v1\Controllers\PayPalController@cancel']);
    $api->get('paypal/congress', ['as' => 'paypal.congress', 'uses' => 'App\Api\v1\Controllers\PayPalController@congress']);

    $api->group(['middleware' => ['api.auth', 'jwt.refresh']], function ($api) {

        // Auth
        $api->get('auth/me', 'App\Api\v1\Controllers\AuthController@show');
        $api->put('auth/me', 'App\Api\v1\Controllers\AuthController@update');
        $api->post('auth/picture', 'App\Api\v1\Controllers\AuthController@picture');
        $api->put('auth/password', 'App\Api\v1\Controllers\AuthController@password');

        // Super Admin
        $api->group(['middleware' => 'role:super'], function ($api) {

            // Users
            $api->resource('users', 'App\Api\v1\Controllers\UserController', array('except' => array('edit', 'create')));
            $api->post('users/{user}/activation', 'App\Api\v1\Controllers\UserController@activation');
            $api->put('users/{user}', 'App\Api\v1\Controllers\UserController@update');
            $api->get('users/{user}', 'App\Api\v1\Controllers\UserController@show');
            $api->put('users/{user}/partial', 'App\Api\v1\Controllers\UserController@updatePartial');

            // User Roles
            $api->post('users/{user}/roles', 'App\Api\v1\Controllers\UserRoleController@store');
            $api->delete('users/{user}/roles/{role}', 'App\Api\v1\Controllers\UserRoleController@destroy');

            // Roles
            $api->get('roles', 'App\Api\v1\Controllers\RoleController@index');

            // Profiles
            $api->post('profiles', 'App\Api\v1\Controllers\ProfileController@create');
            $api->put('profiles/{id}', 'App\Api\v1\Controllers\ProfileController@updateAny');
            $api->put('profiles/{id}/content', 'App\Api\v1\Controllers\ProfileController@updateContentAny');
            //$api->delete('profiles/{id}', 'App\Api\v1\Controllers\ProfileController@destroy');

            // Pages
            $api->put('pages/{id}/content', 'App\Api\v1\Controllers\PageController@updateContent');

            // Events
            $api->put('events/{id}/content', 'App\Api\v1\Controllers\EventController@updateContent');

            // Orders
            $api->get('orders', 'App\Api\v1\Controllers\OrderController@index');

            $api->get('paypal/detail', ['as' => 'paypal.detail', 'uses' => 'App\Api\v1\Controllers\PayPalController@detail']);
        });

        // Admin
        $api->group(['middleware' => 'role:admin'], function ($api) {

        });

        // Member
        $api->group(['middleware' => 'role:member'], function ($api) {

            // Profiles
            $api->put('me/profile', 'App\Api\v1\Controllers\ProfileController@updateMe');
            $api->put('me/profile/content', 'App\Api\v1\Controllers\ProfileController@updateContentMe');
            $api->get('profiles/{id}', 'App\Api\v1\Controllers\ProfileController@show');

            // Posts
            $api->get('me/posts', 'App\Api\v1\Controllers\PostController@indexMe');
            $api->get('me/posts/{slug}', 'App\Api\v1\Controllers\PostController@showMe');
            $api->post('me/posts', 'App\Api\v1\Controllers\PostController@store');
            $api->put('me/posts/{id}', 'App\Api\v1\Controllers\PostController@update');
            $api->delete('me/posts/{id}', 'App\Api\v1\Controllers\PostController@destroy');
        });

    });
});