<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        $whiteList = array(
            'http://bolivianstudies',
            'http://bolivianstudies:3000',
            'https://bolivianstudies.org',
            'https://www.bolivianstudies.org'
        );

        $origin = $request->server('HTTP_ORIGIN');

        $headers = [
            'Access-Control-Expose-Headers' => 'Accept-Language, Accept, Content-Type, Authorization, Origin',
            'Access-Control-Allow-Headers' => 'Accept-Language, Accept, Content-Type, Authorization',
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
        ];

        if (in_array($origin, $whiteList)) {
            $headers['Access-Control-Allow-Origin'] = $origin;
        } else {
            $headers['Access-Control-Allow-Origin'] = null;
        }

        if ($request->method() == 'OPTIONS') {
            response()->make('Ok', 200);
        }

        $response = $next($request);

        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;

    }
}
