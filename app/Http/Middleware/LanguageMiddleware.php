<?php

namespace App\Http\Middleware;

use Closure;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $languages = array(
            'en',
            'es'
        );

        $lang = $request->header('Accept-Language');

        if (!empty($lang) && in_array($lang, $languages)) {
            app()->setLocale($lang);
        }

        return $next($request);
    }
}
