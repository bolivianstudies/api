<?php

namespace App\Http\Middleware;

use Closure;
use Dingo\Api\Routing\Helpers;

class RoleMiddleware
{

    use Helpers;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        foreach ($this->auth->user()->roles as $userRole) {
            if ($userRole->name === $role) {
                return $next($request);
            }
        }
        return response()->unauthorized(trans('reasons.permissions'));
    }
}
