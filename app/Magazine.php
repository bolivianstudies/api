<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Magazine extends Model
{
    use Eloquence;

    protected $table = 'magazines';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'title', 'volume', 'number', 'date', 'content', 'image_id'];

    protected $searchableColumns = ['title', 'volume', 'number', 'date', 'content', 'docs.title'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    public function docs()
    {
        return $this->belongsToMany('App\Doc', 'magazine_doc');
    }

}
