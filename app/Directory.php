<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class Directory extends Model
{
    use Translatable {
        toArray as protected translatableToArray;
    }

    public $translationModel = 'App\DirectoryTranslation';

    public $translatedAttributes = ['label'];

    protected $table = 'directories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'year'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function directors()
    {
        return $this->belongsToMany('App\Director', 'director_directory')
            ->withPivot('role', 'position')
            ->orderBy('directors.name', 'ASC');
    }

    public function toArray()
    {
        $attributes = $this::translatableToArray();
        if (isset($attributes['pivot'])) {
            $attributes['director'] = $attributes['pivot'];
            unset($attributes['pivot']);
        }
        return $attributes;
    }
}
