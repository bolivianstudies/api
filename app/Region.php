<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    public $incrementing = false;
    protected $table = 'regions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function countries()
    {
        return $this->hasMany('App\Country', 'region_id');
    }

}
