<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Post extends Model
{
    use Sluggable, SluggableScopeHelpers, Eloquence {
        Eloquence::replicate insteadof Sluggable;
    }

    protected $table = 'posts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'title', 'excerpt', 'content', 'slug', 'user_id', 'image_id', 'url', 'created_at'];

    protected $searchableColumns = ['title', 'excerpt', 'content', 'user.name', 'user.email', 'tags.title'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'post_tag')->orderBy('title', 'asc');
    }

    public function comments()
    {
        return $this->hasMany('App\PostComment')->orderBy('post_comment.created_at', 'desc');
    }

    public function commentsCount()
    {
        return $this->hasMany('App\PostComment')
            ->selectRaw('post_id, count(*) as aggregate')
            ->groupBy('post_id');
    }

    public function getCommentsCountAttribute()
    {
        if (!array_key_exists('commentsCount', $this->relations))
            $this->load('commentsCount');

        $related = $this->getRelation('commentsCount');

        return ($related) ? (int)$related->aggregate : 0;
    }


}
