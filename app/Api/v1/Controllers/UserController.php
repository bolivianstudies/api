<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Mail;
use App\Image;

/**
 * @Resource("Users", uri="/users")
 */
class UserController extends Controller
{
    /**
     * Show all users
     *
     * Get a JSON paginated representation of all the registered users.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"total": "50", "current_page" : "2", "per_page" : "20", "last_page" : "3", "next_page_url": "http://api.butaca.com/users?page=3", "prev_page_url": "http://api.butaca.com/users?page=1", "from": "21", "to": "40", "data": "[users]"})
     * @Parameters({
     *      @Parameter("search", description="A term to compare against user names and emails.", type="string", required=false),
     *      @Parameter("page", description="The page of results to view.", default=1, type="integer", required=false),
     *      @Parameter("limit", description="The amount of results per page.", default=20, type="integer", required=false)
     * })
     */
    public function index(Request $request)
    {
        $users = User::search($request->input('search', ''))
            ->where('id', '!=', $this->auth->user()->id)
            ->with('image', 'roles')
            ->paginate($request->input('limit', 10));
        return response()->json($users, 200);
    }

    /**
     * Register new user
     *
     * Get a JSON object of the recently created resource.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(201, body={"name" : "Alberto Díaz", "email": "alberto.diaz@hotmail.com", "active" : "0", "role" : {"name" : "subscriber"}, "image" : {"id": "213", "path" : "img/users/324897fAGD.jpg"}})
     * @Parameters({
     *      @Parameter("name", description="The new user full name.", type="string", required=true),
     *      @Parameter("email", description="The new user email.", type="email", required=true),
     *      @Parameter("password", description="The new user password 8 chars min, 32 chars max.", type="string", required=true),
     *      @Parameter("confirmPassword", description="The new user password confirmation.", type="string", required=true),
     *      @Parameter("image_id", description="The new user related image id.", type="integer", required=true),
     *      @Parameter("role_id", description="The new user related role id.", type="integer", required=true),
     *      @Parameter("active", description="The activation state of the new user.", type="integer", required=false)
     * })
     */
    public function store(Request $request)
    {
        $validator = app('validator')->make(array(
            'name' => $request->input('name', ''),
            'email' => $request->input('email', ''),
            'password' => $request->input('password', ''),
            'password_confirmation' => $request->input('confirmPassword', ''),
            'active' => $request->input('active', 1),
        ), array(
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'active' => 'boolean',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->active = $request->input('active', 1);
            $user->activation_token = md5($request->input('email'));
            $user->image_id = Image::where('path', 'img/users/default.svg')->first()->id;

            if ($user->save()) {
                return response()->json($user, 200);
            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.user'))));
            }
        }

    }

    /**
     * Show all users
     *
     * Get a JSON paginated representation of all the registered users.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"name" : "Alberto Díaz", "email": "alberto.diaz@hotmail.com", "active" : "0", "role" : {"name" : "subscriber"}, "image" : {"id": "213", "path" : "img/users/324897fAGD.jpg"}})
     */
    public function show($id)
    {
        $user = User::where('id', $id)->with('image', 'roles', 'profile')->first();
        return response()->json($user, 200);
    }

    /**
     * Activate a specific user
     *
     * Get the response message of the request.
     *
     * @Post("/{id}/activation")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"message" : "The activation email was sent!"})
     */
    public function activation($id)
    {
        $user = User::find($id);

        Mail::send('auth.emails.register',
            array(
                'name' => $user->name,
                'email' => $user->email,
                'token' => $user->activation_token,
            ),
            function ($message) use ($user) {
                $message
                    ->to($user->email, $user->name)
                    ->subject(trans('reminders.register'));
            });

        return response()->success(trans('reasons.sent', array('email' => $user->email)));
    }

    /**
     * Update specific user
     *
     * Get the response message of the request.
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"message" : "The user was updated"})
     * @Parameters({
     *      @Parameter("active", description="The activation state of the user.", type="integer", required=false),
     *      @Parameter("role_id", description="The related role id of the user.", type="integer", required=false)
     * })
     */
    public function update($id, Request $request)
    {
        $user = User::find($id);

        if ($request->input('active') !== 1)
        {
            foreach ($user->roles as $role) {
                if ($role->name === 'super') {
                    return response()->forbidden();
                }
            }
        }

        $validator = app('validator')->make(array(
            'active' => $request->input('active', $user->active),
        ), array(
            'active' => 'required|boolean',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }
        
        $user->active = $request->input('active', $user->active);

        if (!$user->save()) {
            return response()->error(trans('messages.notUpdated', array('model' => trans('models.user'))));
        }
        
        return response()->success(trans('messages.updated', array('model' => trans('models.user'))));
    }

    public function updatePartial($id, Request $request)
    {
        $user = User::find($id);

        if ($this->auth->user()->id !== $id) {
            foreach ($user->roles as $role) {
                if ($role->name === 'super') {
                    return response()->forbidden();
                }
            }
        }

        if ($user->email !== $request->input('email', '')) {
            $validator = app('validator')->make(array(
                'email' => $request->input('email', ''),
            ), array(
                'email' => 'required|email|unique:users',
            ));
            if ($validator->fails()) {
                return response()->badRequest($validator->messages());
            }
        }
        $validator = app('validator')->make(array(
            'name' => $request->input('name'),
        ), array(
            'name' => 'required|min:3',
        ));
        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if (!$user->save()) {
            return response()->error(trans('messages.notUpdated', array('model' => trans('models.user'))));
        }
        
        return response()->success(trans('messages.updated', array('model' => trans('models.user'))));
    }

    /**
     * Delete specific user
     *
     * Get a success or error response.
     *
     * @Delete("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200)
     */
    public function destroy($id)
    {
        if ($this->auth->user()->id === $id) {
            return response()->forbidden();
        } else {
            $user = User::find($id);

            foreach ($user->roles as $role) {
                if ($role->name === 'super') {
                    return response()->forbidden();
                }
            }

            if ($user->delete()) {
                return response()->success(trans('messages.deleted', array('model' => trans('models.user'))));
            } else {
                return response()->error();
            }

        }
    }
}