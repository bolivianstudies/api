<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Newsletter;
use Illuminate\Http\Request;

/**
 * @Resource("Newsletters", uri="/newsletters")
 */
class NewsletterController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all paginated newsletters.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[newsletters]"})
     */
    public function index(Request $request)
    {
        $newsletters = Newsletter::where('lang', config('app.locale'))->search($request->input('search', ''))->orderBy('date', 'desc')->with('image', 'doc')->paginate($request->input('limit', 5));
        return response()->json($newsletters, 200);
    }
}
