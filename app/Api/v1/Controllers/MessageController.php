<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;
use Mail;

/**
 * @Resource("Messages", uri="/messages")
 */
class MessageController extends Controller
{

    /**
     * Store new message
     *
     * Get a confirmation of the recently stored and sent message.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message": "Your message has been sent"})
     * @Parameters({
     *      @Parameter("name", description="The sender name.", type="string", required=true),
     *      @Parameter("email", description="The sender email address.", type="email", required=true),
     *      @Parameter("message", description="The sender message content.", type="text", required=true),
     *      @Parameter("institution", description="The sender institution.", type="text", required=false),
     *      @Parameter("phone", description="The sender phone number.", type="text", required=false)
     * })
     */
    public function store(Request $request)
    {
        $validator = app('validator')->make(array(
            'name' => $request->input('name', ''),
            'email' => $request->input('email', ''),
            'message' => $request->input('message', ''),
        ), array(
            'name' => 'required|min:3',
            'email' => 'required|email',
            'message' => 'required|min:10',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $message = new Message;
            $message->name = $request->input('name', '');
            $message->email = $request->input('email', '');
            $message->message = $request->input('message', '');

            if ($message->save()) {

                Mail::send('message.emails.message', array(
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'phone' => $request->input('phone'),
                    'institution' => $request->input('institution'),
                    'content' => $request->input('message'),
                ), function ($message) {
                    $message->to(config('mail.recipient.address'), config('mail.recipient.name'));
                    $message->subject(trans('emails.contact'));
                });

                return response()->success(trans('reasons.sent', array('email' => config('mail.from.address'))));

            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.message'))));
            }
        }

    }

}
