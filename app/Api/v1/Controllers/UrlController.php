<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @Resource("Url", uri="/url")
 */
class UrlController extends Controller
{

    /**
     * Shorten URL through google URL Shortener service
     *
     * Get the stored short url
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"filename": "DG3242RA6G45SJF97.jpg", "originalName" : "an-uploaded-file.jpg", "dir" : "uploads", "extension" : ".jpg", "size": "1024", "mime": "image/jpeg"})
     * @Parameters({
     *      @Parameter("url", description="A URL.", type="url", required=true)
     * })
     */
    public function index(Request $request)
    {
        if ($request->has('url')) {

            $url = $request->input('url');

            if (!empty($url)) {
                $apiKey = env('GOOGLE_API_KEY', '');
                $curl = array(
                    'longUrl' => $url,
                    'key' => $apiKey,
                );
                $data_string = json_encode($curl);
                $ch = curl_init('https://www.googleapis.com/urlshortener/v1/url?key=' . $apiKey);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );

                $shortUrl = curl_exec($ch);
                curl_close($ch);

                return response()->make($shortUrl, 200);
            } else {
                return response()->error();
            }
        } else {
            return response()->badRequest(trans('reasons.notUrlData'));
        }
    }

}
