<?php

namespace App\Api\v1\Controllers;

use App\Faq;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @Resource("Faqs", uri="/faqs")
 */
class FaqController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all paginated faqs.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[faqs]"})
     */
    public function index(Request $request)
    {
        $faqs = Faq::where('lang', config('app.locale'))->search($request->input('search', ''))->paginate($request->input('limit', 10));
        return response()->json($faqs, 200);
    }

    /**
     * Show a single faq
     *
     * Get a JSON object of a single faq.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id": "1", "lang": "es", "question" : "This is a question example", "answer" : "This is the answer example.", "created_at" : "2061-05-19 23:47:32", "updated_at" : "2061-05-19 23:47:32" })
     */

    public function show($id)
    {
        $faq = Faq::find($id);
        return response()->json($faq, 200);
    }
}
