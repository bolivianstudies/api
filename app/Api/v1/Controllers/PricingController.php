<?php

namespace App\Api\v1\Controllers;

use App\Event;
use App\Http\Controllers\Controller;
use App\Page;
use App\Pricing;
use App\PricingTag;
use Illuminate\Http\Request;

/**
 * @Resource("Pricing", uri="/pricing")
 */
class PricingController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all paginated events.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"total": "50", "current_page" : "2", "per_page" : "20", "last_page" : "3", "next_page_url": "http://api.pasf.com/users?page=3", "prev_page_url": "http://api.pasf.com/users?page=1", "from": "21", "to": "40", "data": "[posts]"})
     * @Parameters({
     *      @Parameter("page", description="The page slug related to pricing table.", type="string", required=false),
     *      @Parameter("event", description="The event id related to the pricing table.", type="integer", required=false)
     * })
     */
    public function index(Request $request)
    {
        $pricing = array();
        if ($request->input('page')) {
            $page = Page::where('slug', $request->input('page'))->first();
            if ($page) {
                $pricing = $page->pricing;
            }
        } elseif ($request->input('event')) {
            $event = Event::find($request->input('event'));
            if ($event) {
                $pricing = $event->pricing;
            }
        } elseif ($request->input('tag')) {
            $tag = PricingTag::where('label', $request->input('tag'))->first();
            if ($tag) {
                $pricing = $tag->pricing;
            }
        } else {
            $pricing = Pricing::orderBy('created_at', 'desc')->get();
        }
        return response()->json($pricing, 200);
    }
}
