<?php

namespace App\Api\v1\Controllers;

use Carbon\Carbon;
use App\Currency;
use App\Fee;
use App\Http\Controllers\Controller;
use App\Order;
use App\Payment;
use App\PaymentMethod;
use App\Tax;
use App\PasswordReset;
use Illuminate\Http\Request;
use PayPal;
use Password;
use PayPal\Api\FlowConfig;
use PayPal\Api\Presentation;
use PayPal\Api\InputFields;
use PayPal\Api\WebProfile;
use PayPal\Exception\PayPalConnectionException;

/**
 * @Resource("PayPal", uri="/paypal")
 */
class PayPalController extends Controller
{
    private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = PayPal::ApiContext(
            env('PAYPAL_CLIENT_ID'),
            env('PAYPAL_SECRET'));

        $this->_apiContext->setConfig(array(
            'mode' => env('PAYPAL_MODE'),
            'service.EndPoint' => env('PAYPAL_SERVICE_ENDPOINT'),
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE',
        ));

    }

    /**
     * Get Token
     *
     * Get the PayPal formed url to start the pop-up window with the correct token.
     *
     * @Get("/token")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"url": "https://www.butaca.com/resumen?token=343ASGFDG298f789ADSA-FAS344RF"})
     */

    public function order(Request $request)
    {
        $validator = app('validator')->make([
            'order_id' => $request->input('id', '')
        ], [
            'order_id' => 'required|exists:orders,id'
        ]);

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        $order = Order::where('id', $request->input('id'))
            ->with('pricing.tags', 'user', 'country')
            ->first();

        $tag = $order->pricing->tags[0];

        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');

        $item = PayPal::Item();
        $item->setName($order->pricing->description)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setSku($order->id)
            ->setPrice($order->pricing->price_usd);

        $itemList = PayPal::ItemList();
            $itemList->setItems(array($item));

        $details = PayPal::Details();
        $details->setSubtotal($order->pricing->price_usd);

        $amount = PayPal::Amount();
        $amount
            ->setCurrency('USD')
            ->setTotal($order->pricing->price_usd);

        $transaction = PayPal::Transaction();
        $transaction
            ->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription($order->pricing->description);

        $redirectUrls = PayPal::RedirectUrls();

        switch ($tag->label) {
            case 'membership':
                //$redirectUrls->setReturnUrl(config('app.url').'bienvenida');
                $redirectUrls->setReturnUrl(route('paypal.done'));
                break;
            case 'congress':
                $redirectUrls->setReturnUrl(route('paypal.congress'));
                break;
            default:
                $redirectUrls->setReturnUrl(config('app.url'));
                break;
        }

        $redirectUrls->setCancelUrl(config('app.url'));

        $payment = PayPal::Payment();
        $payment
            ->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $webProfile = null;

        $logoUrl = config('app.url')."img/assets/logo-paypal-header.png";

        $flowConfig = new FlowConfig();
        $flowConfig->setLandingPageType("Billing");
        $flowConfig->setBankTxnPendingUrl(config('app.url'));
        $presentation = new Presentation();

        if (config('app.locale') === 'es') {
            $presentation->setLogoImage($logoUrl)
                ->setBrandName("Asociación de Estudios Bolivianos")
                ->setLocaleCode("es_ES");
        } else {
            $presentation->setLogoImage($logoUrl)
                ->setBrandName("Bolivian Studies Asociation")
                ->setLocaleCode("en_US");
        }

        $inputFields = new InputFields();
        $inputFields->setAllowNote(true)
            ->setNoShipping(1)
            ->setAddressOverride(0);
        $webProfile = new WebProfile();
        $webProfile->setName("Bolivian Studies Membership" . uniqid())
            ->setFlowConfig($flowConfig)
            ->setPresentation($presentation)
            ->setInputFields($inputFields);
        $request = clone $webProfile;

        try {
            $createProfileResponse = $webProfile->create($this->_apiContext);
            $payment->setExperienceProfileId($createProfileResponse->getId());

            $response = $payment->create($this->_apiContext);

            if ($response) {
                $order->payment_description = $response->getId();
                $order->save();
                return response()->json(array('url' => $response->getApprovalLink()), 200);
            } else {
                return response()->badRequest(trans('reasons.paypalTokenFailed'));
            }
        } catch (PayPalConnectionException $ex) {
            $error = $ex->getData();
            return response()->json($error, 400);
        }
    }

    public function congress(Request $request) {
        $validator = app('validator')->make(array(
            'paymentId' => $request->input('paymentId'),
            'token' => $request->input('token'),
            'PayerID' => $request->input('PayerID'),
        ), array(
            'paymentId' => 'required',
            'token' => 'required',
            'PayerID' => 'required',
        ));

        $paymentId = $request->input('paymentId');

        $order = Order::where('payment_description', $paymentId)->first();

        if (!$order) {
            return response()->error('order not exist');
        }

        if (!empty($order->verified_at)) {
            return response()->error('order was already processed');
        }

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        try {
            $payment = \PayPal\Api\Payment::get($paymentId, $this->_apiContext);
            $execution = new \PayPal\Api\PaymentExecution();
            $execution->setPayerId($request->input('PayerID'));
            $transaction = new \PayPal\Api\Transaction();
            $amount = new \PayPal\Api\Amount();
            $details = new \PayPal\Api\Details();
            $details->setSubtotal($order->pricing->price_usd);
            $amount->setCurrency('USD');
            $amount->setTotal($order->pricing->price_usd);
            $amount->setDetails($details);
            $transaction->setAmount($amount);
            $execution->addTransaction($transaction);
            $result = $payment->execute($execution, $this->_apiContext);
            $payment = \PayPal\Api\Payment::get($paymentId, $this->_apiContext);

            if ($payment->state === 'approved') {

                $order->verified_at = Carbon::now();
                $order->save();

                return response()->redirectTo(config('app.url').'evento/congreso');
            } else {
                return response()->json(['message' => 'Not Approved'], 400);
            }
        } catch (\Exception $ex) {
            return response()->json(['message' => $ex->getMessage()], 400);
        }
    }

    public function done(Request $request) {
        $validator = app('validator')->make(array(
            'paymentId' => $request->input('paymentId'),
            'token' => $request->input('token'),
            'PayerID' => $request->input('PayerID'),
        ), array(
            'paymentId' => 'required',
            'token' => 'required',
            'PayerID' => 'required',
        ));

        $paymentId = $request->input('paymentId');

        $order = Order::where('payment_description', $paymentId)->first();

        if (!$order) {
            return response()->error('order not exist');
        }

        if (!empty($order->verified_at)) {
            return response()->error('order was already processed');
        }

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        try {
            $payment = \PayPal\Api\Payment::get($paymentId, $this->_apiContext);
            $execution = new \PayPal\Api\PaymentExecution();
            $execution->setPayerId($request->input('PayerID'));
            $transaction = new \PayPal\Api\Transaction();
            $amount = new \PayPal\Api\Amount();
            $details = new \PayPal\Api\Details();
            $details->setSubtotal($order->pricing->price_usd);
            $amount->setCurrency('USD');
            $amount->setTotal($order->pricing->price_usd);
            $amount->setDetails($details);
            $transaction->setAmount($amount);
            $execution->addTransaction($transaction);

            $result = $payment->execute($execution, $this->_apiContext);

            $payment = \PayPal\Api\Payment::get($paymentId, $this->_apiContext);

            if ($payment->state === 'approved') {

                $order->verified_at = Carbon::now();
                $order->save();
                $order->user->active = 1;
                $order->user->save();

                $user = $order->user;

                view()->composer('auth.emails.password', function ($view) use ($user) {
                    $view->with([
                        'name' => $user->name,
                        'email' => $user->email,
                        'token' => PasswordReset::where('email', $user->email)->first()->token,
                    ]);
                });

                switch ($response = Password::sendResetLink(array('email' => $user->email), function ($message) {
                    $message->subject(trans('reminders.remind'));
                })) {
                    case Password::INVALID_USER:
                        return response()->badRequest(trans('reminders.user'));
                    case Password::RESET_LINK_SENT:
                        return response()->redirectTo(config('app.url').'bienvenida?email='.$user->email);
                        //return response()->success(trans('reminders.sent'));
                    default :
                        return response()->error();
                }
            } else {
                 return response()->json(array('message' => 'Not Approved'), 400);
            }

        } catch (\Exception $ex) {
            return response()->json(array('message' => $ex->getMessage()), 400);
        }
    }

    public function cancel(Request $request) {
        // paymentId is not returned when user cancels payment
        // https://github.com/paypal/PayPal-Ruby-SDK/issues/119
        // https://github.com/paypal/PayPal-REST-API-issues/issues/29
        return response()->json($request->toArray(), 200);
    }

    /**
     * Make Payment
     *
     * Process the PayPal payment with the url callback parameters provided
     *
     * @Post("/payment")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message": "The payment has been successfully completed"})
     * @Parameters({
     *      @Parameter("token", description="The generated order token.", type="string", required=true),
     *      @Parameter("paymentId", description="The payment unique id.", type="string", required=true),
     *      @Parameter("PayerID", description="The PayPal payer user ID.", type="string", required=true)
     * })
     */


    public function payment(Request $request)
    {
        $validator = app('validator')->make(array(
            'order_id' => $request->input('id', ''),
            'payment_token' => $request->input('token', ''),
            'payer_id' => $request->input('PayerID', ''),
            'payment_id' => $request->input('paymentId', ''),
        ), array(
            'order_id' => 'required|exists:orders,id',
            'payment_token' => 'required',
            'payer_id' => 'required',
            'payment_id' => 'required',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        $id = $request->input('paymentId');
        $payer_id = $request->input('PayerID');

        $payment = PayPal::getById($id, $this->_apiContext);
        $paymentExecution = PayPal::PaymentExecution();
        $paymentExecution->setPayerId($payer_id);

        try {
            $executePayment = $payment->execute($paymentExecution, $this->_apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            $error = $ex->getData();
            return response()->json($error, 400);
        }
        if ($executePayment) {
            $payment = new Payment;
            $payment->id = $request->input('paymentId');
            $payment->order_id = $request->input('id');
            $payment->payment_method_id = PaymentMethod::where('code', '=', 'paypal')->first()->id;
            if ($payment->save()) {
                return response()->json($payment, 201);
            } else {
                return response()->error();
            }
        } else {
            return response()->error(trans('reasons.payPalPaymentFailed'));
        }
    }

    public function detail(Request $request) {
        $validator = app('validator')->make([
            'payment_id' => $request->input('payment_id')
        ], [
            'payment_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        $payment = PayPal::getById($request->input('payment_id'), $this->_apiContext);

        $transactions = $payment->getTransactions();
        $relatedResources = $transactions[0]->getRelatedResources();
        $sale = $relatedResources[0]->getSale();
        $saleId = $sale->getId();

        $transactionUrl = $this->_apiContext->getConfig()['mode'] === 'sandbox'
            ? 'https://sandbox.paypal.com/cgi-bin/webscr?cmd=_view-a-trans&id='.$saleId
            : 'https://www.paypal.com/cgi-bin/webscr?cmd=_view-a-trans&id='.$saleId;

        return response()->json(["url" => $transactionUrl], 200);
    }
}