<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Role;

/**
 * @Resource("Roles", uri="/roles")
 */
class RoleController extends Controller
{

    /**
     * Show all user roles
     *
     * Get a JSON array of all the user roles.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body="[roles]")
     */
    public function index()
    {
        $roles = Role::all();
        return response()->json($roles, 200);
    }

}
