<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Post;
use App\Tag;
use App\Image;
use Illuminate\Http\Request;

/**
 * @Resource("Posts", uri="/posts")
 */
class PostController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all, category or tag paginated posts.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"total": "50", "current_page" : "2", "per_page" : "20", "last_page" : "3", "next_page_url": "http://api.pasf.com/users?page=3", "prev_page_url": "http://api.pasf.com/users?page=1", "from": "21", "to": "40", "data": "[posts]"})
     * @Parameters({
     *      @Parameter("tag", description="A tag slug.", type="string", required=false),
     *      @Parameter("page", description="The page number.", type="integer", required=false),
     *      @Parameter("limit", description="The number of results per page.", type="integer", required=false),
     *      @Parameter("search", description="A search term.", type="sting", required=false),
     *      @Parameter("ignore", description="A post id to ignore.", type="integer", required=false)
     * })
     */
    public function index(Request $request)
    {
        $fields = ['id', 'lang', 'title', 'image_id', 'user_id', 'excerpt', 'slug', 'created_at'];
        $posts = Post::select($fields)->where('lang', config('app.locale'));

        if ($request->input('tag')) {
            $tag = Tag::where('slug', $request->input('tag'))->first();
            if ($tag) {
                $posts = $tag->posts;
            }
        }

        if ($request->input('ignore')) {
            $posts = $posts->where('posts.id', '!=', $request->input('ignore'));
        }

        $posts = $posts->search($request->input('search', ''))->orderBy('created_at', 'desc')->with('image', 'user', 'tags', 'commentsCount')->paginate($request->input('limit', 10));

        return response()->json($posts, 200);
    }

    public function indexMe(Request $request)
    {
        $user = $this->auth->user();
        $fields = ['id', 'lang', 'title', 'image_id', 'user_id', 'excerpt', 'slug', 'created_at'];
        $posts = Post::select($fields)
            ->where('lang', config('app.locale'))
            ->where('user_id', $user->id);

        if ($request->input('tag')) {
            $tag = Tag::where('slug', $request->input('tag'))->first();
            if ($tag) {
                $posts = $tag->posts;
            }
        }

        if ($request->input('ignore')) {
            $posts = $posts->where('posts.id', '!=', $request->input('ignore'));
        }

        $posts = $posts->search($request->input('search', ''))->orderBy('created_at', 'desc')->with('image', 'user', 'tags', 'commentsCount')->paginate($request->input('limit', 10));

        return response()->json($posts, 200);
    }

    /**
     * Show a single post
     *
     * Get a JSON object of a single post with its category and image.
     *
     * @Get("/{post}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id": "1", "title" : "This is a title example", "excerpt" : "This is the post excerpt...", "content" : "<p>This is a large post content...</p>", "category" : {"id" : "3", "title" : "Notas de prensa"}, "comments_count" : {"aggregate" : "12"}, "created_at" : "2061-05-19 23:47:32" })
     */

    public function show($postSlug)
    {
        $post = Post::where('lang', config('app.locale'))
            ->where('slug', $postSlug)
            ->with('image', 'user', 'comments')
            ->first();

        if (!$post) {
            return response()->notFound();
        }

        return response()->json($post, 200);
    }

    public function showMe($postSlug)
    {
        $user = $this->auth->user();
        $post = Post::where('lang', config('app.locale'))
            ->where('slug', $postSlug)
            ->where('user_id', $user->id)
            ->with('image', 'user', 'comments')
            ->first();

        if (!$post) {
            return response()->notFound();
        }

        return response()->json($post, 200);
    }

    public function store(Request $request)
    {
        $user = $this->auth->user();

        if ($user->id != $request->input('user_id', $user->id)) {
            return response()->forbidden();
        }

        $validator = app('validator')->make(array(
            'lang' => $request->input('lang', config('app.locale')),
            'title' => $request->input('title', ''),
            'excerpt' => $request->input('excerpt', ''),
            'content' => $request->input('content', '')
        ), array(
            'lang' => 'required',
            'title' => 'required',
            'excerpt' => 'required',
            'content' => 'required'
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        $image_id = Image::where('path', 'img/newsletters/default.svg')->first()->id;

        $post = new Post;
        $post->user_id = $user->id;
        $post->lang = $request->input('lang', config('app.locale'));
        $post->title = $request->input('title');
        $post->excerpt = $request->input('excerpt');
        $post->content = $request->input('content');
        $post->image_id = $image_id;

        if (!$post->save()) {
            return response()->error(trans('reasons.notCreated', array('model' => trans('models.post'))));
        }

        return response()->json($post, 200);
    }

    public function update($id, Request $request)
    {
        $user = $this->auth->user();
        $post = Post::find($id);

        if (!$post) {
            return response()->notFound();
        }

        if ($user->id != $post->user_id) {
            return response()->forbidden();
        }

        $validator = app('validator')->make(array(
            'lang' => $request->input('lang', config('app.locale')),
            'title' => $request->input('title', ''),
            'excerpt' => $request->input('excerpt', ''),
            'content' => $request->input('content', '')
        ), array(
            'lang' => 'required',
            'title' => 'required',
            'excerpt' => 'required',
            'content' => 'required'
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        if ($post->title !== $request->input('title')) {
            $post->slug = null;
        }

        $post->title = $request->input('title');
        $post->excerpt = $request->input('excerpt');
        $post->content = $request->input('content');
        $post->image_id = $request->input('image_id', $post->image_id);

        if (!$post->save()) {
            return response()->error(trans('reasons.notUpdated', array('model' => trans('models.post'))));
        }

        return response()->json($post, 200);
    }

    public function destroy($id, Request $request)
    {
        $user = $this->auth->user();
        $post = Post::find($id);

        if (!$post) {
            return response()->notFound();
        }

        if ($user->id != $post->user_id) {
            return response()->forbidden();
        }

        if (!$post->delete()) {
            return response()->error(trans('reasons.notDeleted', array('model' => trans('models.post'))));
        }

        return response()->success(trans('messages.deleted', array('model' => trans('models.post'))));
    }

}
