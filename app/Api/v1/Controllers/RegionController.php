<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Region;

/**
 * @Resource("Regions", uri="/regions")
 */
class RegionController extends Controller
{

    /**
     * Show all regions
     *
     * Get a JSON array of all regions.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[regions]")
     */
    public function index()
    {
        $regions = Region::with('countries')->get();
        return response()->json($regions, 200);
    }

}
