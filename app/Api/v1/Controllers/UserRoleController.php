<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;

/**
 * @Resource("User Roles", uri="/users/{id}/roles")
 */
class UserRoleController extends Controller
{

    /**
     * Register new user role
     *
     * Get a JSON object of the recently created resource.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(201, body={"message" : "The role has been added to the user"})
     * @Parameters({
     *      @Parameter("role_id", description="The new user role id.", type="integer", required=true)
     * })
     */
    public function store($userId, Request $request)
    {

        $validator = app('validator')->make(array(
            'user_id' => $userId,
            'role_id' => $request->input('role_id'),
        ), array(
            'user_id' => 'required|exists:users,id',
            'role_id' => 'required|exists:roles,id',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {

            $user = User::find($userId);

            foreach ($user->roles as $role) {
                if ($role->id === $request->input('role_id')) {
                    return response()->badRequest(trans('reasons.exists', array('model' => trans('models.role'))));
                }
            }

            $userRole = new UserRole;
            $userRole->user_id = $userId;
            $userRole->role_id = $request->input('role_id');

            if ($userRole->save()) {
                return response()->created();
            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.role'))));
            }
        }

    }

    /**
     * Delete user role
     *
     * Get a success or error response.
     *
     * @Delete("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"message" : "The role has been removed from the user"})
     */
    public function destroy($userId, $roleId)
    {

        $validator = app('validator')->make(array(
            'user_id' => $userId,
            'role_id' => $roleId,
        ), array(
            'user_id' => 'required|exists:user_role,user_id',
            'role_id' => 'required|exists:user_role,role_id',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $userRole = UserRole::where('user_id', '=', $userId)->where('role_id', '=', $roleId)->first();
            if ($userRole->delete()) {
                return response()->success(trans('messages.deleted', array('model' => trans('models.role'))));
            } else {
                return response()->error();
            }
        }
    }
}