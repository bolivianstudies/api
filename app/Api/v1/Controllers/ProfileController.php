<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Carbon;
use Purifier;

/**
 * @Resource("Profiles", uri="/profiles")
 */
class ProfileController extends Controller
{

    /**
     * Show all, category or tag profiles
     *
     * Get a JSON array of all paginated profiles.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[profiles]"})
     */
    public function index(Request $request)
    {
        $profiles = Profile::select('*')->whereHas('user', function($query) {
            $query->where('active', 1);
        });

        if ($request->input('letter')) {
            $profiles = $profiles->where('name', 'LIKE', $request->input('letter') . '%');
        }

        $profiles = $profiles
            ->search($request->input('search', ''))
            ->with('user.image', 'country.region')
            ->orderBy($request->input('order', 'name'), $request->input('sort', 'asc'))
            ->paginate($request->input('limit', 10));

        return response()->json($profiles, 200);
    }

    /**
     * Show a single profile
     *
     * Get a JSON object of a single profile with its category and image.
     *
     * @Get("/{profile}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id": "1", "title" : "This is a title example", "excerpt" : "This is the profile excerpt...", "content" : "<p>This is a large profile content...</p>", "category" : {"id" : "3", "title" : "Notas de prensa"}, "comments_count" : {"aggregate" : "12"}, "created_at" : "2061-05-19 23:47:32" })
     */

    public function show($id)
    {
        $profile = Profile::where('id', $id)->with('user.image', 'country', 'education')->first();
        return response()->json($profile, 200);
    }

    public function create(Request $request)
    {
        $validator = app('validator')->make(array(
            'user_id' => $request->input('user_id'),
        ), array(
            'user_id' => 'required|exists:users,id',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        $profile = new Profile;
        $profile->user_id = $request->input('user_id');

        $validator = $this->profileValidator($profile, $request);

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }
        
        $profile = $this->fill($profile, $request);
        if (!$profile->save()) {
            return response()->error(trans('messages.notCreated', array('model' => trans('models.profile'))));
        }
        
        return response()->json($profile, 200);
    }

    /**
     * Updates a single profiles
     *
     * @Put("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * 
     */
    public function updateMe(Request $request)
    {
        $user = $this->auth->user();
        return $this->update($user->profile, $request);
    }

    /**
     * Updates a single profiles
     *
     * @Put("/{profile}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * 
     */
    public function updateAny($id, Request $request)
    {
        $profile = Profile::find($id);
        return $this->update($profile, $request);
    }

    private function update(Profile $profile, Request $request)
    {
        $validator = $this->profileValidator($profile, $request);
        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        $profile = $this->fill($profile, $request);
        if (!$profile->save()) {
            return response()->error(trans('messages.notUpdated', array('model' => trans('models.profile'))));
        }

        return response()->success(trans('messages.updated', array('model' => trans('models.profile'))));
    }

    private function profileValidator(Profile $profile, Request $request)
    {
        $validator = app('validator')->make(array(
            'name' => $request->input('name'),
            'country_id' => $request->input('country_id'),
            'birthday' => $request->input('birthday'),
            'website' => $request->input('website'),
            'facebook' => $request->input('facebook'),
            'google' => $request->input('google'),
        ), array(
            'name' => 'required',
            'country_id' => 'required|exists:countries,id',
            'birthday' => 'date',
            'website' => 'url',
            'facebook' => 'url',
            'google' => 'url',
        ));

        return $validator;
    }

    private function fill(Profile $profile, Request $request)
    {
        $profile->name = $request->input('name');
        $profile->address_personal = $request->input('address_personal');
        $profile->phone = $request->input('phone');
        $profile->country_id = $request->input('country_id');
        $profile->city = $request->input('city');
        $profile->birthday = Carbon::parse($request->input('birthday'));

        $profile->address_institution = $request->input('address_institution');
        $profile->discipline = $request->input('discipline');
        $profile->institution = $request->input('institution');
        $profile->areas = $request->input('areas');
        
        $profile->website = $request->input('website');
        $profile->facebook = $request->input('facebook');
        $profile->google = $request->input('google');
        $profile->skype = $request->input('skype');
        $profile->twitter = $request->input('twitter');

        return $profile;
    } 

    public function destroy($id, Request $request)
    {
        $profile = Profile::find($id);
        if ($profile->delete()) {
            return response()->success(trans('messages.deleted', array('model' => trans('models.profile'))));
        } else {
            return response()->error();
        }
    }

    private function updateContent(Profile $profile, Request $request)
    {
        $profile->content = clean($request->input('content', ''));
        if (!$profile->save()) {
            return response()->error(trans('messages.notUpdated', array('model' => trans('models.profile'))));
        }
        return response()->success(trans('messages.updated', array('model' => trans('models.profile'))));
    }

    public function updateContentAny($id, Request $request)
    {
        $profile = Profile::find($id);
        return $this->updateContent($profile, $request);
    }

    public function updateContentMe(Request $request)
    {
        $user = $this->auth->user();
        $profile = Profile::where('user_id', $user->id)->first();
        return $this->updateContent($profile, $request);
    }
}
