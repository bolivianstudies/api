<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str;

/**
 * @Resource("Uploads", uri="/uploads")
 */
class UploadController extends Controller
{

    /**
     * Upload a file
     *
     * Get the recently uploaded file information.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"filename": "DG3242RA6G45SJF97.jpg", "originalName" : "an-uploaded-file.jpg", "dir" : "uploads", "extension" : ".jpg", "size": "1024", "mime": "image/jpeg"})
     * @Parameters({
     *      @Parameter("file", description="A file.", type="file", required=true),
     *      @Parameter("dir", description="The path to store the uploaded file.", type="string", required=false),
     * })
     */
    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $baseDir = explode('/', base_path());
            unset ($baseDir[count($baseDir) - 1]);
            $dir = implode('/', $baseDir) . '/frontend/' . $request->input('dir', 'uploads');
            $fileInfo = array(
                'filename' => Str::random(32) . '.' . strtolower($file->getClientOriginalExtension()),
                'originalName' => $file->getClientOriginalName(),
                'dir' => $request->input('dir', 'uploads'),
                'extension' => strtolower($file->getClientOriginalExtension()),
                'size' => $file->getSize(),
                'mime' => $file->getMimeType(),
            );

            $validator = app('validator')->make(array(
                'file' => $request->file('file'),
            ), array(
                'file' => 'max:5120',
            ));

            if ($validator->fails()) {
                return response()->badRequest($validator->messages());
            } else {
                if ($file->move($dir, $fileInfo['filename'])) {
                    return response()->json($fileInfo, 200);
                } else {
                    return response()->error(trans('reasons.notUpload', array('model' => trans('models.file'))));
                }
            }
        } else {
            return response()->badRequest(trans('reasons.fileNotUploaded'));
        }
    }

}
