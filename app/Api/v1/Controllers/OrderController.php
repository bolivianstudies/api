<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Order;

class OrderController extends Controller
{
    public function index(Request $request) {

        $from = $request->input('from');
        $from = $from ? Carbon.parse($from) : Carbon::minValue();

        $to = $request->input('to');
        $to = $to ? Carbon.parse($to) : Carbon::now();

        $orders = Order::with('user', 'country', 'pricing');

        if ($request->input('verified')) {
            $orders = $orders->whereNotNull('verified_at');
        }

        $orders = $orders
            ->whereBetween('created_at', [$from, $to])
            ->search($request->input('search', ''))
            ->orderBy('created_at', 'desc')
            ->paginate($request->input('limit', 25));

        return response()->json($orders, 200);
    }
}