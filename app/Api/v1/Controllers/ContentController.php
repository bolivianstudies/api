<?php

namespace App\Api\v1\Controllers;

use App\Content;
use App\Http\Controllers\Controller;

/**
 * @Resource("Contents", uri="/contents")
 */
class ContentController extends Controller
{

    /**
     * Show a single content
     *
     * Get a JSON object of a single content.
     *
     * @Get("/{slug}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id": "1", "title" : "This is a title example", "excerpt" : "This is the post excerpt...", "content" : "<p>This is a large post content...</p>", "category" : {"id" : "3", "title" : "Notas de prensa"}, "comments_count" : {"aggregate" : "12"}, "created_at" : "2061-05-19 23:47:32" })
     */

    public function show($slug)
    {
        $content = Content::where('slug', $slug)->where('lang', config('app.locale'))->first();
        return response()->json($content, 200);
    }
}
