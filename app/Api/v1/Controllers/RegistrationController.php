<?php

namespace App\Api\v1\Controllers;

use App\Profile;
use App\Image;
use App\User;
use App\Order;
use App\Membership;
use App\Subscriber;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @Resource("RegistrationController", uri="/registrations")
 */
class RegistrationController extends Controller
{
    public function enroll(Request $request)
    {
        $user = $this->auth->user();

        $validator = app('validator')->make([
            'billing_name' => $request->input('billing.name', ''),
            'billing_email' => $request->input('billing.email', ''),
            'billing_city' => $request->input('billing.city', ''),
            'billing_country_id' => $request->input('billing.country_id'),
            'billing_address' => $request->input('billing.address', ''),
            'pricing_id' => $request->input('pricing_id'),
        ], [
            'billing_name' => 'required|min:3',
            'billing_email' => 'required|email',
            'billing_city' => 'required',
            'billing_country_id' => 'required|exists:countries,id',
            'billing_address' => 'required',
            'pricing_id' => 'required|exists:pricings,id'
        ]);

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        }

        if (!$user) {
            $validator = app('validator')->make([
                'inscription_name' => $request->input('inscription.name', ''),
                'inscription_city' => $request->input('inscription.city', ''),
                'inscription_areas' => $request->input('inscription.areas', ''),
                'inscription_country_id' => $request->input('inscription.country_id'),
                'inscription_discipline' => $request->input('inscription.discipline', ''),
                'inscription_institution' => $request->input('inscription.institution', ''),
                'inscription_address' => $request->input('inscription.address', ''),
            ], [
                'inscription_name' => 'required|min:3',
                'inscription_city' => 'required',
                'inscription_areas' => 'required',
                'inscription_country_id' => 'required|exists:countries,id',
                'inscription_discipline' => 'required',
                'inscription_institution' => 'required',
                'inscription_address' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->badRequest($validator->messages());
            }

            $userValidator = app('validator')->make([
                'inscription_email' => $request->input('inscription.email', ''),
            ], [
                'inscription_email' => 'required|email|unique:users,email',
            ]);

            $user = null;
            $profile = null;
            $order = null;

            DB::beginTransaction();
            try {

                if ($userValidator->fails()) {
                    $user = User::where('email', $request->input('inscription.email'))->first();
                    if ($user->active !== 0) {

                        DB::rollback();
                        return response()->badRequest($userValidator->messages());
                    } else {
                        $user->delete();
                    }
                }
                $subscriber = Subscriber::where('email', $request->input('inscription.email'))->delete();
                $user = new User;
                $user->name = $request->input('inscription.name');
                $user->email = $request->input('inscription.email');
                $user->password = bcrypt(str_random(10));
                $user->activation_token = sha1($request->input('inscription.email').str_random(10));
                $user->remember_token = str_random(10);
                $user->image_id = Image::where('path', 'img/users/default.svg')->first()->id;
                if (!$user->save()) {
                    DB::rollback();
                    return response()->error(trans('reasons.notCreated', array('model' => trans('models.user'))));
                }
                $profile = new Profile;
                $profile->name = $user->name;
                $profile->user_id = $user->id;
                $profile->city = $request->input('inscription.city');
                $profile->areas = $request->input('inscription.areas');
                $profile->country_id = $request->input('inscription.country_id');
                $profile->discipline = $request->input('inscription.discipline');
                $profile->institution = $request->input('inscription.institution');
                $profile->address_personal = $request->input('inscription.address');
                $profile->phone = $request->input('inscription.phone', '');
                if (!$profile->save()) {
                    DB::rollback();
                    return response()->error(trans('reasons.notCreated', array('model' => trans('models.profile'))));
                }
                $order = new Order;
                $order->user_id = $user->id;
                $order->pricing_id = $request->input('pricing_id');
                $order->country_id = $request->input('billing.country_id');
                $order->name = $request->input('billing.name');
                $order->email = $request->input('billing.email');
                $order->city = $request->input('billing.city');
                $order->address = $request->input('billing.address');
                $order->phone = $request->input('billing.phone', '');
                $order->zip = $request->input('billing.zip', '');
                $order->payment_method_id = 1;
                if (!$order->save()) {
                    DB::rollback();
                    return response()->error(trans('reasons.notCreated', array('model' => trans('models.order'))));
                }
                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.registration'))));
            }

            return response()->json(array('id' => $order->id), 200);
        } else {
            $order = new Order;
            $order->user_id = $user->id;
            $order->pricing_id = $request->input('pricing_id');
            $order->country_id = $request->input('billing.country_id');
            $order->name = $request->input('billing.name');
            $order->email = $request->input('billing.email');
            $order->city = $request->input('billing.city');
            $order->address = $request->input('billing.address');
            $order->phone = $request->input('billing.phone', '');
            $order->zip = $request->input('billing.zip', '');
            $order->payment_method_id = 1;
            if (!$order->save()) {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.order'))));
            }
            return response()->json(array('id' => $order->id), 200);
        }
    }
}
