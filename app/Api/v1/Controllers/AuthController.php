<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\PasswordReset;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Mail;
use Password;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * @Resource("Authentication", uri="/auth")
 */
class AuthController extends Controller
{

    /**
     * Login
     *
     * Get the JWT token for API consumption.
     *
     * @Post("/login")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2JvbGl2aWFuc3R1ZGllczo4MDAwXC9hdXRoXC9sb2dpbiIsImlhdCI6MTQ4MzA1NzI1MCwiZXhwIjoxNDgzMDYwODUwLCJuYmYiOjE0ODMwNTcyNTAsImp0aSI6IjlkNjUzMWM0NjE0MDg0Y2ZlNGE1MmQzZTI2NmY3OTI2In0.Nrmdrsw0fVsEpnKXIALJtnqWABrSdreIGxbrhIkvJyw"})
     * @Parameters({
     *      @Parameter("email", description="The user email.", type="email", required=true),
     *      @Parameter("password", description="The user password.", type="string", required=true)
     * })
     */

    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->badRequest(trans('auth.failed'));
            } else {
                $auth = [
                    "token" => $token,
                    "user" => User::where('email', $request->input('email'))->with(['image', 'roles', 'profile' => function ($query) {
                        $query->select('id', 'user_id');
                    }])->first(),
                ];
                return response()->json($auth, 200);
            }
        } catch (JWTException $e) {
            return response()->error(trans('auth.token'));
        }
    }

    /**
     * Me
     *
     * Get the current user.
     *
     * @Get("/me")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"id" : "1", "name" : "Jorge Flores", "email" : "j.flores@butaca.com", "role" : {"name" : "subscriber"}, "image" : {"id": "213", "path" : "img/users/324897fAGD.jpg", "thumb" : "img/users/thumbs/324897fAGD.jpg", "cover" : "img/users/covers/324897fAGD.jpg"}})
     */

    public function show()
    {
        $user = User::where('id', $this->auth->user()->id)->with('roles', 'image', 'profile.country')->first();
        return response()->json($user, 200);
    }

    /**
     * Register
     *
     * Register as a new user with lowest role privileges by default.
     *
     * @Post("/register")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.starzinfinite.v1+json"})
     * @Response(201, body={"message": "An email was sent to your address for further activation."})
     * @Parameters({
     *      @Parameter("name", description="The new user full name.", type="string", required=true),
     *      @Parameter("email", description="The new user email.", type="email", required=true),
     *      @Parameter("password", description="The new user password 8 chars min, 32 chars max.", type="string", required=true),
     *      @Parameter("confirmPassword", description="The new user password confirmation.", type="string", required=true),
     *      @Parameter("image_id", description="The new user related image id.", type="integer", required=true),
     *      @Parameter("role_id", description="The new user related role id.", type="integer", required=true),
     *      @Parameter("active", description="The activation state of the new user.", type="integer", required=false)
     * })
     */

    public function register(Request $request)
    {

        $validator = app('validator')->make(array(
            'name' => $request->input('name', ''),
            'email' => $request->input('email', ''),
            'password' => $request->input('password', ''),
            'password_confirmation' => $request->input('password_confirmation', ''),
        ), array(
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->activation_token = md5($request->input('email'));
            $user->remember_token = str_random(10);
            $user->active = 0;
            if ($user->save()) {

                Mail::send('auth.emails.register', array(
                    'name' => $user->name,
                    'email' => $user->email,
                    'token' => $user->activation_token,
                ), function ($message) use ($user) {
                    $message->to($user->email, $user->name);
                    $message->subject(trans('reminders.register'));
                });

                return response()->created(trans('reasons.userRegistered', array('email' => $user->email)));

            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.user'))));
            }
        }
    }

    /**
     * Update current user
     *
     * Get the updated JSON object of the resource.
     *
     * @Put("/me}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"id" : "1", "name" : "Jorge Flores", "email" : "j.flores@butaca.com", "role" : {"name" : "subscriber"}, "image" : {"id": "213", "path" : "img/users/324897fAGD.jpg", "thumb" : "img/users/thumbs/324897fAGD.jpg", "cover" : "img/users/covers/324897fAGD.jpg"}})
     * @Parameters({
     *      @Parameter("name", description="The new user full name.", type="string", required=true),
     *      @Parameter("email", description="The new user email.", type="email", required=true),
     *      @Parameter("image_id", description="The new user related image id.", type="integer", required=true),
     * })
     */
    public function update(Request $request)
    {

        $user = $this->auth->user();

        $duplicateEmail = User::where('email', $request->input('email'))->where('id', '!=', $user->id)->count();

        if ($duplicateEmail >= 1) {
            return response()->badRequest(trans('reasons.duplicate', array('model' => trans('models.email'))));
        } else {
            $validator = app('validator')->make(array(
                'name' => $request->input('name', $user->name),
                'email' => $request->input('email', $user->email),
                'image_id' => $request->input('image_id', $user->image_id),
            ), array(
                'name' => 'required|min:3',
                'email' => 'required|email',
                'image_id' => 'required|exists:images,id',
            ));

            if ($validator->fails()) {
                return response()->badRequest($validator->messages());
            } else {

                $user->name = $request->input('name', $user->name);
                $user->email = $request->input('email', $user->email);
                $user->image_id = $request->input('image_id', $user->image_id);

                if ($user->save()) {
                    return response()->success(trans('reasons.updated', array('model' => trans('models.user'))));
                } else {
                    return response()->error(trans('reasons.notUpdated', array('model' => trans('models.user'))));
                }
            }
        }
    }

    /**
     * Activate user
     *
     * Get success or error response.
     *
     * @Post("/activate}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"message": "Your account was successfully activated"})
     * @Parameters({
     *      @Parameter("email", description="The user email.", type="email", required=true),
     *      @Parameter("token", description="The user activation token sent to the email as a link.", type="string", required=true),
     * })
     */
    public function activate(Request $request)
    {
        $user = User::where('email', $request->input('email'))->where('activation_token', $request->input('token'))->first();
        if ($user) {
            $user->active = 1;
            if ($user->save()) {
                return response()->success(trans('reasons.activated'));
            } else {
                return response()->error(trans('reasons.errorActivate'));
            }
        } else {
            return response()->badRequest(trans('reasons.invalidActivationLink'));
        }
    }

    /**
     * Change Password
     *
     * Set a new password for the current user.
     *
     * @Put("/password}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"message": "Your password was successfully changed"})
     * @Parameters({
     *      @Parameter("email", description="The user current email.", type="email", required=true),
     *      @Parameter("password", description="The user current password.", type="string", required=true),
     *      @Parameter("newPassword", description="The user new password.", type="string", required=true),
     *      @Parameter("confirmPassword", description="The user new password confirmation that must match the new password.", type="string", required=true),
     * })
     */
    public function password(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->badRequest(trans('auth.failed'));
        } else {

            $validator = app('validator')->make($request->only('newPassword', 'confirmNewPassword'), array(
                'newPassword' => 'required|min:6',
                'confirmNewPassword' => 'required|same:newPassword',
            ));

            if ($validator->fails()) {
                return response()->badRequest($validator->messages());
            } else {
                $user = $this->auth->user();
                $user->password = bcrypt($request->input('newPassword'));

                if ($user->save()) {
                    return response()->success(trans('reasons.updated', array('model' => trans('models.password'))));

                } else {
                    return response()->error(trans('reasons.notUpdated', array('model' => trans('models.password'))));
                }
            }
        }

    }

    /**
     * Password Reminder
     *
     * Request a password reset through email verification.
     *
     * @Post("/remind}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message": "We sent you an email with instructions to reset your password"})
     * @Parameters({
     *      @Parameter("email", description="The user current email.", type="email", required=true),
     * })
     */
    public function remind(Request $request)
    {

        $user = User::where('email', $request->input('email'))->first();

        if ($user) {

            view()->composer('auth.emails.password', function ($view) use ($user) {
                $view->with([
                    'name' => $user->name,
                    'email' => $user->email,
                    'token' => PasswordReset::where('email', $user->email)->first()->token,
                ]);
            });

            switch ($response = Password::sendResetLink($request->only('email'), function ($message) {
                $message->subject(trans('reminders.remind'));
            })) {
                case Password::INVALID_USER:
                    return response()->badRequest(trans('reminders.user'));
                case Password::RESET_LINK_SENT:
                    return response()->success(trans('reminders.sent'));
                default :
                    return response()->error();
            }


        } else {
            return response()->badRequest(trans('reminders.user'));
        }

    }

    /**
     * Password Reset
     *
     * Reset a new password through email and token verification.
     *
     * @Post("/reset}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message": "We sent you an email with instructions to reset your password"})
     * @Parameters({
     *      @Parameter("email", description="The user current email.", type="email", required=true),
     *      @Parameter("token", description="The token attached to the email button url as parameter.", type="string", required=true),
     *      @Parameter("password", description="The new password to reset.", type="string", required=true),
     *      @Parameter("password_confirmation", description="The new password confirmation that must match the password.", type="string", required=true),
     * })
     */
    public function reset(Request $request)
    {

        $credentials = $request->only('email', 'token', 'password', 'password_confirmation');

        $response = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
                return response()->badRequest(trans('reminders.password'));
            case Password::INVALID_TOKEN:
                return response()->badRequest(trans('reminders.token'));
            case Password::INVALID_USER:
                return response()->badRequest(trans('reminders.user'));

            case Password::PASSWORD_RESET:
                return response()->success(trans('reminders.reset'));
            default:
                return response()->error();
        }
    }

    /**
     * Social Login/Register
     *
     * Login/Register a new/current user through email obtained from a social network (Facebook, Google, Twitter, etc).
     *
     * @Post("/social}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"token": "5EYPA2hPuf00KM7CQG1Zs"})
     * @Parameters({
     *      @Parameter("email", description="The user current email obtained from the social network OAuth2 API response.", type="email", required=true),
     * })
     */
    public function social(Request $request)
    {

        $social = explode('|', $request->input('id'));
        $socialClient = reset($social);
        $socialId = end($social);

        $validator = app('validator')->make(
            array(
                'email' => $request->input('email'),
                'social_client' => $socialClient,
                'social_id' => $socialId,
            ),
            array(
                'email' => 'required|email',
                'social_client' => 'required',
                'social_id' => 'required',
            )
        );

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {

            $user = User::where('email', $request->input('email'))->first();

            if (!$user) {
                switch ($socialClient) {
                    case 'facebook':
                        $user = User::where('facebook_id', $socialId)->first();
                        break;
                    default:
                        $user = User::where('email', $request->input('email'))->first();
                        break;
                }
            }

            if ($user) {

                $user->name = $request->input('name', $user->name);
                $user->email = $request->input('email', $user->email);

                switch ($socialClient) {
                    case 'facebook':
                        $user->facebook_id = $socialId;
                        break;
                    case 'google-oauth2':
                        $user->google_id = $socialId;
                        break;
                }

                if ($user->save()) {
                    try {
                        if (!$token = JWTAuth::fromUser($user)) {
                            return response()->badRequest(trans('auth.failed'));
                        } else {
                            $auth = [
                                "token" => $token,
                                "user" => User::where('email', $request->input('email'))->with('image', 'roles')->first(),
                            ];
                            return response()->json($auth, 200);
                        }
                    } catch (JWTException $e) {
                        return response()->error(trans('auth.token'));
                    }
                } else {
                    return response()->error(trans('reasons.notUpdated', array('model' => trans('models.user'))));
                }

            } else {

                $user = new User;

                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->active = 1;
                $user->activation_token = md5($request->input('email'));
                $user->remember_token = str_random(10);

                switch ($socialClient) {
                    case 'facebook':
                        $user->facebook_id = $socialId;
                        break;
                    case 'google-oauth2':
                        $user->google_id = $socialId;
                        break;
                }

                if ($user->save()) {
                    try {
                        if (!$token = JWTAuth::fromUser($user)) {
                            return response()->badRequest(trans('auth.failed'));
                        } else {
                            $auth = [
                                "token" => $token,
                                "user" => User::where('email', $request->input('email'))->with('image', 'roles')->first(),
                            ];
                            return response()->json($auth, 200);
                        }
                    } catch (JWTException $e) {
                        return response()->error(trans('auth.token'));
                    }

                } else {
                    return response()->error(trans('reasons.notCreated', array('model' => trans('models.user'))));
                }
            }
        }
    }

    public function token(){
        $token = JWTAuth::getToken();
        if (!$token) {
            throw new BadRequestHtttpException('Token not provided');
        }
        try {
            $token = JWTAuth::refresh($token);
        } catch (TokenInvalidException $e){
            throw new AccessDeniedHttpException('The token is invalid');
        }
        return $this->response->withArray(['token'=>$token]);
    }
}
