<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Magazine;
use Illuminate\Http\Request;

/**
 * @Resource("Magazines", uri="/magazines")
 */
class MagazineController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all paginated magazines with their corresponding image.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[magazines]"})
     */
    public function index(Request $request)
    {
        $fields = ['id', 'lang', 'title', 'volume', 'number', 'date', 'image_id'];
        $magazines = Magazine::select($fields)->where('lang', config('app.locale'))->search($request->input('search', ''))->with('image')->paginate($request->input('limit', 10));
        return response()->json($magazines, 200);
    }

    /**
     * Show a single magazine
     *
     * Get a JSON object of a single magazine with its corresponding image and docs.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id": "1", "lang": "es", "question" : "This is a question example", "answer" : "This is the answer example.", "created_at" : "2061-05-19 23:47:32", "updated_at" : "2061-05-19 23:47:32" })
     */

    public function show($id)
    {
        $magazine = Magazine::where('id', $id)->with('image', 'docs.features')->first();
        return response()->json($magazine, 200);
    }
}
