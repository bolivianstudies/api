<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Publication;
use Illuminate\Http\Request;

/**
 * @Resource("Publications", uri="/publications")
 */
class PublicationController extends Controller
{

    /**
     * Show all, category or tag publications
     *
     * Get a JSON array of all paginated publications.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[publications]"})
     */
    public function index(Request $request)
    {
        $publications = Publication::where('lang', config('app.locale'));

        if ($request->input('key')) {
            if ($request->input('letter')) {
                $publications = $publications->where($request->input('key'), 'LIKE', $request->input('letter') . '%');
            }
            $publications = $publications->search($request->input('search', ''), [$request->input('key')])->orderBy($request->input('key'), 'asc');
        } else {
            $publications = $publications->search($request->input('search', ''));
        }

        $publications = $publications->with('image')->paginate($request->input('limit', 6));

        return response()->json($publications, 200);
    }
}
