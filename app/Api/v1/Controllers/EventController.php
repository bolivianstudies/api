<?php

namespace App\Api\v1\Controllers;

use App\Event;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @Resource("Events", uri="/events")
 */
class EventController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all paginated events.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"total": "50", "current_page" : "2", "per_page" : "20", "last_page" : "3", "next_page_url": "http://api.pasf.com/users?page=3", "prev_page_url": "http://api.pasf.com/users?page=1", "from": "21", "to": "40", "data": "[posts]"})
     * @Parameters({
     *      @Parameter("page", description="The page number.", type="integer", required=false),
     *      @Parameter("limit", description="The number of results per page.", type="integer", required=false),
     *      @Parameter("search", description="A search term.", type="sting", required=false),
     *      @Parameter("ignore", description="A post id to ignore.", type="integer", required=false)
     * })
     */
    public function index(Request $request)
    {
        $fields = ['id', 'lang', 'title', 'start_date', 'image_id', 'location_id', 'slug'];
        $events = Event::select($fields)->where('lang', config('app.locale'));
        $events = $events->search($request->input('search', ''))->orderBy('start_date', 'desc')->with('image', 'location.country', 'features')->paginate($request->input('limit', 6));
        return response()->json($events, 200);
    }

    /**
     * Show a single event
     *
     * Get a JSON object of a single event with its image, timeline, pricing and features.
     *
     * @Get("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id": "1", "title" : "This is a title example", "excerpt" : "This is the post excerpt...", "content" : "<p>This is a large post content...</p>", "category" : {"id" : "3", "title" : "Notas de prensa"}, "comments_count" : {"aggregate" : "12"}, "created_at" : "2061-05-19 23:47:32" })
     */

    public function show($id)
    {
        $event = Event::where('slug', $id)->where('lang', config('app.locale'))->with('image', 'images', 'docs', 'location.markers', 'location.country', 'timeline', 'pricing')->first();
        return response()->json($event, 200);
    }

    public function updateContent($id, Request $request)
    {
        $event = Event::find($id);
        $event->content = clean($request->input('content', ''));
        if (!$event->save()) {
            return response()->error(trans('messages.notUpdated', array('model' => trans('models.event'))));
        }
        return response()->success(trans('messages.updated', array('model' => trans('models.event'))));
    }
}
