<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;
use Mail;

/**
 * @Resource("Subscribers", uri="/subscribers")
 */
class SubscriberController extends Controller
{

    /**
     * Show a single subscriber
     *
     * Get a JSON object of a single subscriber with its corresponding settings.
     *
     * @Get("/{token}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"id" : "1", "name" : "John Smith", "email" : "john.smith@gmail.com", "active" : "1", "posts": "1", "events" : "0", "docs" : "0", "magazines" : "1", "newsletters": "0", "publications" : "1", "activation_token" : "425ASD32421DFSA3R23FA" })
     */

    public function show($token)
    {
        $subscriber = Subscriber::where('activation_token', $token)->first();
        if ($subscriber) {
            return response()->json($subscriber, 200);
        } else {
            return response()->notFound();
        }
    }

    /**
     * Create a new subscriber
     *
     * Get a JSON response of the newly created subscriber.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message" : "The subscriber was created successfully"}})
     * @Parameters({
     *      @Parameter("name", description="The new subscriber full name.", type="string", required=true),
     *      @Parameter("email", description="The new subscriber email.", type="email", required=true)
     * })
     */
    public function store(Request $request)
    {
        $validator = app('validator')->make(array(
            'name' => $request->input('name', ''),
            'email' => $request->input('email', ''),
        ), array(
            'name' => 'required|min:3',
            'email' => 'required|email|unique:subscribers',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $subscriber = new Subscriber;
            $subscriber->name = $request->input('name');
            $subscriber->email = $request->input('email');
            $subscriber->activation_token = md5($request->input('email'));
            if ($subscriber->save()) {
                Mail::send('subscriber.emails.subscribe', array(
                    'name' => $subscriber->name,
                    'email' => $subscriber->email,
                    'token' => $subscriber->activation_token,
                ), function ($message) use ($subscriber) {
                    $message->to($subscriber->email, $subscriber->name);
                    $message->subject(trans('reminders.subscribe'));
                });
                return response()->success(trans('reasons.preSubscribed', array('email' => $subscriber->email)));
            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.subscriber'))));
            }
        }
    }

    /**
     * Update specific subscriber
     *
     * Get the response message of the request.
     *
     * @Put("/{token}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message" : "The subscription settings were updated"})
     * @Parameters({
     *      @Parameter("active", description="The subscription activation status.", type="boolean", required=false),
     *      @Parameter("posts", description="The subscription to posts status.", type="boolean", required=false),
     *      @Parameter("events", description="The subscription to events status.", type="boolean", required=false),
     *      @Parameter("docs", description="The subscription to docs status.", type="boolean", required=false),
     *      @Parameter("magazines", description="The subscription to magazines status.", type="boolean", required=false),
     *      @Parameter("newsletters", description="The subscription to newsletters status.", type="boolean", required=false),
     *      @Parameter("publications", description="The subscription to publications status.", type="boolean", required=false)
     * })
     */
    public function update($token, Request $request)
    {
        $subscriber = Subscriber::where('activation_token', $token)->first();

        if ($subscriber) {

            $validator = app('validator')->make(array(
                'active' => $request->input('active', $subscriber->active),
                'posts' => $request->input('posts', $subscriber->posts),
                'events' => $request->input('events', $subscriber->events),
                'docs' => $request->input('docs', $subscriber->docs),
                'magazines' => $request->input('magazines', $subscriber->magazines),
                'newsletters' => $request->input('newsletters', $subscriber->newsletters),
                'publications' => $request->input('publications', $subscriber->publications),
            ), array(
                'active' => 'boolean',
                'posts' => 'boolean',
                'events' => 'boolean',
                'docs' => 'boolean',
                'magazines' => 'boolean',
                'newsletters' => 'boolean',
                'publications' => 'boolean',
            ));

            if ($validator->fails()) {
                return response()->badRequest($validator->messages());
            } else {

                $subscriber->active = $request->input('active', $subscriber->active);
                $subscriber->posts = $request->input('posts', $subscriber->posts);
                $subscriber->events = $request->input('events', $subscriber->events);
                $subscriber->docs = $request->input('docs', $subscriber->docs);
                $subscriber->magazines = $request->input('magazines', $subscriber->magazines);
                $subscriber->newsletters = $request->input('newsletters', $subscriber->newsletters);
                $subscriber->publications = $request->input('publications', $subscriber->publications);

                if ($subscriber->save()) {
                    return response()->success(trans('messages.updated', array('model' => trans('models.subscriber'))));
                } else {
                    return response()->error(trans('messages.notUpdated', array('model' => trans('models.subscriber'))));
                }
            }
        } else {
            return response()->notFound();
        }
    }
}
