<?php

namespace App\Api\v1\Controllers;

use App\Directory;
use App\Http\Controllers\Controller;

/**
 * @Resource("Directory", uri="/directories")
 */
class DirectoryController extends Controller
{

    /**
     * Show all directories
     *
     * Get a JSON array of all directories.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[directories]")
     */
    public function index()
    {
        $directories = Directory::orderBy('id', 'DESC')->get();
        return response()->json($directories, 200);
    }

}
