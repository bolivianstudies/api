<?php

namespace App\Api\v1\Controllers;

use App\Country;
use App\Http\Controllers\Controller;

/**
 * @Resource("Countries", uri="/countries")
 */
class CountryController extends Controller
{

    /**
     * Show all countries
     *
     * Get a JSON array of all countries.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[countries]")
     */
    public function index()
    {
        $countries = Country::all();
        return response()->json($countries, 200);
    }

}
