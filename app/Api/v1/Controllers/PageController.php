<?php

namespace App\Api\v1\Controllers;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Purifier;

/**
 * @Resource("Pages", uri="/pages")
 */
class PageController extends Controller
{

    /**
     * Show all pages
     *
     * Get a JSON array of all pages.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[pages]")
     */
    public function index()
    {
        $fields = array('id', 'lang', 'parent_id', 'lft', 'rgt', 'depth', 'order', 'title', 'slug');
        $pages = Page::where('lang', config('app.locale'))->orderBy('order', 'asc')->get($fields);
        return response()->json(array_values($pages->toHierarchy()->toArray()), 200);
    }

    /**
     * Show a page
     *
     * Get a JSON object of the page.
     *
     * @Get("/slug")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[{}]")
     */
    public function show($slug, Request $request)
    {
        $fields = array('id', 'lang', 'parent_id', 'rgt', 'lft', 'depth', 'title', 'slug', 'content', 'image_id');
        $page = Page::where('slug', $slug)->with('image', 'pricing', 'timeline')->where('lang', config('app.locale'))->first($fields);
        if ($page) {
            if ($request->input('siblings')) {
                $fields = array('id', 'parent_id', 'rgt', 'lft', 'depth', 'title', 'slug', 'order');
                $page = $page->siblingsAndSelf()->get($fields);
            }
            return response()->json($page, 200);
        } else {
            return response()->notFound();
        }
    }

    public function updateContent($id, Request $request)
    {
        $page = Page::find($id);
        $page->content = clean($request->input('content', ''));
        if (!$page->save()) {
            return response()->error(trans('messages.notUpdated', array('model' => trans('models.page'))));
        }
        return response()->success(trans('messages.updated', array('model' => trans('models.page'))));
    }
}
