<?php

namespace App\Api\v1\Controllers;

use App\Director;
use App\Directory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * @Resource("Directors", uri="/directors")
 */
class DirectorController extends Controller
{

    /**
     * Show all directors
     *
     * Get a JSON array of all directors.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[directors]")
     * @Parameters({
     *      @Parameter("year", description="A reference year of the directory.", type="integer", required=false)
     * })
     */
    public function index(Request $request)
    {
        if ($request->input('year')) {
            $directory = Directory::find($request->input('year'));
            if ($directory) {
                $directors = $directory->directors()->with('image')->get();
            } else {
                return response()->notFound();
            }
        } else {
            $directors = Director::with('image')->get();
        }
        return response()->json($directors, 200);
    }

}
