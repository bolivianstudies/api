<?php

namespace App\Api\v1\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Http\Request;

/**
 * @Resource("Categories", uri="/categories")
 */
class CategoryController extends Controller
{

    /**
     * Show all categories
     *
     * Get a JSON array of all categories.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[categories]")
     */
    public function index()
    {
        $categories = Category::orderBy('order', 'asc')->get();
        return response()->json(array_values($categories->toHierarchy()->toArray()), 200);
    }

    /**
     * Show one category
     *
     * Get a JSON object of the category.
     *
     * @Get("/{slug}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="{'id' : 1, 'title' : 'Inicio', 'slug' : 'inicio', 'lang' : 'es', 'country' : 'BO', 'content' : '<p>Esta es la página de inicio</p>'}")
     */
    public function show($slug)
    {
        $category = Category::where('slug', '=', $slug)->first();
        if ($category) {
            $category = $category->getDescendantsAndSelf()->toHierarchy()->first();
            return response()->json($category, 200);
        } else {
            return response()->notFound();
        }
    }

    /**
     * Insert a new category
     *
     * Get a JSON response of the action
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(201, body={"message" : "The category was created successfully"})
     * @Parameters({
     *      @Parameter("title", description="The new category title.", type="string", required=true),
     *      @Parameter("description", description="The new category description.", type="text", required=true)
     * })
     */
    public function store(Request $request)
    {

        $validator = app('validator')->make(array(
            'title' => $request->input('title', ''),
        ), array(
            'title' => 'required|min:3',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $category = new Category;
            $category->title = $request->input('title');
            $category->icon = $request->input('icon', '');
            $category->parent_id = $request->input('parent_id', null);
            $category->order = $request->input('order', 0);
            if ($category->save()) {
                return response()->created();
            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.category'))));
            }
        }
    }

    /**
     * Update a current category
     *
     * Get a JSON response of the action
     *
     * @Put("/{category}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message" : "The category was updated"})
     * @Parameters({
     *      @Parameter("title", description="The new category title.", type="string", required=true),
     *      @Parameter("description", description="The new category description.", type="text", required=true)
     * })
     */
    public function update($categoryId, Request $request)
    {

        $category = Category::find($categoryId);

        $validator = app('validator')->make(array(
            'title' => $request->input('title', $category->title),
        ), array(
            'title' => 'required|min:4',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $category->title = $request->input('title', $category->title);
            $category->order = $request->input('order', $category->ord);
            if ($category->save()) {
                return response()->success(trans('reasons.updated', array('model' => trans('models.category'))));
            } else {
                return response()->error(trans('reasons.notCreated', array('model' => trans('models.category'))));
            }
        }
    }

    /**
     * Remove a category
     *
     * Get a response after the update
     *
     * @Delete("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"message" : "The category was deleted"})
     */
    public function destroy($categoryId)
    {
        $category = Category::find($categoryId);

        if ($category->delete()) {
            return response()->success(trans('messages.deleted', array('model' => trans('models.category'))));
        } else {
            return response()->error(trans('reasons.notDeleted', array('model' => trans('models.category'))));
        }

    }

}
