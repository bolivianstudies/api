<?php

namespace App\Api\v1\Controllers;

use App\Event;
use App\Http\Controllers\Controller;
use App\Page;
use App\Timeline;
use Illuminate\Http\Request;

/**
 * @Resource("Timeline", uri="/timeline")
 */
class TimelineController extends Controller
{

    /**
     * Show all, category or tag posts
     *
     * Get a JSON array of all paginated events.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body={"total": "50", "current_page" : "2", "per_page" : "20", "last_page" : "3", "next_page_url": "http://api.pasf.com/users?page=3", "prev_page_url": "http://api.pasf.com/users?page=1", "from": "21", "to": "40", "data": "[posts]"})
     * @Parameters({
     *      @Parameter("page", description="The page slug related to timeline table.", type="string", required=false),
     *      @Parameter("event", description="The event id related to the timeline table.", type="integer", required=false)
     * })
     */
    public function index(Request $request)
    {
        $timeline = array();
        if ($request->input('page')) {
            $page = Page::where('slug', $request->input('page'))->where('lang', config('app.locale'))->first();
            if ($page) {
                $timeline = $page->timeline;
            }
        } elseif ($request->input('event')) {
            $event = Event::find($request->input('event'));
            if ($event) {
                $timeline = $event->timeline;
            }
        } else {
            $timeline = Timeline::where('lang', config('app.locale'))->orderBy('date', 'asc')->get();
        }
        return response()->json($timeline, 200);
    }
}
