<?php

namespace App\Api\v1\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Image;
use Illuminate\Http\Request;
use Picture;

/**
 * @Resource("Images", uri="/images")
 */
class ImageController extends Controller
{


    /**
     * Show all images
     *
     * Get a JSON array of all images filtered by category.
     *
     * @Get("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json"})
     * @Response(200, body="[images]")
     */
    public function index(Request $request)
    {
        if ($request->input('category')) {
            $category = Category::where('lang', '=', config('app.locale'))->where('country', '=', config('app.country'))->where('slug', '=', $request->input('category'))->first(['id', 'parent_id', 'rgt', 'lft', 'depth']);
            if ($category) {
                $images = $category->images()->paginate($request->input('limit', 12));
            } else {
                return response()->notFound();
            }
        } else {
            $images = Image::paginate($request->input('limit', 12));
        }

        return response()->json($images, 200);
    }


    /**
     * Store an image with thumb and cover paths
     *
     * Get the recently stored image as a JSON object.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"path": "path/to/original/image.jpg", "thumb" : "path/to/image/thumb.jpg", "cover" : "path/to/image/cover.jpg"})
     * @Parameters({
     *      @Parameter("dir", description="The dir where the image is stored.", type="string", required=true),
     *      @Parameter("filename", description="The filename of the uploaded image to store.", type="string", required=true)
     * })
     */
    public function store(Request $request)
    {
        if ($request->has('dir') && $request->has('filename')) {

            $baseDir = explode('/', base_path());
            unset ($baseDir[count($baseDir) - 1]);
            $baseDir = implode('/', $baseDir) . '/frontend';
            $imageDir = $baseDir . '/' . $request->input('dir') . '/' . $request->input('filename');

            $image = new Image;

            if ($request->has('thumb') && $request->input('thumb') === true) {
                $thumbWidth = 200;
                $thumbHeight = 200;
                $thumb = Picture::make($imageDir)->fit($thumbWidth, $thumbHeight, function ($constraint) {
                    $constraint->upsize();
                });
                $thumb->save($baseDir . '/' . $request->input('dir') . '/thumbs/' . $request->input('filename'));
            }

            $image->thumb = $request->input('dir') . '/thumbs/' . $request->input('filename');
            $image->path = $request->input('dir') . '/' . $request->input('filename');
            if ($image->save()) {
                return response()->json($image, 201);
            } else {
                return response()->error(trans('reasons.notSavedImage'));
            }

        } else {
            return response()->badRequest(trans('reasons.notImageData'));
        }
    }

    /**
     * Update an image
     *
     * Get a JSON response message
     *
     * @Put("/")
     * @Versions({"v1"})
     * @Request(headers={"Accept": "application/prs.aeb.v1+json", "Authorization" : "Bearer {5EYPA2hPuf00KM7CQG1Zs}"})
     * @Response(200, body={"message": "The image has been updated"})
     * @Parameters({
     *      @Parameter("path", description="The path of the updated image.", type="string", required=false),
     *      @Parameter("thumb", description="The path of the updated thumbnail image", type="string", required=false)
     * })
     */
    public function update($imageId, Request $request)
    {

        $validator = app('validator')->make(array(
            'id' => $imageId,
        ), array(
            'id' => 'required|exists:images,id',
        ));

        if ($validator->fails()) {
            return response()->badRequest($validator->messages());
        } else {
            $image = Image::find($imageId);
            $image->thumb = $request->input('thumb', $image->thumb);
            $image->path = $request->input('path', $image->path);
            if ($image->save()) {
                return response()->success(trans('reasons.updated', array('model' => trans('models.image'))));
            } else {
                return response()->error(trans('reasons.notUpdated', array('model' => trans('models.image'))));
            }
        }
    }
}
