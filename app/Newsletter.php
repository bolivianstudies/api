<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Newsletter extends Model
{
    use Eloquence;

    protected $table = 'newsletters';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'title', 'date', 'excerpt', 'image_id', 'doc_id'];

    protected $searchableColumns = ['title', 'date', 'excerpt', 'doc.path'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    public function doc()
    {
        return $this->belongsTo('App\Doc');
    }
}
