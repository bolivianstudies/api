<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Event extends Model
{
    use Sluggable, SluggableScopeHelpers, Eloquence {
        Eloquence::replicate insteadof Sluggable;
    }

    protected $table = 'events';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'title', 'start_date', 'end_date', 'start_time', 'end_time', 'excerpt', 'content', 'slug', 'image_id', 'location_id'];

    protected $searchableColumns = ['title', 'excerpt', 'content', 'location.address'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function image()
    {
        return $this->belongsTo('App\Image');
    }

    function location()
    {
        return $this->belongsTo('App\Location');
    }

    function timeline()
    {
        return $this->belongsToMany('App\Timeline', 'event_timeline')->orderBy('date', 'asc');
    }

    function pricing()
    {
        return $this->belongsToMany('App\Pricing', 'event_pricing')->orderBy('event_pricing.order', 'asc')->withPivot('order');
    }

    function features()
    {
        return $this->belongsToMany('App\Feature', 'event_feature')->orderBy('order', 'asc');
    }

    function docs()
    {
        return $this->belongsToMany('App\Doc', 'event_doc')->orderBy('docs.created_at', 'desc');
    }

    function images()
    {
        return $this->belongsToMany('App\Image', 'event_image')->orderBy('images.created_at', 'desc')->withPivot('order');
    }
}
