<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationMarker extends Model
{
    protected $table = 'location_marker';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'location_id', 'longitude', 'latitude', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
