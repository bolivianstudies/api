<?php

namespace App;

use Baum\Node;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Page extends Node
{

    use Sluggable, SluggableScopeHelpers;

    protected $table = 'pages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'title', 'order', 'excerpt', 'content', 'slug', 'parent_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Column to perform the default sorting
     *
     * @var string
     */
    protected $orderColumn = 'order';

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function image()
    {
        return $this->belongsTo('App\Image');
    }

    function pricing()
    {
        return $this->belongsToMany('App\Pricing', 'page_pricing')->orderBy('page_pricing.order', 'asc')->withPivot('order');
    }

    function timeline()
    {
        return $this->belongsToMany('App\Timeline', 'page_timeline')->orderBy('date', 'asc');
    }

}
