<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    protected $table = 'directors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'email', 'image_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function image()
    {
        return $this->belongsTo('App\Image');
    }

    public function directories()
    {
        return $this->belongsToMany('App\Directory', 'director_directory')
            ->withPivot('role', 'position')
            ->orderBy('directories.year', 'DESC');
    }

    public function toArray()
    {
        $attributes = $this->attributesToArray();
        $attributes = array_merge($attributes, $this->relationsToArray());

        if (isset($attributes['pivot'])) {
            $attributes['directory'] = $attributes['pivot'];
            unset($attributes['pivot']);
        }
        return $attributes;
    }
}
