<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Dimsav\Translatable\Translatable;

class PricingTag extends Model
{
    use Translatable;

    public $translationModel = 'App\PricingTagTranslation';

    public $translatedAttributes = ['description'];

    protected $table = 'pricing_tags';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['label'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function pricing()
    {
        return $this->belongsToMany('App\Pricing', 'pricing_tag_pricing')
            ->orderBy('pricing_tag_pricing.order', 'asc')
            ->withPivot('order');
    }
}
