<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Order extends Model
{
    use Eloquence;

    protected $table = 'orders';

    protected $fillable = ['user_id', 'membership_id', 'country_id', 'city', 'name', 'email', 'address', 'phone', 'zip', 'verified_at', 'payment_method_id', 'payment_description'];

    protected $searchableColumns = ['name', 'email', 'user.name', 'user.email'];

    protected $hidden = [];

    public function pricing()
    {
        return $this->belongsTo('App\Pricing');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
}
