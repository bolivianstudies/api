<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Profile extends Model
{
    use Eloquence;

    protected $table = 'profiles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'name', 'institution', 'address_institution', 'address_personal', 'discipline', 'areas', 'content', 'birthday', 'phone', 'website', 'facebook', 'twitter', 'google', 'skype', 'city', 'country_id', 'user_id'];

    protected $searchableColumns = ['name', 'discipline', 'areas', 'user.email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function education()
    {
        return $this->hasMany('App\Education');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
