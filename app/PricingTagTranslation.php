<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PricingTagTranslation extends Model
{
    protected $table = 'pricing_tags_translations';
    public $timestamps = false;
    protected $fillable = ['description'];
}