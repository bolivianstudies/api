<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    public $incrementing = false;
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

}
