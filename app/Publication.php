<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Publication extends Model
{

    use Eloquence;

    protected $table = 'publications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'lang', 'title', 'author', 'year', 'editorial', 'city_country', 'discipline', 'case', 'image_id'];

    protected $searchableColumns = ['title', 'author', 'year', 'editorial', 'city_country', 'discipline', 'case'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function image()
    {
        return $this->belongsTo('App\Image');
    }
}
