<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'address', 'location', 'city', 'country_id', 'latitude', 'longitude'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function markers()
    {
        return $this->hasMany('App\LocationMarker');
    }
}
